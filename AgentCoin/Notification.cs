﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace AgentCoin
{
    class Notification
    {
        public string TipTitle { get; set; }
        public string TipText { get; set; }
        public bool IsError { get; set; }
        public Icon AgentIcon { get; set; }

        public Notification(string tipTitle, string tipText, bool isError, Icon agentIcon)
        {
            this.TipTitle = tipTitle;
            this.TipText = tipText;
            this.IsError = isError;
            this.AgentIcon = agentIcon;
        }
    }
}
