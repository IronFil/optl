﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;
using AgentCoin.Properties;

namespace AgentCoin
{
    class CancelException : Exception
    {
        public override string Message
        {
            get
            {
                return "Операция отменена";
            }
        }
    }

    abstract class QueuedTask
    {
        protected Logger logger;

        public QueuedTask(DateTime startTime, Logger logger)
        {
            this.StartTime = startTime;
            this.logger = logger;
        }

        public delegate bool IsCancelledDelegate();

        abstract internal void Execute(IsCancelledDelegate isCancelled);

        abstract internal IEnumerable<QueuedTask> OnAfterCompleted(out Notification notification);

        internal DateTime StartTime { get; set; }
    }

    class CheckNewDBTask : QueuedTask
    {
        List<FtpFile> newDBs = new List<FtpFile>();
        List<FtpFile> updatedDBs = new List<FtpFile>();
        Exception error;

        Dictionary<string, FtpFile> lastSeen;

        void LoadLastSeenDBs(string lastSeenFile)
        {
            lastSeen = new Dictionary<string, FtpFile>();
            if (File.Exists(lastSeenFile))
            {
                foreach (var line in File.ReadAllLines(lastSeenFile))
                {
                    var ff = FtpFile.Parse(line);
                    lastSeen.Add(ff.Name, ff);
                }
            }
        }

        public CheckNewDBTask(DateTime startTime, Logger logger)
            : base(startTime, logger)
        {
            
        }

        internal override void Execute(IsCancelledDelegate isCancelled)
        {
            string lastSeenFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "lastseen.lst");

            LoadLastSeenDBs(lastSeenFile);

            var request = (FtpWebRequest)WebRequest.Create("ftp://216.178.212.20/bulletin/");//216.255.89.39 ftp.cme.com
            request.Timeout = 5000;
            request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
            request.Credentials = new NetworkCredential("anonymous", "tumypv@rf.ru");

            FtpWebResponse response = null;

            for (int i = 0; i < 6; i++)
            {
                var newLastSeen = new StringBuilder();
                error = null;
                try
                {
                    using (response = (FtpWebResponse)request.GetResponse())
                    using (Stream responseStream = response.GetResponseStream())
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            var file = FtpFile.Parse(line);

                            if (lastSeen.ContainsKey(file.Name))
                            {
                                var last = lastSeen[file.Name];
                                if (!last.Equals(file))
                                    updatedDBs.Add(last);
                            }
                            else
                                newDBs.Add(file);

                            newLastSeen.AppendLine(line);
                        }
                        reader.Close();
                        response.Close();
                    }
                }
                catch (Exception ex)
                {
                    this.error = ex;
                    //errors.Add(ex);
                }
                finally
                {
                }

                if (error == null)
                {
                    File.WriteAllText(lastSeenFile, newLastSeen.ToString());
                    break;
                }
                else if (isCancelled())
                    break;
                else
                    Thread.Sleep(15000);
            }
        }

        internal override IEnumerable<QueuedTask> OnAfterCompleted(out Notification notification)
        {
            notification = null;

            if (error != null)
            {
                notification = new Notification("Ошибка проверки обновлений CME DB", error.Message, true, Resources.AgentError);
            }
            else if (newDBs.Count > 1)
            {
                notification = new Notification("Обнаружены новые DB", newDBs.Count.ToString() + " шт.", false, Resources.AgentHasNews);
            }
            else if (newDBs.Count == 1)
            {
                notification = new Notification("Обнаружен новый DB", newDBs[0].Name, false, Resources.AgentHasNews);
            }
            else if (updatedDBs.Count > 1)
            {
                notification = new Notification("Обнаружены обновлённые DB", updatedDBs.Count.ToString() + " шт.", false, Resources.AgentHasNews);
            }
            else if (updatedDBs.Count == 1)
            {
                notification = new Notification("Обнаружено обновление DB", updatedDBs[0].Name, false, Resources.AgentHasNews);
            }

            var newTasks = new List<QueuedTask>();
            newTasks.Add(new CheckNewDBTask(DateTime.Now.AddMinutes(15), logger));
            if (lastSeen.Count > 0 && Settings.Default.Autodownload)
            {
                foreach (var item in newDBs)
                    newTasks.Add(new DownloadDBTask(logger, item.Name));
                foreach (var item in updatedDBs)
                    newTasks.Add(new DownloadDBTask(logger, item.Name));
            }

            return newTasks;
        }
    }

    class DownloadDBTask : QueuedTask
    {
        Exception error;
        string fileName;
        public string downloadedFilePath;

        public DownloadDBTask(Logger logger, string fileName)
            : base(DateTime.Now, logger)
        {
            this.fileName = fileName;
        }

        internal override void Execute(IsCancelledDelegate isCancelled)
        {
            for (int i = 0; i < 4; i++)
            {
                downloadedFilePath = Path.Combine(Program.DbDownloadsPath, fileName);
                if (File.Exists(downloadedFilePath))
                    downloadedFilePath = Path.Combine(Program.DbDownloadsPath,
                        Path.GetFileName(fileName) + "#" + DateTime.Now.ToString("yyMMddHHmmss") + Path.GetExtension(fileName));

                error = null;
                try
                {
                    var request = (FtpWebRequest)WebRequest.Create("ftp://216.178.212.20/bulletin/" + fileName);//216.255.89.39 ftp.cme.com
                    request.Timeout = 15000;
                    request.Method = WebRequestMethods.Ftp.DownloadFile;
                    request.Credentials = new NetworkCredential("anonymous", "tumypv@rf.ru");

                    FtpWebResponse response = null;

                    using (response = (FtpWebResponse)request.GetResponse())
                    {
                        var dir = Path.GetDirectoryName(downloadedFilePath);
                        if (!Directory.Exists(dir))
                            Directory.CreateDirectory(dir);

                        using (var inputStream = response.GetResponseStream())
                        using (var outputStream = File.OpenWrite(downloadedFilePath))
                        {
                            var buffer = new byte[1024 * 1024];
                            int totalReadBytesCount = 0;
                            int readBytesCount;
                            while ((readBytesCount = inputStream.Read(buffer, 0, buffer.Length)) > 0)
                            {
                                outputStream.Write(buffer, 0, readBytesCount);
                                totalReadBytesCount += readBytesCount;
                                //var progress = totalReadBytesCount * 100.0 / inputStream.Length;
                                //backgroundWorker1.ReportProgress((int)progress);
                                if (isCancelled())
                                    throw new CancelException();
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.error = ex;
                    if (File.Exists(downloadedFilePath))
                        File.Delete(downloadedFilePath);
                }

                if (error == null || error is CancelException)
                    break;
                else if (isCancelled())
                    break;
                else
                    Thread.Sleep(15000);
            }
        }

        internal override IEnumerable<QueuedTask> OnAfterCompleted(out Notification notification)
        {
            if (error != null)
            {
                notification = new Notification("Ошибка загрузки CME DB", error.Message, true, Resources.AgentError);
            }
            else
            {
                notification = new Notification("Загружен CME DB", downloadedFilePath, false, Resources.AgentHasNews);
            }
            return new QueuedTask[0];
        }
    }

    class CheckCOT : QueuedTask
    {
        Exception error;

        string lastSeenCOT;

        bool newCOT = false;

        void LoadLastSeenCOT(string lastSeenFile)
        {

            if (File.Exists(lastSeenFile))
                lastSeenCOT = File.ReadAllText(lastSeenFile);
            else
                lastSeenCOT = null;
        }

        public CheckCOT(DateTime startTime, Logger logger)
            : base(startTime, logger)
        {

        }

        internal override void Execute(IsCancelledDelegate isCancelled)
        {
            string lastSeenFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "lastseenCOT.txt");

            LoadLastSeenCOT(lastSeenFile);

            int year = DateTime.Now.Year;
            var request = System.Net.HttpWebRequest.Create(string.Format("http://www.cftc.gov/files/dea/history/deahistfo{0}.zip",year));
            request.Timeout = 15000;
            request.Method = "HEAD";

            for (int i = 0; i < 6; i++)
            {
                var newLastSeen = new StringBuilder();
                string contentLength = null;
                error = null;
                try
                {
                    using (var response = request.GetResponse())
                    {
                        contentLength = response.Headers.Get("Content-Length");
                    }
                }
                catch (Exception ex)
                {
                    this.error = ex;
                    //errors.Add(ex);
                }

                var webError = error as WebException;
                if (webError != null && webError.Message.Contains("404"))
                {
                    //Happy new year!
                    error = null;
                    break;
                }
                else if (error == null)
                {
                    if (lastSeenCOT != null && lastSeenCOT != contentLength)
                        newCOT = true;
                    File.WriteAllText(lastSeenFile, contentLength);
                    break;
                }
                else if (isCancelled())
                    break;
                else
                    Thread.Sleep(15000);
            }
        }

        internal override IEnumerable<QueuedTask> OnAfterCompleted(out Notification notification)
        {
            notification = null;

            if (error != null)
            {
                notification = new Notification("Ошибка проверки СОТ", error.Message, true, Resources.AgentError);
            }
            else if (newCOT)
            {
                notification = new Notification("Проверка СОТ", "Обнаружен новый СОТ", false, Resources.AgentHasNews);
            }

            var newTasks = new List<QueuedTask>();
            newTasks.Add(new CheckCOT(DateTime.Now.AddMinutes(20), logger));

            return newTasks;
        }
    }

    class CheckForwardPoints : QueuedTask
    {
        Exception error;
        bool forwardPointsUpdated = false;

        public CheckForwardPoints(DateTime startTime, Logger logger) : base(startTime, logger) { }

        string[] productCodes = { "6E", "6J", "6B", "6C", "6S", "6A", "6M", "6N" };

        internal override void Execute(IsCancelledDelegate isCancelled)
        {
            string forwardPointsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "ForwardPoints.csv");

            if (File.Exists(forwardPointsFile))
            {
                var forwardPointsUpdate = File.GetLastWriteTime(forwardPointsFile);
                if (DateTime.Now.Subtract(forwardPointsUpdate).TotalHours < 12)
                    return;
            }
            else
            {
                File.WriteAllText(forwardPointsFile, "updatetime;cme_date;" + string.Join(";;", productCodes) + System.Environment.NewLine);
            }

            var uri = new Uri("http://datasuite.cmegroup.com/md/dataSuite/template/nfx/productCode/6E,6J,6B/XCME/NFX?selectedTab=fx");
            var request = System.Net.HttpWebRequest.Create(uri);
            //var request = System.Net.HttpWebRequest.Create("http://ya.ru/");

            request.Timeout = 15000;
            request.Method = "GET";

            System.Xml.XPath.XPathDocument result = null;

            for (int i = 0; i < 6; i++)
            {
                error = null;
                try
                {
                    using (var responseStream = request.GetResponse().GetResponseStream())
                    //using (var sr = new StreamReader(responseStream, Encoding.UTF8))
                    {
                        result = new System.Xml.XPath.XPathDocument(responseStream);

                        //result = new System.Xml.XPath.XPathDocument(File.OpenRead(@"C:\New folder\FP\120807.xml"));
                    }
                }
                catch (Exception ex)
                {
                    this.error = ex;
                    //errors.Add(ex);
                }

                if (error == null)
                {
                    var nav = result.CreateNavigator();
                  //<forwardPoints month="September" tradeDate="07/26/2012">
                  //  <forwardPoint bid="6.80" currencyName="Euro FX" offer="7.00" productCode="6E"/>
                  //  <forwardPoint bid="-4.60" currencyName="J Yen" offer="-4.40" productCode="6J"/>
                  //  <forwardPoint bid="-1.70" currencyName="B Pound" offer="-1.50" productCode="6B"/>
                  //  <forwardPoint bid="11.50" currencyName="Canadian$" offer="11.70" productCode="6C"/>
                  //  <forwardPoint bid="-10.80" currencyName="Swiss Franc" offer="-10.60" productCode="6S"/>
                  //  <forwardPoint bid="-48.70" currencyName="Aussie$" offer="-48.50" productCode="6A"/>
                  //  <forwardPoint bid="660.00" currencyName="M Peso" offer="690.00" productCode="6M"/>
                  //  <forwardPoint bid="-27.20" currencyName="NewZ$" offer="-27.00" productCode="6N"/>
                  //</forwardPoints>

                    var fp = nav.SelectSingleNode("datasuite/forwardPoints");

                    var sb = new StringBuilder();
                    var tradeDate = DateTime.Parse(fp.GetAttribute("tradeDate", ""), System.Globalization.CultureInfo.GetCultureInfo("en-us"));
                    sb.Append(DateTime.Now.ToString());
                    sb.Append(";");
                    sb.Append(tradeDate.ToShortDateString());

                    var iv = System.Globalization.CultureInfo.InvariantCulture;

                    foreach (string productCode in productCodes)
                    {
                        sb.Append(";");
                        var p = fp.SelectSingleNode("*[@productCode='" + productCode + "']");
                        sb.Append(decimal.Parse(p.GetAttribute("bid", ""), iv));
                        sb.Append(";");
                        sb.Append(decimal.Parse(p.GetAttribute("offer", ""), iv));
                    }
                    sb.Append(Environment.NewLine);

                    File.AppendAllText(forwardPointsFile, sb.ToString(), Encoding.GetEncoding(1251));
                    forwardPointsUpdated = true;
                    break;
                }
                else if (isCancelled())
                    break;
                else
                    Thread.Sleep(15000);
            }
        }

        internal override IEnumerable<QueuedTask> OnAfterCompleted(out Notification notification)
        {
            notification = null;

            if (error != null)
            {
                notification = new Notification("Ошибка проверки Forward points", error.Message, true, Resources.AgentError);
            }
            else if (forwardPointsUpdated)
            {
                notification = new Notification("Forward points", "выполненно обновление Forward points", false, Resources.AgentHasNews);
            }

            var newTasks = new List<QueuedTask>();
            newTasks.Add(new CheckForwardPoints(DateTime.Now.AddHours(4), logger));
            return newTasks;
        }
    }

}
