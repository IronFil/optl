﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Threading;

namespace AgentCoin
{
    static class Program
    {
        //public static bool isRunning = true;

        public static string DbDownloadsPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Downloads\DB\");

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            //Application.Run(new MainForm());

            Application.Run(new MainClass());

            //var mc = new MainClass();
            //do
            //{
            //    Application.DoEvents();
            //    System.Threading.Thread.Sleep(0);
            //} while (isRunning);
        }
    }
}
