﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace AgentCoin
{
    class Logger : IDisposable
    {
        StreamWriter sw;

        public Logger(string logFile)
        {
            sw = File.AppendText(logFile);
        }

        public void LogMessage(string message)
        {
            sw.Write("[" + DateTime.Now.ToLongTimeString() + "]");
            sw.WriteLine(message);
            sw.WriteLine();
            sw.Flush();
        }

        public void LogException(Exception exception)
        {
            sw.Write("[" + DateTime.Now.ToLongTimeString() + "]");
            sw.WriteLine(exception.ToString());
            sw.WriteLine();
            sw.Flush();
        }

        public void Dispose()
        {
            sw.Dispose();
        }
    }
}
