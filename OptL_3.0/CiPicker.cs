﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OptL
{
    public partial class CiPicker : Form
    {
        public CiPicker()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            foreach (var ci in CurrencyInfo.AvailableCurrencyInfoList)
                CurrencyInfoCheckedListBox.Items.Add(ci);
        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }

        private void DoneButton_Click(object sender, EventArgs e)
        {
            if (CurrencyInfoCheckedListBox.CheckedItems.Count == 0)
                MessageBox.Show("Нужно выбрать торговые инструменты");
            else
                DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        public IEnumerable<CurrencyInfo> Picked
        {
            get
            {
                foreach (var item in CurrencyInfoCheckedListBox.CheckedItems)
                {
                    yield return (CurrencyInfo)item;
                }
                
            }
        }

        bool updating = false;
        private void CurrencyInfoCheckedListBox_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (updating) return;
            updating = true;
            var ci = CurrencyInfoCheckedListBox.Items[e.Index] as CurrencyInfo;
            for (int i = 0; i < CurrencyInfoCheckedListBox.Items.Count; i++)
            {
                var ci2 = CurrencyInfoCheckedListBox.Items[i] as CurrencyInfo;
                if (ci2.CallSection == ci.CallSection || ci2.PutSection == ci.PutSection)
                    CurrencyInfoCheckedListBox.SetItemCheckState(i, e.NewValue);
            }
            updating = false;
        }
    }
}
