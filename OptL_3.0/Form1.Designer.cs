﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

namespace OptL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.CurrencyComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SpotCheckBox = new System.Windows.Forms.CheckBox();
            this.GeneratorsListView = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.LevelsTextBox = new System.Windows.Forms.TextBox();
            this.LevelsGeneratorSettingsTabPage = new System.Windows.Forms.TabPage();
            this.LevelsGeneratorPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.CurrencyPropertyGrid = new System.Windows.Forms.PropertyGrid();
            this.UpdateTemplateButton = new System.Windows.Forms.Button();
            this.SaveTemplateButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.autoRemember = new System.Windows.Forms.CheckBox();
            this.StrikeExplorer = new System.Windows.Forms.LinkLabel();
            this.templateOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.DBListView = new System.Windows.Forms.DDListView();
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.LevelsGeneratorSettingsTabPage.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddButton
            // 
            this.AddButton.Location = new System.Drawing.Point(6, 19);
            this.AddButton.Name = "AddButton";
            this.AddButton.Size = new System.Drawing.Size(71, 23);
            this.AddButton.TabIndex = 1;
            this.AddButton.Text = "Добавить";
            this.AddButton.UseVisualStyleBackColor = true;
            this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(83, 19);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(71, 23);
            this.removeButton.TabIndex = 2;
            this.removeButton.Text = "Убрать";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.removeButton_Click);
            // 
            // CurrencyComboBox
            // 
            this.CurrencyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CurrencyComboBox.FormattingEnabled = true;
            this.CurrencyComboBox.Location = new System.Drawing.Point(6, 201);
            this.CurrencyComboBox.Name = "CurrencyComboBox";
            this.CurrencyComboBox.Size = new System.Drawing.Size(105, 21);
            this.CurrencyComboBox.TabIndex = 4;
            this.CurrencyComboBox.SelectedIndexChanged += new System.EventHandler(this.CurrencyComboBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 185);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Валюта";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.DefaultExt = "tpl";
            this.saveFileDialog1.FileName = "template.tpl";
            this.saveFileDialog1.Filter = "Файлы шаблонов|*.tpl";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "zip";
            this.openFileDialog1.Filter = "CME Daily Bulletin|*.zip";
            this.openFileDialog1.Multiselect = true;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(602, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Запомнить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.SpotCheckBox);
            this.groupBox2.Controls.Add(this.GeneratorsListView);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.CurrencyComboBox);
            this.groupBox2.Controls.Add(this.UpdateTemplateButton);
            this.groupBox2.Controls.Add(this.SaveTemplateButton);
            this.groupBox2.Location = new System.Drawing.Point(687, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(304, 516);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Расчёт уровней";
            // 
            // SpotCheckBox
            // 
            this.SpotCheckBox.AutoSize = true;
            this.SpotCheckBox.Checked = true;
            this.SpotCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.SpotCheckBox.Location = new System.Drawing.Point(225, 12);
            this.SpotCheckBox.Name = "SpotCheckBox";
            this.SpotCheckBox.Size = new System.Drawing.Size(73, 17);
            this.SpotCheckBox.TabIndex = 14;
            this.SpotCheckBox.Text = "Для спот";
            this.SpotCheckBox.UseVisualStyleBackColor = true;
            this.SpotCheckBox.CheckedChanged += new System.EventHandler(this.SpotCheckBox_CheckedChanged);
            // 
            // GeneratorsListView
            // 
            this.GeneratorsListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GeneratorsListView.CheckBoxes = true;
            this.GeneratorsListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5});
            this.GeneratorsListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.GeneratorsListView.HideSelection = false;
            this.GeneratorsListView.Location = new System.Drawing.Point(6, 32);
            this.GeneratorsListView.MultiSelect = false;
            this.GeneratorsListView.Name = "GeneratorsListView";
            this.GeneratorsListView.Size = new System.Drawing.Size(292, 150);
            this.GeneratorsListView.TabIndex = 13;
            this.GeneratorsListView.UseCompatibleStateImageBehavior = false;
            this.GeneratorsListView.View = System.Windows.Forms.View.Details;
            this.GeneratorsListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.GeneratorsListView_ItemChecked);
            this.GeneratorsListView.SelectedIndexChanged += new System.EventHandler(this.GeneratorsListView_SelectedIndexChanged);
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Путь";
            this.columnHeader5.Width = 250;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Генераторы уровней";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.LevelsGeneratorSettingsTabPage);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(6, 228);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(292, 276);
            this.tabControl1.TabIndex = 11;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.LevelsTextBox);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(284, 250);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Уровни";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // LevelsTextBox
            // 
            this.LevelsTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LevelsTextBox.BackColor = System.Drawing.SystemColors.Window;
            this.LevelsTextBox.Location = new System.Drawing.Point(3, 6);
            this.LevelsTextBox.Multiline = true;
            this.LevelsTextBox.Name = "LevelsTextBox";
            this.LevelsTextBox.ReadOnly = true;
            this.LevelsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.LevelsTextBox.Size = new System.Drawing.Size(275, 238);
            this.LevelsTextBox.TabIndex = 8;
            this.LevelsTextBox.Tag = "X";
            this.LevelsTextBox.Text = "OptL v3.17";
            // 
            // LevelsGeneratorSettingsTabPage
            // 
            this.LevelsGeneratorSettingsTabPage.Controls.Add(this.LevelsGeneratorPropertyGrid);
            this.LevelsGeneratorSettingsTabPage.Location = new System.Drawing.Point(4, 22);
            this.LevelsGeneratorSettingsTabPage.Name = "LevelsGeneratorSettingsTabPage";
            this.LevelsGeneratorSettingsTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.LevelsGeneratorSettingsTabPage.Size = new System.Drawing.Size(284, 250);
            this.LevelsGeneratorSettingsTabPage.TabIndex = 1;
            this.LevelsGeneratorSettingsTabPage.Text = "Настройки генератора";
            this.LevelsGeneratorSettingsTabPage.UseVisualStyleBackColor = true;
            // 
            // LevelsGeneratorPropertyGrid
            // 
            this.LevelsGeneratorPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LevelsGeneratorPropertyGrid.Location = new System.Drawing.Point(3, 3);
            this.LevelsGeneratorPropertyGrid.Name = "LevelsGeneratorPropertyGrid";
            this.LevelsGeneratorPropertyGrid.Size = new System.Drawing.Size(278, 244);
            this.LevelsGeneratorPropertyGrid.TabIndex = 0;
            this.LevelsGeneratorPropertyGrid.ToolbarVisible = false;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.CurrencyPropertyGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(284, 250);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Настройки валюты";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // CurrencyPropertyGrid
            // 
            this.CurrencyPropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CurrencyPropertyGrid.Location = new System.Drawing.Point(0, 0);
            this.CurrencyPropertyGrid.Name = "CurrencyPropertyGrid";
            this.CurrencyPropertyGrid.PropertySort = System.Windows.Forms.PropertySort.Categorized;
            this.CurrencyPropertyGrid.Size = new System.Drawing.Size(284, 250);
            this.CurrencyPropertyGrid.TabIndex = 1;
            this.CurrencyPropertyGrid.ToolbarVisible = false;
            // 
            // UpdateTemplateButton
            // 
            this.UpdateTemplateButton.Location = new System.Drawing.Point(135, 188);
            this.UpdateTemplateButton.Name = "UpdateTemplateButton";
            this.UpdateTemplateButton.Size = new System.Drawing.Size(73, 34);
            this.UpdateTemplateButton.TabIndex = 2;
            this.UpdateTemplateButton.Text = "Обновить шаблон";
            this.UpdateTemplateButton.UseVisualStyleBackColor = true;
            this.UpdateTemplateButton.Click += new System.EventHandler(this.UpdateTemplateButton_Click);
            // 
            // SaveTemplateButton
            // 
            this.SaveTemplateButton.Location = new System.Drawing.Point(214, 188);
            this.SaveTemplateButton.Name = "SaveTemplateButton";
            this.SaveTemplateButton.Size = new System.Drawing.Size(84, 34);
            this.SaveTemplateButton.TabIndex = 2;
            this.SaveTemplateButton.Text = "Создать шаблон";
            this.SaveTemplateButton.UseVisualStyleBackColor = true;
            this.SaveTemplateButton.Click += new System.EventHandler(this.SaveTemplateButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(280, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 13);
            this.label4.TabIndex = 16;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.autoRemember);
            this.groupBox1.Controls.Add(this.StrikeExplorer);
            this.groupBox1.Controls.Add(this.AddButton);
            this.groupBox1.Controls.Add(this.removeButton);
            this.groupBox1.Controls.Add(this.DBListView);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(2, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(679, 516);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Загруженные бюллетени CME";
            // 
            // autoRemember
            // 
            this.autoRemember.AutoSize = true;
            this.autoRemember.Location = new System.Drawing.Point(160, 23);
            this.autoRemember.Name = "autoRemember";
            this.autoRemember.Size = new System.Drawing.Size(168, 17);
            this.autoRemember.TabIndex = 15;
            this.autoRemember.Text = "Запоминать автоматически";
            this.autoRemember.UseVisualStyleBackColor = true;
            // 
            // StrikeExplorer
            // 
            this.StrikeExplorer.AutoSize = true;
            this.StrikeExplorer.LinkColor = System.Drawing.SystemColors.MenuHighlight;
            this.StrikeExplorer.Location = new System.Drawing.Point(466, 24);
            this.StrikeExplorer.Name = "StrikeExplorer";
            this.StrikeExplorer.Size = new System.Drawing.Size(130, 13);
            this.StrikeExplorer.TabIndex = 14;
            this.StrikeExplorer.TabStop = true;
            this.StrikeExplorer.Text = "Проводник по страйкам";
            this.StrikeExplorer.VisitedLinkColor = System.Drawing.SystemColors.MenuHighlight;
            this.StrikeExplorer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.StrikeExplorer_LinkClicked);
            // 
            // templateOpenFileDialog
            // 
            this.templateOpenFileDialog.DefaultExt = "tpl";
            this.templateOpenFileDialog.Filter = "Файлы шаблонов|*.tpl";
            // 
            // DBListView
            // 
            this.DBListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.DBListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2,
            this.columnHeader1,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader6});
            this.DBListView.FullRowSelect = true;
            this.DBListView.GridLines = true;
            this.DBListView.HideSelection = false;
            this.DBListView.Location = new System.Drawing.Point(6, 48);
            this.DBListView.Name = "DBListView";
            this.DBListView.Size = new System.Drawing.Size(667, 462);
            this.DBListView.TabIndex = 0;
            this.DBListView.UseCompatibleStateImageBehavior = false;
            this.DBListView.View = System.Windows.Forms.View.Details;
            this.DBListView.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.DBListView_ColumnClick);
            this.DBListView.SelectedIndexChanged += new System.EventHandler(this.DBListView_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.DisplayIndex = 1;
            this.columnHeader2.Text = "Forward Points";
            this.columnHeader2.Width = 100;
            // 
            // columnHeader1
            // 
            this.columnHeader1.DisplayIndex = 0;
            this.columnHeader1.Text = "Файл";
            this.columnHeader1.Width = 240;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Дата уровней";
            this.columnHeader3.Width = 88;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Дата торгов";
            this.columnHeader4.Width = 91;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Тип";
            this.columnHeader6.Width = 110;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 522);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.groupBox2);
            this.KeyPreview = true;
            this.MinimumSize = new System.Drawing.Size(760, 400);
            this.Name = "Form1";
            this.Text = "OptL v3.17";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.LevelsGeneratorSettingsTabPage.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.ComboBox CurrencyComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.DDListView DBListView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button SaveTemplateButton;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ListView GeneratorsListView;
        private System.Windows.Forms.TabPage LevelsGeneratorSettingsTabPage;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.PropertyGrid CurrencyPropertyGrid;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PropertyGrid LevelsGeneratorPropertyGrid;
        private System.Windows.Forms.CheckBox SpotCheckBox;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.LinkLabel StrikeExplorer;
        private System.Windows.Forms.Button UpdateTemplateButton;
        private System.Windows.Forms.OpenFileDialog templateOpenFileDialog;
        private System.Windows.Forms.CheckBox autoRemember;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox LevelsTextBox;
    }
}

