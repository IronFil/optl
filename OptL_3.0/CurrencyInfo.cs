﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Xml;

namespace OptL
{
    public class CurrencyInfo : INotifyPropertyChanged
    {
        void OnPropertyChanged(string propertyName)
        {
            var h = PropertyChanged;
            if (h != null)
                h(this, new PropertyChangedEventArgs(propertyName));
        }

        public static List<CurrencyInfo> AvailableCurrencyInfoList {get; private set;}

        [Category("1. Общие"), DisplayName("Forex название"), Description("Название этого торгового инструмента на форекс")]
        [System.ComponentModel.ParenthesizePropertyName]
        public string ForexName { get; private set; }
        [Category("1. Общие"), DisplayName("Тикер фьючерса"), Description("Постоянная часть тикера фьючерса для этого торгового инструмента")]
        public string FuturesTicker { get; private set; }
        [Category("1. Общие"), DisplayName("Обратная пара"), Description("Если True (истина), то это обратная пара (как USDCAD), иначе False (как GBPUSD).")]
        public bool IsReverse { get; private set; }
        [Category("1. Общие"), DisplayName("Точность фьючерса"), Description("Кол-во знаков после запятой в котировке фьючерса")]
        public int FuturesPrecision { get; private set; }
        [Category("4. PDF"), DisplayName("Контракт CALL"), Description("Название контракта CALL в DailyBulletin")]
        public string CallContractName { get; private set; }
        [Category("4. PDF"), DisplayName("Контракт PUT"), Description("Название контракта PUT в DailyBulletin")]
        public string PutContractName { get; private set; }
        [Category("4. PDF"), DisplayName("Секция CALL"), Description("Код секции, содержащей контракты CALL в DailyBulletin")]
        public int CallSection { get; private set; }
        [Category("4. PDF"), DisplayName("Секция PUT"), Description("Код секции, содержащей контракты PUT в DailyBulletin")]
        public int PutSection { get; private set; }
        [Category("4. PDF"), DisplayName("Секция фьючерсов"), Description("Код секции, содержащей фьючерсы в DailyBulletin")]
        public int FutSection { get; private set; }
        [Category("4. PDF"), DisplayName("Фьючерс"), Description("Название раздела с фьючерсами в DailyBulletin")]
        public string FuturesName { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель страйка"), Description("Значение, на которое нужно умножить страйк, чтобы получить его цену в пипках.")]
        public decimal StrikeMultiplier { get; private set; }
        [Category("4. PDF"), DisplayName("Шаг страйков"), Description("Значение, которое нужно прибавить к страйку, чтобы получить следующий страйк.")]
        public int StrikeStep { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель премии"), Description("Значение, на которое нужно умножить премию, чтобы получить её цену в пипках.")]
        public decimal SettPriceMultiplier { get; private set; }
        [Category("4. PDF"), DisplayName("Множитель forward points"), Description("Значение, на которое нужно умножить froward points, чтобы перевести его в кол-во пипок (4 знака).")]
        public decimal ForwardPointsMultiplier { get; private set; }

        public string FormatFuturesPrice(decimal price)
        {
            return price.ToString("0." + new string('0', FuturesPrecision), System.Globalization.CultureInfo.InvariantCulture);
        }

        public CurrencyInfo(string forexName, string FuturesTicker, string callContractName, string putContractName, string futuresName, int callSection, int putSection, int futSection,
            decimal strikeMultiplier, int strikeStep, decimal settPriceMultiplier, decimal spotMultiplier, bool isReverse, int FuturesPrecision)
        {
            this.ForexName = forexName;
            this.FuturesTicker = FuturesTicker;
            this.CallSection = callSection;
            this.PutSection = putSection;
            this.FutSection = futSection;
            this.CallContractName = callContractName;
            this.PutContractName = putContractName;
            this.FuturesName = futuresName;

            this.StrikeMultiplier = strikeMultiplier;
            this.StrikeStep = strikeStep;
            this.SettPriceMultiplier = settPriceMultiplier;
            this.ForwardPointsMultiplier = spotMultiplier;
            this.IsReverse = isReverse;
            this.FuturesPrecision = FuturesPrecision;
        }

        static CurrencyInfo()
        {
            AvailableCurrencyInfoList = new List<CurrencyInfo>();
            AvailableCurrencyInfoList.Add(new CurrencyInfo("GBP", "6B", "BRIT PND CALL", "BRIT PND PUT", "BRIT PND FUT", 27, 28, 27, 0.001m, 5, 0.01m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("EUR", "6E", "EURO FX CALL", "EURO FX PUT", "EURO FX FUT", 39, 39, 39, 0.001m, 5, 0.001m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("CAD", "6C", "CANADA DLR CALL", "CANADA DLR PUT", "CANADA DLR FUT", 29, 30, 29, 0.001m, 5, 0.01m, 0.0001m, true, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("AUD", "6A", "AUST DLR CALL", "AUST DLR PUT", "AUST DLR FUT", 38, 38, 38, 0.001m, 5, 0.01m, 0.0001m, false, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("JPY", "6J", "JAPAN YEN CALL", "JAPAN YEN PUT", "JAPAN YEN FUT", 33, 34, 33, 0.00001m, 5, 0.0001m, 0.01m, true, 6));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("CHF", "6S", "SWISS FRNC CALL", "SWISS FRNC PUT", "SWISS FRNC FUT", 35, 36, 35, 0.001m, 5, 0.01m, 0.0001m, true, 4));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("GOLD", "GC", "OG CALL", "OG PUT", "COMEX GOLD FUTURES", 64, 64, 62, 1m, 5, 1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("SILVER", "SI", "SO CALL", "SO PUT", "COMEX SILVER FUTURES", 64, 64, 62, 0.01m, 25, 1m, 0.01m, false, 3));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Corn", "ZC", "CORN CALL", "CORN PUT", "CORN FUTURES", 56, 56, 56, 1m, 5, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("SOYBEAN", "ZS", "SOYBEAN CALL", "SOYBEAN PUT", "CORN FUTURES", 57, 57, 57, 1m, 10, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("WHEAT", "ZW", "WHEAT CALL", "WHEAT PUT", "WHEAT FUT", 57, 57, 57, 1m, 5, 0.1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Crude Oil", "CL", "LO CALL", "LO PUT", "LO FUT", 63, 63, 63 /* 61, но пока не поддерживается */, 0.01m, 50, 1m, 1m, false, 2));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("Natural Gas", "NG", "ON CALL", "ON PUT", "ON FUT", 63, 63, 63, 0.001m, 50, 1m, 1m, false, 3));
            AvailableCurrencyInfoList.Add(new CurrencyInfo("E-MINI S&P", "ES", "EMINI S&P CALL", "EMINI S&P PUT", "EMINI S&P FUT", 47, 48, 47, 1m, 5, 1m, 1m, false, 2));
        }

        //public decimal ToPipsMultyplier
        //{
        //    get
        //    {
        //        return this.SettPriceMultiplier * 10000;
        //    }
        //}

        public delegate TResult Func<T, TResult>(T arg);

        private void SetBulletinValue(Func<DBParser, int?> pget,Func<DBParser, CurrencySpecificProperty<int>> f, string propertyName, int? value)
        {
            if (pget(null) != value)
            {
                foreach (var bulletin in selectedBulletins)
                    f(bulletin)[this.ForexName] = value;
                OnPropertyChanged(propertyName);
            }
        }

        private int? GetBulletinValue(Func<DBParser, int?> f)
        {
            bool isFirst = true;
            int? res = null;
            foreach (var bulletin in selectedBulletins)
            {
                if (isFirst)
                {
                    res = f(bulletin);
                    isFirst = false;
                }
                else if (res != f(bulletin))
                {
                    res = null;
                }
            }
            return res;
        }

        //[Category("2. Поправка бюллетеня на спот"), DisplayName("CALL"), Description("Значение в пипках, добавляется к вычисленному CALL уровню. Задаётся для каждого бюллетеня отдельно.")]
        //public int? SpotAdjustmentCall
        //{
        //    get
        //    {
        //        return GetBulletinValue(db=>db.SpotAdjustmentCall[this.ForexName]);
        //    }
        //    set
        //    {
        //        SetBulletinValue(d => SpotAdjustmentCall, db => db.SpotAdjustmentCall, "SpotAdjustmentCall", value);
        //    }
        //}

        //[Category("2. Поправка бюллетеня на спот"), DisplayName("PUT"), Description("Значение в пипках, добавляется к вычисленному PUT уровню. Задаётся для каждого бюллетеня отдельно.")]
        //public int? SpotAdjustmentPut
        //{
        //    get
        //    {
        //        return GetBulletinValue(db => db.SpotAdjustmentPut[this.ForexName]);
        //    }
        //    set
        //    {
        //        SetBulletinValue(d => SpotAdjustmentPut, db => db.SpotAdjustmentPut, "SpotAdjustmentPut", value);
        //    }
        //}

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("Forward Points"), Description("Значение форвард пойнт в пипках (4 знак). Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsForwardPoints
        {
            get
            {
                return GetBulletinValue(db => db.ForwardPoints[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsForwardPoints, db => db.ForwardPoints, "ForwardPoints", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("ОИ минимум"), Description("Страйки с ОИ меньше данного не будут учитываться. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMinOi
        {
            get
            {
                return GetBulletinValue(db => db.MinOi[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMinOi, db => db.MinOi, "MinOi", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("ОИ максимум"), Description("Страйки с ОИ больше данного не будут учитываться. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMaxOi
        {
            get
            {
                return GetBulletinValue(db => db.MaxOi[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMaxOi, db => db.MaxOi, "MaxOi", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("Изменение ОИ минимум"), Description("Страйки с изменением ОИ меньше данного не будут учитываться. Можно указывать отрицательные значения. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMinOiCh
        {
            get
            {
                return GetBulletinValue(db => db.MinOiCh[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMinOiCh, db => db.MinOiCh, "MinOiCh", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("Изменение ОИ максимум"), Description("Страйки с изменением ОИ больше данного не будут учитываться. Можно указывать отрицательные значения. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMaxOiCh
        {
            get
            {
                return GetBulletinValue(db => db.MaxOiCh[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMaxOiCh, db => db.MaxOiCh, "MaxOiCh", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("Объём минимум"), Description("Страйки с объёмом меньше данного не будут учитываться. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMinVol
        {
            get
            {
                return GetBulletinValue(db => db.MinVol[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMinVol, db => db.MinVol, "MinVol", value);
            }
        }

        [Category("2. Параметры выделенных бюллетеней"), DisplayName("Объём максимум"), Description("Страйки с объёмом больше данного не будут учитываться. Задаётся для каждого бюллетеня отдельно.")]
        public int? SelectedBulletinsMaxVol
        {
            get
            {
                return GetBulletinValue(db => db.MaxVol[this.ForexName]);
            }
            set
            {
                SetBulletinValue(d => SelectedBulletinsMaxVol, db => db.MaxVol, "MaxVol", value);
            }
        }

        IEnumerable<DBParser> selectedBulletins;

        public void SetSelectedBulletins(IEnumerable<DBParser> selectedBulletins)
        {
            this.selectedBulletins = selectedBulletins;                        
        }

        [Category("3. Параметры новых бюллетеней"), DisplayName("ОИ новых DB минимум"), Description("Значение поля ''ОИ минимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMinOi { get; set; }

        [Category("3. Параметры новых бюллетеней"), DisplayName("ОИ новых DB максимум"), Description("Значение поля ''ОИ максимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMaxOi { get; set; }

        [Category("3. Параметры новых бюллетеней"), DisplayName("Изменение ОИ новых DB минимум"), Description("Значение поля ''Изменение ОИ минимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMinOiCh { get; set; }

        [Category("3. Параметры новых бюллетеней"), DisplayName("Изменение ОИ новых DB максимум"), Description("Значение поля ''Изменение ОИ максимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMaxOiCh { get; set; }

        [Category("3. Параметры новых бюллетеней"), DisplayName("Объём новых DB минимум"), Description("Значение поля ''Объём минимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMinVol { get; set; }

        [Category("3. Параметры новых бюллетеней"), DisplayName("Объём новых DB максимум"), Description("Значение поля ''Объём максимум'' новых бюллетений.")]
        [DefaultValue(0)]
        public int NewDbMaxVol { get; set; }

        public override string ToString()
        {
            return ForexName;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal void SaveSettings(System.Xml.XmlDocument doc, List<DBParser> dbs)
        {
            var settings = doc.CreateElement("CurrencyInfo");

            settings.SetAttribute("ForexName", this.ForexName);
            settings.SetAttribute("NewDbMaxOi", this.NewDbMaxOi.ToString());
            settings.SetAttribute("NewDbMinOi", this.NewDbMinOi.ToString());
            settings.SetAttribute("NewDbMaxOiCh", this.NewDbMaxOiCh.ToString());
            settings.SetAttribute("NewDbMinOiCh", this.NewDbMinOiCh.ToString());
            settings.SetAttribute("NewDbMaxVol", this.NewDbMaxVol.ToString());
            settings.SetAttribute("NewDbMinVol", this.NewDbMinVol.ToString());

            foreach (var db in dbs)
            {
                var dateSpecific = doc.CreateElement("DateSpecific");

                int? v;

                v = db.ForwardPoints[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("ForwardPoints", v.ToString());

                //int? v = db.SpotAdjustmentCall[this.ForexName];
                //if (v.HasValue) dateSpecific.SetAttribute("SpotAdjustmentCall", v.ToString());
                
                //v = db.SpotAdjustmentPut[this.ForexName];
                //if (v.HasValue) dateSpecific.SetAttribute("SpotAdjustmentPut", v.ToString());
                
                v = db.MinOi[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MinOi", v.ToString());
                
                v = db.MaxOi[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MaxOi", v.ToString());

                v = db.MinOiCh[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MinOiCh", v.ToString());

                v = db.MaxOiCh[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MaxOiCh", v.ToString());

                v = db.MinVol[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MinVol", v.ToString());

                v = db.MaxVol[this.ForexName];
                if (v.HasValue) dateSpecific.SetAttribute("MaxVol", v.ToString());

                if (dateSpecific.Attributes.Count > 0)
                {
                    dateSpecific.SetAttribute("Date", System.Xml.XmlConvert.ToString(db.Date, System.Xml.XmlDateTimeSerializationMode.Unspecified));
                    settings.AppendChild(dateSpecific);
                }
            }

            doc.DocumentElement["CurrencyInfoSettings"].AppendChild(settings);
        }

        internal void LoadSettings(XmlElement settings, Dictionary<DateTime, List<DBParser>> parsers)
        {
            this.NewDbMaxOi = int.Parse(settings.GetAttribute("NewDbMaxOi"));
            this.NewDbMinOi = int.Parse(settings.GetAttribute("NewDbMinOi"));
            this.NewDbMaxOiCh = int.Parse(settings.GetAttribute("NewDbMaxOiCh"));
            this.NewDbMinOiCh = int.Parse(settings.GetAttribute("NewDbMinOiCh"));
            this.NewDbMaxVol = int.Parse(settings.GetAttribute("NewDbMaxVol"));
            this.NewDbMinVol = int.Parse(settings.GetAttribute("NewDbMinVol"));

            foreach (XmlElement xeDateSpecific in settings.SelectNodes("DateSpecific"))
            {
                var date = XmlConvert.ToDateTime(xeDateSpecific.GetAttribute("Date"), XmlDateTimeSerializationMode.Unspecified);

                if (!parsers.ContainsKey(date))
                    continue;

                //var spotAdjustmentCall = GetValue(xeDateSpecific.GetAttribute("SpotAdjustmentCall"));
                //var spotAdjustmentPut = GetValue(xeDateSpecific.GetAttribute("SpotAdjustmentPut"));
                var forwardPoints = GetValue(xeDateSpecific.GetAttribute("ForwardPoints"));
                var MaxOi = GetValue(xeDateSpecific.GetAttribute("MaxOi"));
                var MinOi = GetValue(xeDateSpecific.GetAttribute("MinOi"));
                var MaxOiCh = GetValue(xeDateSpecific.GetAttribute("MaxOiCh"));
                var MinOiCh = GetValue(xeDateSpecific.GetAttribute("MinOiCh"));
                var MaxVol = GetValue(xeDateSpecific.GetAttribute("MaxVol"));
                var MinVol = GetValue(xeDateSpecific.GetAttribute("MinVol"));

                foreach (var db in parsers[date])
                {
                    //db.SpotAdjustmentCall[ForexName] = spotAdjustmentCall;
                    //db.SpotAdjustmentPut[ForexName] = spotAdjustmentPut;
                    db.ForwardPoints[ForexName] = db.ForwardPoints[this.ForexName] = forwardPoints;
                    db.MinOi[ForexName] = MinOi;
                    db.MaxOi[ForexName] = MaxOi;
                    db.MinOiCh[ForexName] = MinOiCh;
                    db.MaxOiCh[ForexName] = MaxOiCh;
                    db.MinVol[ForexName] = MinVol;
                    db.MaxVol[ForexName] = MaxVol;
                }
            }
        }

        int? GetValue(string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            else
                return int.Parse(value);
        }

    }
}
