﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;
using OptL;

namespace MT4Tools
{
    public class TemplateReader : XmlReader
    {
        string[] lines;
        public int lineN = 0;
        
        ReadState state;

        string localName;
        XmlNodeType nodeType = XmlNodeType.None;

        string currentValue;

        enum SimpleElementState
        {
            none, content, end
        }

        SimpleElementState readingSimpleElementState = SimpleElementState.none;

        //List<KeyValuePair<string, string>> attributes;
        //int currentAttribute = -1;

        private string CurrentLine { get { return lineN >= lines.Length ? null : lines[lineN]; } }

        public TemplateReader(string templatePath)
        {
            lines = File.ReadAllLines(templatePath, Encoding.GetEncoding(1251));
            state = System.Xml.ReadState.Initial;
        }

        public override int AttributeCount
        {
            get { return 0; }
        }

        public override string BaseURI
        {
            get { return ""; }
        }

        public override void Close()
        {
            throw new NotImplementedException();
        }

        public override int Depth
        {
            get { throw new NotImplementedException(); }
        }

        public override bool EOF
        {
            get { throw new NotImplementedException(); }
        }

        public override string GetAttribute(int i)
        {
            throw new NotImplementedException();
        }

        public override string GetAttribute(string name, string namespaceURI)
        {
            throw new NotImplementedException();
        }

        public override string GetAttribute(string name)
        {
            throw new NotImplementedException();
        }

        public override bool HasValue
        {
            get { throw new NotImplementedException(); }
        }

        public override bool IsEmptyElement
        {
            get { return false; }
        }

        public override string LocalName
        {
            get
            {
                return localName;                
            }
        }

        public override string LookupNamespace(string prefix)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToAttribute(string name, string ns)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToAttribute(string name)
        {
            throw new NotImplementedException();
        }

        public override bool MoveToElement()
        {
            nodeType = XmlNodeType.Element;
            return true;
        }

        public override bool MoveToFirstAttribute()
        {
            return false;

            //currentAttribute = 0;
            //localName = attributes[currentAttribute].Key;
            //nodeType = XmlNodeType.Attribute;
            //return attributes.Count > 0;
        }

        public override bool MoveToNextAttribute()
        {
            return false;
            //currentAttribute++;
            //if (currentAttribute >= attributes.Count)
            //    return false;
                        
            //localName = attributes[currentAttribute].Key;
            //nodeType = XmlNodeType.Attribute;
            //return true;
        }

        public override XmlNameTable NameTable
        {
            get { throw new NotImplementedException(); }
        }

        public override string NamespaceURI
        {
            get { return string.Empty; }
        }

        public override XmlNodeType NodeType
        {
            get
            {
                return nodeType;
            }
        }

        public override string Prefix
        {
            get { return string.Empty; }
        }

        public override bool Read()
        {
            state = System.Xml.ReadState.Interactive;
            var currentLine = CurrentLine;

            if (currentLine == null)
                return false;

            if (currentLine.StartsWith("</"))
            {
                nodeType = XmlNodeType.EndElement;
                lineN++;
            }
            else if (currentLine.StartsWith("<"))
            {
                localName = currentLine.Substring(1, currentLine.Length - 2);
                //attributes = new List<KeyValuePair<string, string>>();

                //for (; ;)
                //{
                //    lineN++;
                //    currentLine = CurrentLine;

                //    int eqPos = CurrentLine.IndexOf('=');
                //    if (eqPos == -1)
                //        break;
                //    attributes.Add(new KeyValuePair<string, string>(
                //        currentLine.Substring(0, eqPos), currentLine.Substring(eqPos + 1, currentLine.Length - eqPos - 1)));
                //}
                nodeType = XmlNodeType.Element;
                lineN++;
            }
            else if (currentLine.Contains("="))
            {
                switch (readingSimpleElementState)
                {
                    case SimpleElementState.none:
                        nodeType = XmlNodeType.Element;
                        localName = currentLine.Substring(0, currentLine.IndexOf('='));
                        readingSimpleElementState = SimpleElementState.content;
                        break;
                    case SimpleElementState.content:
                        nodeType = XmlNodeType.Text;
                        readingSimpleElementState = SimpleElementState.end;
                        currentValue = currentLine.Substring(currentLine.IndexOf('=')+1);
                        break;
                    case SimpleElementState.end:
                        readingSimpleElementState = SimpleElementState.none;
                        nodeType = XmlNodeType.EndElement;
                        lineN++;
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }
            else if (string.IsNullOrEmpty(currentLine))
            {
                nodeType = XmlNodeType.Whitespace;
                lineN++;
            }
            else
                throw new Exception();
            return true;
        }

        public override bool ReadAttributeValue()
        {
            return false;
            //if (nodeType == XmlNodeType.Text)
            //    return false;

            //nodeType = XmlNodeType.Text;
            //currentValue = attributes[currentAttribute].Value;
            //return true;
        }

        public override ReadState ReadState
        {
            get { return state; }
        }

        public override void ResolveEntity()
        {
            throw new NotImplementedException();
        }

        public override string Value
        {
            get { return currentValue; }
        }
    }

    public class TemplateWriter : XmlWriter
    {
        private TextWriter tw;
        private WriteState state = WriteState.Start;

        Stack<string> openElements = new Stack<string>();
        string simpleElementName = null;

        public TemplateWriter(TextWriter tw)
        {
            this.tw = tw;
        }
        public override void Close()
        {
            throw new NotImplementedException();
        }

        public override void Flush()
        {
            tw.Flush();
        }

        public override string LookupPrefix(string ns)
        {
            throw new NotImplementedException();
        }

        public override void WriteBase64(byte[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteCData(string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteCharEntity(char ch)
        {
            throw new NotImplementedException();
        }

        public override void WriteChars(char[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteComment(string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteDocType(string name, string pubid, string sysid, string subset)
        {
            throw new NotImplementedException();
        }

        public override void WriteEndAttribute()
        {
            tw.WriteLine();
        }

        public override void WriteEndDocument()
        {
            throw new NotImplementedException();
        }

        public override void WriteEndElement()
        {
            throw new NotImplementedException();
        }

        public override void WriteEntityRef(string name)
        {
            throw new NotImplementedException();
        }

        public override void WriteFullEndElement()
        {
            if (simpleElementName != null)
                simpleElementName = null;
            else
                tw.WriteLine("</" + openElements.Pop() + ">");
        }

        public override void WriteProcessingInstruction(string name, string text)
        {
            throw new NotImplementedException();
        }

        public override void WriteRaw(string data)
        {
            throw new NotImplementedException();
        }

        public override void WriteRaw(char[] buffer, int index, int count)
        {
            throw new NotImplementedException();
        }

        public override void WriteStartAttribute(string prefix, string localName, string ns)
        {
            tw.Write(localName + "=");
        }

        public override void WriteStartDocument(bool standalone)
        {
            throw new NotImplementedException();
        }

        public override void WriteStartDocument()
        {
            
        }

        public override void WriteStartElement(string prefix, string localName, string ns)
        {
            if (simpleElementName != null)
            {
                tw.WriteLine("<" + simpleElementName + ">");
                openElements.Push(simpleElementName);
                simpleElementName = null;
            }

            simpleElementName = localName;
        }

        public override WriteState WriteState
        {
            get { return state; }
        }

        public override void WriteString(string text)
        {
            if (simpleElementName == null)
                throw new Exception();
            tw.WriteLine(simpleElementName+"="+text);
        }

        public override void WriteSurrogateCharEntity(char lowChar, char highChar)
        {
            throw new NotImplementedException();
        }

        public override void WriteWhitespace(string ws)
        {
            throw new NotImplementedException();
        }
    }

    public class TemplateBuilder : ITemplateBuilder
    {
        public const string SecondWindowName = "Standard Deviation";

        static DateTime _baseDate = new DateTime(1970, 1, 1);

        XmlDocument owner;
        //XmlElement template;
        public XmlElement indicatorPrimary;
        public XmlElement indicatorSecondary;

        private XmlElement secondaryWindow;

        int objectId = 1;

        private Dictionary<string, XmlElement> Templates = new Dictionary<string, XmlElement>();

        CurrencyInfo ci;
        bool spot;

        public TemplateBuilder(XmlDocument owner, CurrencyInfo ci, bool spot)
        {
            this.owner = owner;

            this.ci = ci;
            this.spot = spot;

            indicatorPrimary = (XmlElement)owner.SelectSingleNode("/chart/window/indicator[name='main']");
            indicatorSecondary = (XmlElement)owner.SelectSingleNode("/chart/window/indicator[name='" + SecondWindowName + "']");

            secondaryWindow = (XmlElement)owner.SelectSingleNode("/chart/window[indicator/name='" + SecondWindowName + "']");
            owner.FirstChild.RemoveChild(secondaryWindow);

            ProcessElementTemplates(indicatorPrimary);
            ProcessElementTemplates(indicatorSecondary);

            //foreach (var xe in Templates.Values)
            //    if (!xe.GetAttribute("object_name").Contains("TST"))
            //    xe.ParentNode.RemoveChild(xe);


            //indicator.RemoveChild(callTemplate = (XmlElement)indicator.SelectSingleNode("object[@object_name='CALL']"));
            //indicator.RemoveChild(callTemplateCh = (XmlElement)indicator.SelectSingleNode("object[@object_name='CALLCH']"));
            //indicator.RemoveChild(putTemplate = (XmlElement)indicator.SelectSingleNode("object[@object_name='PUT']"));
            //indicator.RemoveChild(putTemplateCh = (XmlElement)indicator.SelectSingleNode("object[@object_name='PUTCH']"));
            //indicatorNode.RemoveChild(weekDayTemplate = (XmlElement)indicatorNode.SelectSingleNode("object[@object_name='WeekDay']"));
        }

        private void ProcessElementTemplates(XmlElement indicator)
        {
            var toRemove = new List<XmlElement>();
            foreach (var obj in indicator.ChildNodes)
            {
                var xe = obj as XmlElement;
                if (xe.LocalName != "object") continue;
                Templates.Add(xe.SelectSingleNode("object_name").InnerText, xe);
                toRemove.Add(xe);
            }

            foreach (var xe in toRemove)
                indicator.RemoveChild(xe);
        }

        private void EnsureSecondaryWindowPresent()
        {
            owner.FirstChild.AppendChild(secondaryWindow);
        }

        public void AddLabel(DateTime date, string name, string description, string template)
        {
            if (!Templates.ContainsKey(template))
                throw new TemplateNotFound(template);

            EnsureSecondaryWindowPresent();

            var nn = Templates[template].Clone();
            if (nn.SelectSingleNode("description") == null)
                nn.AppendChild(owner.CreateElement("description"));
            nn.SelectSingleNode("description").InnerText = description;

            var node = nn.SelectSingleNode("time_0");
            var t = GetNodeTime(node).Value;
            SetNodeTime(node, date.Add(new TimeSpan(t.Hour, t.Minute, t.Second)));

            indicatorSecondary.AppendChild(nn);
            nn.SelectSingleNode("object_name").InnerText = "OptL" + objectId + "_" + name;
            objectId++;
        }

        public static void SetNodeTime(XmlNode xmlNode, DateTime date)
        {
            xmlNode.InnerText = (date.Subtract(_baseDate).TotalSeconds).ToString();
        }

        public static DateTime? GetNodeTime(XmlNode timeNode)
        {
            return timeNode == null ? (DateTime?)null : _baseDate.AddSeconds(double.Parse(timeNode.InnerText));
        }

        public void AddBar(float fractHeight, DateTime pos, string name, string description, string template)
        {
            if (!Templates.ContainsKey(template))
                throw new TemplateNotFound(template);

            EnsureSecondaryWindowPresent();

            var nn = Templates[template].Clone();
            if (nn.SelectSingleNode("description") == null)
                nn.AppendChild(owner.CreateElement("description"));
            nn.SelectSingleNode("description").InnerText = description;

            SetNodeTime(nn.SelectSingleNode("time_0"), pos);
            SetNodeTime(nn.SelectSingleNode("time_1"), pos);

            nn.SelectSingleNode("value_0").InnerText = "0";
            nn.SelectSingleNode("value_1").InnerText = fractHeight.ToString().Replace(",", ".");

            indicatorSecondary.AppendChild(nn);
            nn.SelectSingleNode("object_name").InnerText = "OptL" + objectId + "_" + name;
            objectId++;
        }

        public decimal Level2Spot(decimal level, DBParser spotDb)
        {
            if (!spot)
                return level;

            var forwardPoints = spotDb == null ? 0 : (spotDb.ForwardPoints[ci.ForexName] ?? 0);
            return (ci.IsReverse ? (1M / level) : level) - forwardPoints * ci.ForwardPointsMultiplier;
        }

        public void AddLevel(decimal level0, decimal level1, DateTime start, DBParser spotDb, float fractLength, string name, string description, string template)
        {
            if (fractLength == float.NaN)
                throw new Exception();

            TimeSpan levelLength = TimeSpan.FromMinutes(Math.Max(60, 1440 * fractLength)); // Не меньше 1 часа
            var end =  start.Add(levelLength);
            AddLevel(level0, level1, start, end, spotDb, name, description, template);
        }

        public void AddLevel(decimal level0, decimal level1, DateTime start, DateTime end, DBParser spotDb, string name, string description, string template)
        {
            if (!Templates.ContainsKey(template))
                throw new TemplateNotFound(template);

            level0 = Level2Spot(level0, spotDb);
            level1 = Level2Spot(level1, spotDb);

            var nn = Templates[template].Clone();
            if (nn.SelectSingleNode("description") == null)
                nn.AppendChild(owner.CreateElement("description"));
            nn.SelectSingleNode("description").InnerText = description;
            SetNodeTime(nn.SelectSingleNode("time_0"), start);
            nn.SelectSingleNode("value_0").InnerText = level0.ToString().Replace(",", ".");
            nn.SelectSingleNode("value_1").InnerText = level1.ToString().Replace(",", ".");

            SetNodeTime(nn.SelectSingleNode("time_1"), end);

            indicatorPrimary.AppendChild(nn);
            nn.SelectSingleNode("object_name").InnerText = "OptL" + objectId + "_" + name;
            objectId++;
        }

        public void AddArrow(decimal level0, DateTime time0, DBParser spotDb, string name, string description, string template, int symbolCode)
        {
            if (!Templates.ContainsKey(template))
                throw new TemplateNotFound(template);

            if (spot)
            {
                var forwardPoints = spotDb == null ? 0 : (spotDb.ForwardPoints[ci.ForexName] ?? 0);
                level0 = (ci.IsReverse ? (1M / level0) : level0) - forwardPoints * ci.ForwardPointsMultiplier;
            }

            var nn = Templates[template].Clone();
            if (nn.SelectSingleNode("description") == null)
                nn.AppendChild(owner.CreateElement("description"));
            nn.SelectSingleNode("description").InnerText = description;
            SetNodeTime(nn.SelectSingleNode("time_0"), time0);
            nn.SelectSingleNode("value_0").InnerText = level0.ToString().Replace(",", ".");
            nn.SelectSingleNode("symbol_code").InnerText = symbolCode.ToString();

            indicatorPrimary.AppendChild(nn);
            nn.SelectSingleNode("object_name").InnerText = "OptL" + objectId + "_" + name;
            objectId++;
        }

        public void AddOrder(decimal openPrice, DateTime openTime, decimal closePrice, DateTime closeTime, string description, bool isBuy)
        {
            //var template = isBuy ? "BUY_ORDER" :  "SELL_ORDER";

            //if (!Templates.ContainsKey(template))
            //    throw new TemplateNotFound(template);

            //var nn = Templates[template].Clone();
            //if (nn.Attributes["description"] == null)
            //    nn.Attributes.Append(owner.CreateAttribute("description"));
            //nn.Attributes["description"].Value = description;
            //nn.Attributes["time_0"].Value = openTime.Subtract(baseDate).TotalSeconds.ToString();
            //nn.Attributes["time_1"].Value = closeTime.Subtract(baseDate).TotalSeconds.ToString();
            //nn.Attributes["value_0"].Value = openPrice.ToString().Replace(",", ".");
            //nn.Attributes["value_1"].Value = closePrice.ToString().Replace(",", ".");

            //indicatorPrimary.AppendChild(nn);
            //nn.Attributes["object_name"].Value = "OptL" + objectId + "_" + template;
            //objectId++;
        }

        public float SecondaryMin
        {
            get
            {
                return float.Parse(indicatorSecondary.SelectSingleNode("min").InnerText, System.Globalization.CultureInfo.InvariantCulture);
            }
            set
            {
                indicatorSecondary.SelectSingleNode("min").InnerText = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }

        public float SecondaryMax
        {
            get
            {
                return float.Parse(indicatorSecondary.SelectSingleNode("max").InnerText, System.Globalization.CultureInfo.InvariantCulture);
            }
            set
            {
                indicatorSecondary.SelectSingleNode("max").InnerText = value.ToString(System.Globalization.CultureInfo.InvariantCulture);
            }
        }


        public string GetObjectType(string template)
        {
            var t = Templates[template];
            return t.SelectSingleNode("type").InnerText;
        }
    }

    public class TemplateNotFound : Exception
    {
        public TemplateNotFound(string templateName)
        {
            this.TemplateName = templateName;
        }

        public string TemplateName { get; set; }
    }
}
