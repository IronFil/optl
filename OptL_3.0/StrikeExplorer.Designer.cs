﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

namespace OptL
{
    partial class StrikeExplorer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ContractTreeView = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.StrikeDataGridView = new System.Windows.Forms.DataGridView();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.ChartPanel = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StrikeDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // ContractTreeView
            // 
            this.ContractTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.ContractTreeView.FullRowSelect = true;
            this.ContractTreeView.HideSelection = false;
            this.ContractTreeView.Location = new System.Drawing.Point(0, 0);
            this.ContractTreeView.Name = "ContractTreeView";
            this.ContractTreeView.Size = new System.Drawing.Size(121, 508);
            this.ContractTreeView.TabIndex = 3;
            this.ContractTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.ContractTreeView_AfterSelect);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.StrikeDataGridView);
            this.panel1.Controls.Add(this.splitter1);
            this.panel1.Controls.Add(this.ChartPanel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(121, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(660, 508);
            this.panel1.TabIndex = 5;
            // 
            // StrikeDataGridView
            // 
            this.StrikeDataGridView.AllowUserToAddRows = false;
            this.StrikeDataGridView.AllowUserToDeleteRows = false;
            this.StrikeDataGridView.AllowUserToOrderColumns = true;
            this.StrikeDataGridView.AllowUserToResizeRows = false;
            this.StrikeDataGridView.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.StrikeDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StrikeDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.StrikeDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.StrikeDataGridView.Location = new System.Drawing.Point(0, 0);
            this.StrikeDataGridView.MultiSelect = false;
            this.StrikeDataGridView.Name = "StrikeDataGridView";
            this.StrikeDataGridView.ReadOnly = true;
            this.StrikeDataGridView.RowHeadersVisible = false;
            this.StrikeDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.StrikeDataGridView.ShowCellErrors = false;
            this.StrikeDataGridView.ShowCellToolTips = false;
            this.StrikeDataGridView.ShowEditingIcon = false;
            this.StrikeDataGridView.ShowRowErrors = false;
            this.StrikeDataGridView.Size = new System.Drawing.Size(660, 409);
            this.StrikeDataGridView.TabIndex = 4;
            this.StrikeDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.StrikeDataGridView_CellEnter);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(0, 409);
            this.splitter1.MinSize = 50;
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(660, 8);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // ChartPanel
            // 
            this.ChartPanel.BackColor = System.Drawing.SystemColors.Window;
            this.ChartPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ChartPanel.Location = new System.Drawing.Point(0, 417);
            this.ChartPanel.Name = "ChartPanel";
            this.ChartPanel.Size = new System.Drawing.Size(660, 91);
            this.ChartPanel.TabIndex = 2;
            this.ChartPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.ChartPanel_Paint);
            // 
            // StrikeExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(781, 508);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.ContractTreeView);
            this.Name = "StrikeExplorer";
            this.Text = "StrikeExplorer";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StrikeDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView ContractTreeView;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView StrikeDataGridView;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel ChartPanel;
    }
}