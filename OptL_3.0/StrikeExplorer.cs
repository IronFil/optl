﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CMEDB;

namespace OptL
{
    public partial class StrikeExplorer : Form
    {
        int minStrike = int.MaxValue;
        int maxStrike = int.MinValue;

        DataGridViewTextBoxColumn putDeltaColumn = new DataGridViewTextBoxColumn() { Name = "PUT Delta" };
        DataGridViewTextBoxColumn putSettPriceColumn = new DataGridViewTextBoxColumn() { Name = "PUT SettPrice" };
        DataGridViewTextBoxColumn putVolumeColumn = new DataGridViewTextBoxColumn() { Name = "PUT Volume" };
        DataGridViewTextBoxColumn putOiColumn = new DataGridViewTextBoxColumn() { Name = "PUT OI" };

        DataGridViewTextBoxColumn strikeColumn = new DataGridViewTextBoxColumn() { Name = "Strike" };

        DataGridViewTextBoxColumn callOiColumn = new DataGridViewTextBoxColumn() { Name = "CALL OI" };
        DataGridViewTextBoxColumn callVolumeColumn = new DataGridViewTextBoxColumn() { Name = "CALL Volume" };
        DataGridViewTextBoxColumn callSettPriceColumn = new DataGridViewTextBoxColumn() { Name = "CALL SettPrice" };
        DataGridViewTextBoxColumn callDeltaColumn = new DataGridViewTextBoxColumn() { Name = "CALL Delta" };

        List<double> graphData = new List<double>();

        public StrikeExplorer()
        {
            InitializeComponent();

            StrikeDataGridView.DefaultCellStyle = new DataGridViewCellStyle(StrikeDataGridView.DefaultCellStyle)
            {
                Alignment = DataGridViewContentAlignment.MiddleRight
            };


            StrikeDataGridView.Columns.Add(putDeltaColumn);
            StrikeDataGridView.Columns.Add(putSettPriceColumn);
            StrikeDataGridView.Columns.Add(putVolumeColumn);
            StrikeDataGridView.Columns.Add(putOiColumn);

            StrikeDataGridView.Columns.Add(strikeColumn);
            strikeColumn.CellTemplate.Style.Font = new Font(FontFamily.GenericSansSerif, 10, FontStyle.Bold);
            strikeColumn.CellTemplate.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;

            StrikeDataGridView.Columns.Add(callOiColumn);
            StrikeDataGridView.Columns.Add(callVolumeColumn);
            StrikeDataGridView.Columns.Add(callSettPriceColumn);
            StrikeDataGridView.Columns.Add(callDeltaColumn);
        }

        class StrikePair
        {
            public Strike CallStrike { get; set; }
            public Strike PutStrike { get; set; }
        }

        Dictionary<int, StrikePair> CombineContractStrikes(ContractData call, ContractData put)
        {
            var strikes = new Dictionary<int, StrikePair>();

            foreach (var strike in call.Strikes)
            {
                if (!strikes.ContainsKey(strike.StrikeValue))
                    strikes.Add(strike.StrikeValue, new StrikePair());
                strikes[strike.StrikeValue].CallStrike = strike;
            }
            foreach (var strike in put.Strikes)
            {
                if (!strikes.ContainsKey(strike.StrikeValue))
                    strikes.Add(strike.StrikeValue, new StrikePair());
                strikes[strike.StrikeValue].PutStrike = strike;
            }

            return strikes;
        }

        internal void SetDBs(List<DBParser> dbs, CurrencyInfo ci)
        {
            foreach (var db in dbs)
            {
                var dbNode = ContractTreeView.Nodes.Add(db.Date.ToShortDateString());
                dbNode.Tag = db;

                if (db.Date.DayOfWeek == DayOfWeek.Monday)
                    dbNode.BackColor = SystemColors.ControlLight;

                var contracts = db.GetSection(ci.CallSection).Contracts;
                if (ci.PutSection != ci.CallSection)
                    contracts.AddRange(db.GetSection(ci.PutSection).Contracts);

                var callPutMatch = new Dictionary<string, ContractData>();

                foreach (var contract in contracts)
                {
                    foreach (var strike in contract.Strikes)
                    {
                        minStrike = Math.Min(strike.StrikeValue, minStrike);
                        maxStrike = Math.Max(strike.StrikeValue, maxStrike);
                    }

                    bool isCall = contract.Name == ci.CallContractName;
                    var key = contract.Date.ToString("MMM yy");

                    if (callPutMatch.ContainsKey(key))
                        dbNode.Nodes.Add(key).Tag = CombineContractStrikes(
                            isCall ? contract : callPutMatch[key], isCall ? callPutMatch[key] : contract);
                    else
                        callPutMatch.Add(key, contract);
                }
            }


            for (int i = minStrike; i <= maxStrike; i += ci.StrikeStep)
            {
                int rowIndex = StrikeDataGridView.Rows.Add();
                var row = StrikeDataGridView.Rows[rowIndex];
                row.Cells[strikeColumn.Name].Value = i;
            }

            StrikeDataGridView.Rows[0].Cells[strikeColumn.Name].Selected = true;
        }

        private void ContractTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            var strikes = (e.Node.Level ==1 ? e.Node : e.Node.Nodes[0]).Tag as Dictionary<int, StrikePair>;
            
            foreach (DataGridViewRow row in StrikeDataGridView.Rows)
            {
                int strike = (int)row.Cells[strikeColumn.Name].Value;

                var sp = strikes.ContainsKey(strike) ? strikes[strike] : new StrikePair();

                foreach (DataGridViewCell cell in row.Cells)
                {
                    if (cell.OwningColumn == strikeColumn)
                        cell.Value = strike;
                    else
                        cell.Value = GetCellValue(sp, cell.OwningColumn);
                }
            }

            UpdateChart();
        }

        private object GetCellValue(StrikePair sp, DataGridViewColumn column)
        {
            object res;
            if (column == callOiColumn && sp.CallStrike != null)
                res = sp.CallStrike.OpenInterest;
            else if (column == callVolumeColumn && sp.CallStrike != null)
                res = sp.CallStrike.VolumeTradesCleared;
            else if (column == callDeltaColumn && sp.CallStrike != null)
                res = sp.CallStrike.Delta;
            else if (column == callSettPriceColumn && sp.CallStrike != null)
                res = sp.CallStrike.SettPrice;
            //
            else if (column == putOiColumn && sp.PutStrike != null)
                res = sp.PutStrike.OpenInterest;
            else if (column == putVolumeColumn && sp.PutStrike != null)
                res = sp.PutStrike.VolumeTradesCleared;
            else if (column == putDeltaColumn && sp.PutStrike != null)
                res = sp.PutStrike.Delta;
            else if (column == putSettPriceColumn && sp.PutStrike != null)
                res = sp.PutStrike.SettPrice;
            //
            else
                res = null;
            return res;
        }


        void UpdateChart()
        {
            var cell = StrikeDataGridView.SelectedCells[0];

            graphData.Clear();
            if (StrikeDataGridView.SelectedCells[0].ColumnIndex != strikeColumn.Index)
            {
                var row = StrikeDataGridView.Rows[cell.RowIndex];
                int strike = (int)row.Cells[strikeColumn.Name].Value;

                foreach (TreeNode node in ContractTreeView.Nodes)
                {
                    var dict = ((Dictionary<int, StrikePair>)node.Nodes[0].Tag);
                    if (!dict.ContainsKey(strike))
                        graphData.Add(0);
                    else
                    {
                        var sp = dict[strike];
                        var val0 = GetCellValue(sp, cell.OwningColumn);
                        var val = Convert.ToDouble(val0);
                        graphData.Add(val);
                    }
                }
            }
                
            ChartPanel.Invalidate();
        }
        

        private void ChartPanel_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.Clear(SystemColors.Window);
            if (graphData.Count == 0)
            {
                e.Graphics.DrawString("Выделите ячейку чтобы увидеть динамику изменения её значения", SystemFonts.DefaultFont, SystemBrushes.WindowText, ChartPanel.DisplayRectangle);
                return;
            }

            var minValue = double.MaxValue;
            var maxValue = double.MinValue;
            foreach (var val in graphData)
            {
                minValue = Math.Min(minValue, val);
                maxValue = Math.Max(maxValue, val);
            }

            var rangeSize = Math.Abs(maxValue - minValue);

            if (rangeSize == 0)
                return;

            var h = ChartPanel.Bounds.Height;
            var w = ChartPanel.Bounds.Width;
            var c = graphData.Count;

            for (int x = 1; x < c; x++)
            {
                var y0 = h- h * (graphData[x-1] / rangeSize);
                var y = h -h * (graphData[x] / rangeSize);
                e.Graphics.DrawLine(Pens.Red, w * ((float)(x - 1) / c), (float)y0, w * ((float)x / c), (float)y);
            }
            //e.Graphics.DrawLine(Pens.Gray, 0, 0, ChartPanel.Bounds.Width, 0);
        }

        private void StrikeDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            UpdateChart();
        }
    }
}
