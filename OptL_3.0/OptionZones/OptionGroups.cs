﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;
using CMEDB;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.OptionZonesGeneratorSettingsEditor")]
    public class OptionGroupsGenerator : LevelsGenerator
    {
        public override string ToString()
        {
            return "Опционные группы";
        }

        List<CMEDB.Strike> GetCalls(DBParser db, CurrencyInfo ci)
        {
            var callSection = db.GetSection(ci.CallSection);

            return db.GetSection(ci.CallSection)
                .Contracts.Find(c => c.Name == ci.CallContractName)
                .Strikes;
        }

        List<CMEDB.Strike> GetPuts(DBParser db, CurrencyInfo ci)
        {
            var putSection = db.GetSection(ci.PutSection);
            return db.GetSection(ci.PutSection)
                .Contracts.Find(c => c.Name == ci.PutContractName)
                .Strikes;
        }

        class TimeGroup
        {
            public int StrikeValue;
            public DateTime FirstDay;
            public DateTime LastDay;
            public List<Strike> Strikes = new List<Strike>();
        }

        class LevelGrp
        {
            public List<TimeGroup> TimeGroups = new List<TimeGroup>();

            public decimal AverageLevel
            {
                get
                {
                    var levelSumm = 0;
                    TimeGroups.ForEach(tg => levelSumm += tg.StrikeValue);
                    return (levelSumm / TimeGroups.Count);
                }
            }

            public string GetDescription(IndicationMode mode)
            {
                double ch = 0;
                double oi = 0;
                double volume = 0;

                foreach (var tg in TimeGroups)
                {
                    var grpOI = 0; // нужен только ОИ последнего страйка в каждой временной группе!!!
                    foreach (var s in tg.Strikes)
                    {
                        ch += s.OpenInterestChange;
                        grpOI = s.OpenInterest;
                        volume += s.VolumeTradesCleared;
                    }
                    oi += grpOI;
                }

                switch (mode)
                {
                    case IndicationMode.Volumes:
                        return volume.ToString();
                    case IndicationMode.OpenInterest:
                        return oi.ToString();
                    case IndicationMode.OiAndVol:
                        return oi + " V=" + volume;
                    case IndicationMode.OiCh:
                        return oi + (ch >= 0 ? "+" : "") + ch;
                    case IndicationMode.OiChAndVol:
                        return (ch >= 0 ? "+" : "") + ch + " V=" + volume;
                    default:
                        throw new NotImplementedException();
                }
            }

            internal void GenerateLevel
            (
                ITemplateBuilder templateBuilder, 
                DBParser db,
                CurrencyInfo ci, 
                bool isCall, 
                string template,
                DateTime minDate,
                DateTime maxDate,
                IndicationMode mode
            )
            {
                var levelLength = (float)(maxDate.Subtract(minDate).TotalDays + 1);

                templateBuilder.AddLevel(AverageLevel * ci.StrikeMultiplier, AverageLevel * ci.StrikeMultiplier, isCall ? minDate : minDate.AddDays(levelLength/2), db,
                    levelLength / 2 , "", GetDescription(mode), template);
            }
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            Dictionary<int, TimeGroup> callTimeGroups = null;
            Dictionary<int, TimeGroup> putTimeGroups = null;

            var grpLevels = ((int)GroupingMode) % 10;
            bool groupWeekly = (int)GroupingMode >= 10;

            int lastWeek = -1;
            DBParser lastDB = null;

            foreach (var db in dbs)
            {
                bool isSameContract = lastDB == null ||
                    db.GetSection(ci.CallSection).Contracts[0].Date ==
                    lastDB.GetSection(ci.CallSection).Contracts[0].Date;

                if (!isSameContract || !groupWeekly || lastWeek != WeekNumber(db.Date))
                {
                    if (callTimeGroups != null)
                        BuildLevels(templateBuilder, db, ci, callTimeGroups, grpLevels, "COL_CALL_BRIGHT", true);
                    if (putTimeGroups != null)
                        BuildLevels(templateBuilder, db, ci, putTimeGroups, grpLevels, "COL_PUT_BRIGHT", false);

                    callTimeGroups = new Dictionary<int, TimeGroup>();
                    putTimeGroups = new Dictionary<int, TimeGroup>();
                    lastWeek = WeekNumber(db.Date);
                }

                foreach (var call in GetCalls(db, ci))
                    if (call.OpenInterestChange != 0 || !HideNoOiCh)
                        TimeGroupAdd(callTimeGroups, db.Date, call);
                foreach (var put in GetPuts(db, ci))
                    if (put.OpenInterestChange != 0 || !HideNoOiCh)
                        TimeGroupAdd(putTimeGroups, db.Date, put);

                lastDB = db;
            }

            if (callTimeGroups.Count > 0)
                BuildLevels(templateBuilder, lastDB, ci, callTimeGroups, grpLevels, "COL_CALL_BRIGHT", true);
            if (putTimeGroups.Count > 0)
                BuildLevels(templateBuilder, lastDB, ci, putTimeGroups, grpLevels, "COL_PUT_BRIGHT", false);
        }

        private static void TimeGroupAdd(Dictionary<int, TimeGroup> timeGroups, DateTime strikeDate, Strike strike)
        {
            TimeGroup g;
            if (!timeGroups.ContainsKey(strike.StrikeValue))
            {
                g = new TimeGroup();
                timeGroups.Add(strike.StrikeValue, g);
                g.FirstDay = strikeDate;
                g.StrikeValue = strike.StrikeValue;
            }
            else
                g = timeGroups[strike.StrikeValue];
            g.Strikes.Add(strike);
            g.LastDay = strikeDate;
        }

        private void BuildLevels(ITemplateBuilder templateBuilder, DBParser db, CurrencyInfo ci, Dictionary<int, TimeGroup> timeGroups, int grpSize, string template, bool isCall)
        {
            var list = new List<TimeGroup>(timeGroups.Values);
            list.Sort((a, b) => a.StrikeValue.CompareTo(b.StrikeValue));

            List<LevelGrp> groups = new List<LevelGrp>();
            LevelGrp grp = null;
            foreach (var strike in list)
            {
                if (grp == null)
                    grp = new LevelGrp();
                grp.TimeGroups.Add(strike);
                if (grp.TimeGroups.Count == grpSize)
                {
                    groups.Add(grp);
                    grp = null;
                }
            }
            if (grp != null && grp.TimeGroups.Count > 0)
                groups.Add(grp);

            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;

            foreach (var g in groups)
            {
                foreach (var tg in g.TimeGroups)
                {
                    if (tg.FirstDay < minDate)
                        minDate = tg.FirstDay;
                    if (tg.LastDay > maxDate)
                        maxDate = tg.LastDay;
                }
            }

            foreach (var g in groups)
                g.GenerateLevel(templateBuilder, db, ci, isCall, template, minDate, maxDate, IndicationMode);

        }

        static int WeekNumber(DateTime dt)
        {
            return (int)(dt.Subtract(DateTime.MinValue).TotalDays) / 7;
        }

        private void ProcessDB(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser db)
        {
            var callSection = db.GetSection(ci.CallSection);
            var putSection = db.GetSection(ci.PutSection);

            var calls = db.GetSection(ci.CallSection)
                .Contracts.Find(c => c.Name == ci.CallContractName)
                .Strikes
                .ConvertAll(s => new OptLevel(s, ci, true));

            var puts = db.GetSection(ci.PutSection)
                .Contracts.Find(c => c.Name == ci.PutContractName)
                .Strikes
                .ConvertAll(s => new OptLevel(s, ci, false));
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser db)
        {
        }

        private bool hideNoOiCh = true;
        [DisplayName("Игнорировать страйки без изменения ОИ")]
        [Description("Значение true (истина) означает, что страйки без изменения ОИ не будут учтены в расчётах")]
        [DefaultValue(true)]
        public bool HideNoOiCh
        {
            get
            {
                return hideNoOiCh;
            }
            set
            {
                if (hideNoOiCh != value)
                {
                    hideNoOiCh = value;
                    OnPropertyChanged("HideNoOiCh");
                }
            }
        }

        private IndicationMode indicationMode = IndicationMode.Volumes;

        [DisplayName("Режим отображения")]
        [Description("Какая инфомация будет отображаться в подписи к страйку")]
        [DefaultValue(IndicationMode.Volumes)]
        public IndicationMode IndicationMode
        {
            get
            {
                return indicationMode;
            }
            set
            {
                if (this.indicationMode != value)
                {
                    this.indicationMode = value;
                    OnPropertyChanged("IndicationMode");
                }
            }
        }

        private GroupingMode groupingMode = GroupingMode.Day1;
        [DisplayName("Режим группировки")]
        [Description("Определяет способ объединения страйков в группы по строкам (день/неделя) и столбцам")]
        [DefaultValue(GroupingMode.Day1)]
        public GroupingMode GroupingMode
        {
            get
            {
                return groupingMode;
            }
            set
            {
                if (this.groupingMode != value)
                {
                    this.groupingMode = value;
                    OnPropertyChanged("GroupingMode");
                }
            }
        }

        public override void SaveSettings(XmlElement settings)
        {
            settings.SetAttribute("IndicationMode", Enum.GetName(typeof(IndicationMode), this.IndicationMode));
            settings.SetAttribute("GroupingMode", Enum.GetName(typeof(GroupingMode), this.GroupingMode));
            settings.SetAttribute("HideNoOiCh", this.HideNoOiCh.ToString());
        }

        public override void LoadSettings(XmlElement genSettings)
        {
            if (genSettings.HasAttribute("HideNoOiCh"))
                HideNoOiCh = bool.Parse(genSettings.GetAttribute("HideNoOiCh"));
            if (genSettings.HasAttribute("GroupingMode"))
                GroupingMode = (GroupingMode)Enum.Parse(typeof(GroupingMode), genSettings.GetAttribute("GroupingMode"));
            if (genSettings.HasAttribute("IndicationMode"))
                IndicationMode = (IndicationMode)Enum.Parse(typeof(IndicationMode), genSettings.GetAttribute("IndicationMode"));
        }
    }

    public enum GroupingMode
    {
        [FieldDisplayName("без группировки")]
        Day1 =1,
        [FieldDisplayName("за день по 2 страйка")]
        Day2 =2,
        [FieldDisplayName("за день по 3 страйка")]
        Day3 =3 ,
        [FieldDisplayName("за день по 4 страйка")]
        Day4 =4,
        [FieldDisplayName("за неделю 1 страйк")]
        Week1 = 11,
        [FieldDisplayName("за неделю по 2 страйка")]
        Week2 = 12,
        [FieldDisplayName("за неделю по 3 страйка")]
        Week3 = 13,
        [FieldDisplayName("за неделю по 4 страйка")]
        Week4 = 14,
    }

    public enum IndicationMode
    {
        [FieldDisplayName("объём")]
        Volumes,
        [FieldDisplayName("ОИ")]
        OpenInterest,
        [FieldDisplayName("ОИ и объём")]
        OiAndVol,
        [FieldDisplayName("изменение ОИ")]
        OiCh,
        [FieldDisplayName("изменение ОИ и объём")]
        OiChAndVol,
    }
}
