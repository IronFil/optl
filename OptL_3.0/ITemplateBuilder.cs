﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;

namespace OptL
{
    public interface ITemplateBuilder
    {
        void AddLabel(DateTime date, string name, string description, string template);
        void AddLevel(decimal level0, decimal level1, DateTime start, DBParser spotDb, float fractLength, string name, string description, string template);
        void AddLevel(decimal level0, decimal level1, DateTime start, DateTime end, DBParser spotDb, string name, string description, string template);
        void AddOrder(decimal openPrice, DateTime openTime, decimal closePrice, DateTime closeTime, string description, bool isBuy);
        void AddBar(float fractHeight, DateTime pos, string name, string description, string template);
        void AddArrow(decimal level0, DateTime time0, DBParser spotDb, string name, string description, string template, int symbolCode);
        float SecondaryMin { get; set; }
        float SecondaryMax { get; set; }

        decimal Level2Spot(decimal level, DBParser spotDb);

        string GetObjectType(string p);
    }
}