﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;
using CMEDB;
using System.Linq;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.FutExportGeneratorSettingsEditor")]
    public class FutExportGenerator : LevelsGenerator
    {
        string File;

        public override string ToString()
        {
            return "Выгрузка данных по фьючерсам";
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            if (File == null)
                return;

            var sb = new StringBuilder();
            var headers1 = "Дата";
            var headers2 = "";

            var cols = new string[] { "GBP", "EUR", "JPY", "CAD", "CHF", "AUD" };

            var headersDone = false;

            foreach (var db in dbs)
            {
                sb.AppendLine();
                sb.Append(db.Date.ToString("dd.MM.yyyy"));
                foreach (var col in cols)
                {
                    var ci2 = CurrencyInfo.AvailableCurrencyInfoList.Find(c => c.ForexName == col);
                    var section = db.GetSection(ci2.FutSection);
                    if (section == null) continue;
                    var fut = 
                        forContractNumber.HasValue ? section._FuturesList[forContractNumber.Value] 
                        : forContractDate.HasValue ? section._FuturesList.First(f => f.Date == forContractDate.Value)
                        : section._FuturesList.First();

                    sb.Append(";");
                    sb.Append(fut.OpenInterest);
                    sb.Append(";");
                    sb.Append(fut.OpenInterestChange);
                    sb.Append(";");
                    sb.Append(fut.SettPrice);
                    sb.Append(";");
                    sb.Append(fut.PtChge);

                    if (!headersDone)
                    {
                        headers1 += ";" + col + ";;;";
                        headers2 += ";ОИ;Изм.ОИ;SETL;Изм.SETL";
                    }
                }
                headersDone = true;
            }

            System.IO.File.WriteAllText(File, headers1 + Environment.NewLine + headers2 + Environment.NewLine + sb.ToString(), Encoding.GetEncoding(1251));
        }

        public override bool WritesTemplate
        {
            get
            {
                return false;
            }
        }

        public override void ShowUI(CurrencyInfo ci, bool spot, IEnumerable<DBParser> dbs)
        {
            var fs = new System.Windows.Forms.SaveFileDialog();
            fs.CheckPathExists = true;
            fs.FileName = "fut.csv";
            fs.Filter = "Excel файл CSV|csv";
            fs.Title = ToString();
            if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                this.File = fs.FileName;
            else
                this.File = null;
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser db)
        {
        }

        //private bool SkipContract()
        //{
        //    return (forContractNumber.HasValue && c.N != forContractNumber) || (forContractDate.HasValue && c.ExpirationDate != forContractDate);
        //}

        DateTime? forContractDate = null;
        int? forContractNumber = 1;
        static System.Globalization.CultureInfo dateFormat = System.Globalization.CultureInfo.GetCultureInfo("en-us");


        [DisplayName("Контракт")]
        [Description("№ контракта (1 = текущий, 2 следующий и т.д.) или дата экспирации (например NOV13 = ноябрь 2013)")]
        [DefaultValue("1")]
        public string ContractN
        {
            get
            {
                return
                    forContractNumber.HasValue ? forContractNumber.ToString()
                    : forContractDate.HasValue ?
                    forContractDate.Value.ToString("MMMyy", dateFormat).ToUpper()
                    : "???";
            }

            set
            {
                int n;
                DateTime dt;

                if (int.TryParse(value, out n))
                {
                    forContractNumber = n;
                    forContractDate = null;
                }
                else if (DateTime.TryParse("01" + value, out dt))
                {
                    forContractNumber = null;
                    forContractDate = dt;
                }
                else
                    throw new Exception("Нужно ввести номер или дату в виде MMMYY");

                OnPropertyChanged("ContractN");
            }
        }


        public override void SaveSettings(XmlElement settings)
        {
        }

        public override void LoadSettings(XmlElement genSettings)
        {
        }
    }
}
