﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using MT4Tools;
using System.ComponentModel;
using System.Xml;
using CMEDB;

namespace OptL
{
    //[LevelsGeneratorSettingsEditor("OptL.StrikeExportGeneratorSettingsEditor")]
    public class StrikeExportGenerator : LevelsGenerator
    {
        public override string ToString()
        {
            return "Выгрузка страйков";
        }

        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            StringBuilder sb = new StringBuilder();

            
            foreach (var db in dbs)
            {
                sb.AppendLine(";;;;;;;;DATE;" + db.Date.ToShortDateString());

                var callSection = db.GetSection(ci.CallSection);
                if (callSection._FuturesList != null && callSection._FuturesList.Count > 0)
                {
                    sb.AppendLine(
                        string.Join(";", new string[] 
                        {
                            "","","","","OPEN RANGE (start)","OPEN RANGE (end)","HIGH","LOW","CLOSING RANGE (start)","CLOSING RANGE (end)","SETT.PRICE",
                            "PT. CHGE","RECIPROCAL","RTH VOLUME","GLOBEX VOLUME","OPEN INTEREST","OPEN INTEREST CHANGE","CONTRACT HIGH","CONTRACT LOW"
                        }));

                    foreach (var fut in callSection._FuturesList)
                    {
                        sb.AppendLine(fut.ToString());
                    }
                    sb.AppendLine();
                }

                sb.AppendLine(";;;;;;;;;OPTIONS");

                var combined = CombineStrikes(db, ci);
                combined.Sort((a,b) => a.Key.CompareTo(b.Key));

                foreach (var kv in combined)
                {
                    sb.AppendLine(";;;;;;;;;" + kv.Key.ToString("MMMM yyyy", System.Globalization.CultureInfo.InvariantCulture));
                    sb.AppendLine(";;;;CALL;;;;;STRIKE PRICE;;;;PUT;;;");

                    var strikeKeys = new List<int>(kv.Value.Keys);
                    strikeKeys.Sort();

                    int c_tv = 0, c_toi = 0, c_toich = 0;
                    int p_tv = 0, p_toi = 0, p_toich = 0;

                    sb.Append("VOLUME TRADES CLEARED;OPEN INTEREST;OPEN INTEREST CHANGE;CONTRACT HIGH;CONTRACT LOW;CLOSING RANGE;SETT.PRICE;PT.CHGE.;DELTA");
                    sb.Append(";;");
                    sb.AppendLine("VOLUME TRADES CLEARED;OPEN INTEREST;OPEN INTEREST CHANGE;CONTRACT HIGH;CONTRACT LOW;CLOSING RANGE;SETT.PRICE;PT.CHGE.;DELTA");

                    foreach (var strikeKey in strikeKeys)
                    {
                        var sp = kv.Value[strikeKey];
                        sb.Append(FormatStrike(sp.CallStrike));
                        sb.Append(";");
                        sb.Append(strikeKey.ToString());
                        sb.Append(";");
                        sb.Append(FormatStrike(sp.PutStrike));
                        sb.AppendLine();
                        if (sp.CallStrike != null)
                        {
                            c_tv += sp.CallStrike.VolumeTradesCleared;
                            c_toi += sp.CallStrike.OpenInterest;
                            c_toich += sp.CallStrike.OpenInterestChange;
                        }
                        if (sp.PutStrike != null)
                        {
                            p_tv += sp.PutStrike.VolumeTradesCleared;
                            p_toi += sp.PutStrike.OpenInterest;
                            p_toich += sp.PutStrike.OpenInterestChange;
                        }
                    }
                    sb.AppendLine(string.Format("{0};{1};{2};;;;;;;TOTAL;{3};{4};{5}", c_tv, c_toi, c_toich, p_tv, p_toi, p_toich));
                    sb.AppendLine();
                }
            }

            var fs = new System.Windows.Forms.SaveFileDialog();
            fs.CheckPathExists = true;
            fs.FileName = ci.ForexName+ ".csv";
            fs.Filter = "Excel файл CSV|csv";
            fs.Title = ToString();
            if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                System.IO.File.WriteAllText(fs.FileName, sb.ToString());
            }
        }

        private string FormatStrike(Strike strike)
        {
            if (strike != null)
            {
                return string.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                    strike.VolumeTradesCleared,
                    strike.OpenInterest,
                    strike.OpenInterestChange,
                    strike.ContractHight,
                    strike.ContractLow,
                    "-''-", //strike.ClosingRangeText,
                    strike.SettPrice,
                    strike.PtChge,
                    strike.Delta);
            }
            else
                return (";;;;;;;;");
        }

        private List<KeyValuePair<DateTime, Dictionary<int, StrikePair>>> CombineStrikes(DBParser db, CurrencyInfo ci)
        {
            var contracts = new List<KeyValuePair<DateTime, Dictionary<int, StrikePair>>>();

            foreach (var contract in db.GetSection(ci.CallSection).Contracts.FindAll(c => c.Name.StartsWith(ci.CallContractName)))
            {
                var strikes = new Dictionary<int, StrikePair>();
                contracts.Add(new KeyValuePair<DateTime, Dictionary<int, StrikePair>>(contract.Date, strikes));

                foreach (var strike in contract.Strikes)
                {
                    if (!strikes.ContainsKey(strike.StrikeValue))
                        strikes.Add(strike.StrikeValue, new StrikePair());
                    strikes[strike.StrikeValue].CallStrike = strike;
                }
                
            }

            foreach (var contract in db.GetSection(ci.PutSection).Contracts.FindAll(c => c.Name.StartsWith(ci.PutContractName)))
            {
                var strikes = contracts.Find(c => c.Key == contract.Date).Value;
                if (strikes == null)
                {
                    strikes = new Dictionary<int, StrikePair>();
                    contracts.Add(new KeyValuePair<DateTime, Dictionary<int, StrikePair>>(contract.Date, strikes));
                }

                foreach (var strike in contract.Strikes)
                {
                    if (!strikes.ContainsKey(strike.StrikeValue))
                        strikes.Add(strike.StrikeValue, new StrikePair());
                    strikes[strike.StrikeValue].PutStrike = strike;
                }
            }

            return contracts;
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser db)
        {
        }

        public override void SaveSettings(XmlElement settings)
        {
        }

        public override void LoadSettings(XmlElement genSettings)
        {
        }

        class StrikePair
        {
            public Strike CallStrike { get; set; }
            public Strike PutStrike { get; set; }
        }
    }
}
