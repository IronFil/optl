﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using OptL;

namespace A_IOZ
{
    public class IOZ : LevelsGenerator
    {
        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            foreach (var db in dbs)
                ProcessDB(templateBuilder, ci, db);
        }

        class StrikeInfo
        {
            public int Strike;
            public int PutOISumm;
            public int CallOISumm;

            public int CallV;
            public int PutV;

            public float CallVPerc;
            public float PutVPerc;
        }

        private void ProcessDB(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser db)
        {
            var strikesDict = new Dictionary<int, StrikeInfo>();
            var strikesList = new List<StrikeInfo>();

            var contracts = db.GetSection(ci.CallSection).Contracts;
            if (ci.PutSection != ci.CallSection)
                contracts.AddRange(db.GetSection(ci.PutSection).Contracts);

            int totalCallOi = 0;
            int totalPutOi = 0;

            foreach (var contract in contracts)
            {
                bool isCall = contract.Name.StartsWith(ci.CallContractName);
                if (!isCall && !contract.Name.StartsWith(ci.PutContractName))
                    continue;

                foreach (var strike in contract.Strikes)
                {
                    StrikeInfo si;
                    if (!strikesDict.ContainsKey(strike.StrikeValue))
                    {
                        si = new StrikeInfo() { Strike = strike.StrikeValue };
                        strikesDict.Add(strike.StrikeValue, si);
                        strikesList.Add(si);
                    }
                    else 
                        si = strikesDict[strike.StrikeValue];

                    if (isCall)
                    {
                        totalCallOi += strike.OpenInterest;
                        si.CallOISumm += strike.OpenInterest;
                    }
                    else
                    {
                        totalPutOi += strike.OpenInterest;
                        si.PutOISumm += strike.OpenInterest;
                    }
                }
            }

            strikesList.Sort((s1, s2) => s1.Strike.CompareTo(s2.Strike));

            int lastIndex = strikesList.Count - 1;
            strikesList[0].CallV = strikesList[0].CallOISumm;
            strikesList[lastIndex].CallV = strikesList[lastIndex].CallOISumm;

            var sb = new StringBuilder();

            StrikeInfo BorderCall = null;
            StrikeInfo BorderPut = null;
            StrikeInfo CriticalCall = null;
            StrikeInfo CriticalPut = null;

            for (int i = 1; i <= lastIndex; i++)
            {
                var si0 = strikesList[i];
                var si1 = strikesList[lastIndex - i];

                si0.CallV = si0.CallOISumm + strikesList[i - 1].CallV;
                si0.CallVPerc = (float)si0.CallV / totalCallOi;
                if (BorderCall == null && si0.CallVPerc >= 0.7f)
                    BorderCall = si0;
                if (CriticalCall == null && si0.CallVPerc >= 0.4f)
                    CriticalCall = si0;

                si1.PutV = si1.PutOISumm + strikesList[lastIndex - i + 1].PutV;
                si1.PutVPerc = (float)si1.PutV / totalPutOi;
                if (BorderPut == null && si1.PutVPerc >= 0.7f)
                    BorderPut = si1;
                if (CriticalPut == null && si1.PutVPerc >= 0.4f)
                    CriticalPut = si1;
            }

            StrikeInfo equilibrium = null;

            for (int i = 0; i <= lastIndex; i++)
            {
                var si = strikesList[i];
                if (equilibrium == null || Math.Abs(equilibrium.PutVPerc - equilibrium.CallVPerc) > Math.Abs(si.PutVPerc - si.CallVPerc))
                    equilibrium = si;

                sb.Append(si.CallVPerc.ToString("0.00"));
                sb.Append("\t");
                sb.Append(si.CallOISumm);
                sb.Append("\t");
                sb.Append(si.Strike);
                sb.Append("\t");
                sb.Append(si.PutVPerc.ToString("0.00"));
                sb.Append("\t");
                sb.Append(si.PutOISumm);
                sb.AppendLine();
            }

            var borderCallLevel = BorderCall.Strike * ci.StrikeMultiplier;
            var CriticalCallLevel = CriticalCall.Strike * ci.StrikeMultiplier;
            var borderPutLevel = BorderPut.Strike * ci.StrikeMultiplier;
            var CriticalPutLevel = CriticalPut.Strike * ci.StrikeMultiplier;
            var equilibriumLevel = equilibrium.Strike * ci.StrikeMultiplier;

            templateBuilder.AddLevel(borderCallLevel, borderCallLevel, db.LevelDate, db, 1f, null, null, "K2_TOP1");
            templateBuilder.AddLevel(CriticalCallLevel, CriticalCallLevel, db.LevelDate, db, 1f, null, null, "K2_TOP2");
            templateBuilder.AddLevel(borderPutLevel, borderPutLevel, db.LevelDate, db, 1f, null, null, "K2_BOTTOM1");
            templateBuilder.AddLevel(CriticalPutLevel, CriticalPutLevel, db.LevelDate, db, 1f, null, null, "K2_BOTTOM2");
            templateBuilder.AddLevel(equilibriumLevel, equilibriumLevel, db.LevelDate, db, 1f, null, null, "K2_BALANCEY");
        }

        public override void SaveSettings(System.Xml.XmlElement settings)
        {
            //throw new NotImplementedException();
        }

        public override void LoadSettings(System.Xml.XmlElement genSettings)
        {
            //throw new NotImplementedException();
        }

        public override string ToString()
        {
            return "Индикатор опционных зон";
        }
    }
}
