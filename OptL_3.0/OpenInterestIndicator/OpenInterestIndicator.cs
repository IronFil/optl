﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using OptL;

namespace OpenInterestIndicator
{
    public class OpenInterestIndicator : LevelsGenerator
    {
        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            foreach (var db in dbs)
            {
                GenerateDayLevels(templateBuilder, ci, db, db.GetSection(ci.CallSection)._FuturesList[0].SettPrice);
            }
        }

        public void GenerateDayLevels(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser l, decimal dayOpenPrice)
        {
            DateTime levelDate;

            switch (l.Date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    levelDate = l.Date.AddDays(1); break;
                case DayOfWeek.Friday:
                    levelDate = l.Date.AddDays(3);
                    break;
                default:
                    throw new Exception();
            }

            if (l.GetSection(ci.CallSection)._FuturesList.Count > 0)
            {
                var firstFutures = l.GetSection(ci.CallSection)._FuturesList[0];
                var nextFutures = l.GetSection(ci.CallSection)._FuturesList[1];
                //templateBuilder.AddLabel(l.Date, "", firstFutures.OpenInterest.ToString("0,0").Replace('\xA0', ' ') + " / " + firstFutures.OpenInterestChange.ToString(), "OI_FIRST");
                //templateBuilder.AddLabel(l.Date, "", firstFutures.OpenInterest.ToString("0,0").Replace('\xA0', ' ') + " / " + firstFutures.OpenInterestChange.ToString(), "OI_NEXT");
                templateBuilder.AddLabel(l.Date, "", firstFutures.OpenInterest.ToString() + "/" + firstFutures.OpenInterestChange.ToString(), "OI_FIRST");
                templateBuilder.AddLabel(l.Date, "", nextFutures.OpenInterest.ToString() + "/" + nextFutures.OpenInterestChange.ToString(), "OI_NEXT");
            }
        }

        public override void SaveSettings(System.Xml.XmlElement settings)
        {
        }

        public override void LoadSettings(System.Xml.XmlElement genSettings)
        {
        }

        public override string ToString()
        {
            return "Индикатор открытого интереса";
        }
    }
}
