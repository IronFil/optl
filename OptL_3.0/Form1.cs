﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Collections;
using MT4Tools;
using CMEDB;
using System.Reflection;
using System.Reflection.Emit;
using System.Linq;

namespace OptL
{
    public partial class Form1 : Form
    {
        string saveTemplateDirectory = "";
        string dbDirectory = "";

        public Form1()
        {
            InitializeComponent();

            foreach (var ci in CurrencyInfo.AvailableCurrencyInfoList)
                ci.PropertyChanged += new PropertyChangedEventHandler(ci_PropertyChanged);
        }

        void ci_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateLevels();

            UpdateDBListViewSpotColumn();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //var dbs = LoadCache(cachePath);

            var clf = new CacheLoadForm();
            if (clf.Start())
            {
                clf.ShowDialog(this);
                if (clf.Result.Count > 0)
                {
                    AddBulletinsToListView(clf.Result);
                    DBListView.EnsureVisible(DBListView.Items.Count - 1);
                    UpdateCurrencyCombobox(clf.CIS);

                }
            }

            var sorter = new ItemSorter();
            DBListView.ListViewItemSorter = sorter;
            sorter.SortColumn = 2;
            DBListView.Sort();

            //openFileDialog1.InitialDirectory = @"C:\New folder\db\zip";

            var generatorsFolder = AppDomain.CurrentDomain.BaseDirectory;
            foreach (var assemblyFile in Directory.GetFiles(generatorsFolder, "*.dll"))
            {
                Assembly assembly = null;
                //try
                //{
                assembly = Assembly.LoadFrom(assemblyFile);
                //}
                //catch
                //{
                //}

                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsSubclassOf(typeof(LevelsGenerator)))
                    {
                        var generator = (LevelsGenerator)(type.GetConstructor(System.Type.EmptyTypes).Invoke(null));
                        var lvi = new ListViewItem() { Checked = false, Tag = generator, Text = generator.ToString() };
                        GeneratorsListView.Items.Add(lvi);
                        generator.PropertyChanged += new PropertyChangedEventHandler(generator_PropertyChanged);
                    }
                }
            }


            LoadSettings();

            //for (int i = 0; i < GeneratorsListView.Items.Count; i++)
            //{
            //    var g = GeneratorsListView.Items[i];
            //    if (g.Tag.GetType().Name == "ClassicOptionLevelsGenerator2") g.Checked = true;
            //}

            //SaveTemplateButton_Click(this, EventArgs.Empty);
        }

        public CurrencyInfo CurrentCurrency { get { return CurrencyComboBox.SelectedItem as CurrencyInfo; } }

        void generator_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateLevels();
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (DBListView.Items.Count == 0)
            {
                var ciP = new CiPicker();
                if (ciP.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    UpdateCurrencyCombobox(ciP.Picked);
                }
                else
                {
                    this.Close();
                    return;
                }
            }

            if (dbDirectory != "" && Directory.Exists(dbDirectory))
                openFileDialog1.InitialDirectory = dbDirectory;
            if (openFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
            {
                dbDirectory = Path.GetDirectoryName(openFileDialog1.FileName);
                var pf = new ProcessingForm();

                var cis = new CurrencyInfo[CurrencyComboBox.Items.Count];
                CurrencyComboBox.Items.CopyTo(cis, 0);
                pf.Start(openFileDialog1.FileNames, cis);

                if (pf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    AddBulletinsToListView(pf.Result);
                    if (autoRemember.Checked)
                        SaveDBs();
                }
            }
        }

        private void UpdateCurrencyCombobox(IEnumerable<CurrencyInfo> cis)
        {
            CurrencyComboBox.Items.Clear();
            foreach (var ci in cis)
                CurrencyComboBox.Items.Add(ci);
        }

        private void AddBulletinsToListView(IEnumerable<DBParser> dbs)
        {
            DBListView.BeginUpdate();

            foreach (var l in new List<DBParser>(dbs))// Чтобы не выпрыгивала ошибка, что коллекция изменилась, когда нажимают "отмена"
            {
                var item = new ListViewItem("");
                item.SubItems.Add(l.FileName);
                item.SubItems.Add(l.LevelDate.ToShortDateString());
                item.SubItems.Add(l.Date.ToShortDateString());
                item.SubItems.Add(l.BulletinType);
                item.Tag = l;

                foreach (CurrencyInfo ci in CurrencyComboBox.Items)
                {
                    if (ci.NewDbMinOi != 0) l.MinOi[ci.ForexName] = ci.NewDbMinOi;
                    if (ci.NewDbMaxOi != 0) l.MaxOi[ci.ForexName] = ci.NewDbMaxOi;
                    if (ci.NewDbMinOiCh != 0) l.MinOiCh[ci.ForexName] = ci.NewDbMinOiCh;
                    if (ci.NewDbMaxOiCh != 0) l.MaxOiCh[ci.ForexName] = ci.NewDbMaxOiCh;
                    if (ci.NewDbMinVol != 0) l.MinVol[ci.ForexName] = ci.NewDbMinVol;
                    if (ci.NewDbMaxVol != 0) l.MaxVol[ci.ForexName] = ci.NewDbMaxVol;
                }

                DBListView.Items.Add(item);
            }
            UpdateDBListViewSpotColumn();
            DBListView.EndUpdate();
        }

        private void CurrencyComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSettings();
            UpdateLevels();
            UpdateDBListViewSpotColumn();
        }


        private void SpotCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateLevels();
        }

        private void UpdateSettings()
        {
            var ci = CurrentCurrency;

            //foreach (ListViewItem lvi in GeneratorsListView.Items)
            //{
            //    var generator = (lvi.Tag as LevelsGenerator);
            //    generator.Currency = ci;
            //    generator.DbParser = DBListView.SelectedItems.Count == 1 ? (DBParser)DBListView.SelectedItems[0].Tag : null;
            //}

            if (ci != null)
            {
                List<DBParser> bulletins = new List<DBParser>();
                foreach (ListViewItem lvi in DBListView.SelectedItems)
                {
                    var db = ((DBParser)lvi.Tag);
                    bulletins.Add(db);
                }
                ci.SetSelectedBulletins(bulletins);
            }

            CurrencyPropertyGrid.SelectedObject = CurrentCurrency;
        }

        private void UpdateDBListViewSpotColumn()
        {
            var ci = CurrentCurrency;
            foreach (ListViewItem lvi in DBListView.Items)
            {
                if (ci == null)
                    lvi.Text = "";
                else
                {
                    var db = (DBParser)lvi.Tag;
                    //var spotAdjustmentCall = db.SpotAdjustmentCall[ci.ForexName];
                    //var spotAdjustmentPut = db.SpotAdjustmentPut[ci.ForexName];
                    //lvi.Text = "CALL=" + (spotAdjustmentCall ?? 0) + ", PUT=" + (spotAdjustmentPut ?? 0);
                    lvi.Text = (db.ForwardPoints[ci.ForexName] ?? 0).ToString();
                }
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            var toRemove = new List<ListViewItem>();
            foreach (var item in DBListView.SelectedItems)
                toRemove.Add((ListViewItem)item);
            foreach (var item in toRemove)
                DBListView.Items.Remove(item);
            if (autoRemember.Checked)
                SaveDBs();
        }

        private void DBListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateSettings();
            UpdateLevels();
        }

        private void UpdateLevels()
        {
            if (DBListView.SelectedItems.Count != 1 || CurrentCurrency == null)
            {
                if (((string)LevelsTextBox.Tag) != "X") LevelsTextBox.Clear();
                return;
            }

            var sb = new StringBuilder();

            foreach (ListViewItem item in GeneratorsListView.CheckedItems)
            {
                var lg = (LevelsGenerator)item.Tag;
                var db = (DBParser)DBListView.SelectedItems[0].Tag;
                lg.GenerateText(sb, CurrentCurrency, SpotCheckBox.Checked, db);
            }

            LevelsTextBox.Text = sb.ToString();
        }


        private void UpdateTemplateButton_Click(object sender, EventArgs e)
        {
            SaveTemplate(update: true);
        }

        private void SaveTemplateButton_Click(object sender, EventArgs e)
        {
            SaveTemplate(update: false);
        }

        private void SaveTemplate(bool update)
        {
            if (DBListView.Items.Count > 0 && CurrentCurrency != null)
            {
                var counts = new Dictionary<DateTime, int>();

                StringBuilder errorMessageBuilder = null;

                var loadedDBs = GetLoadedDbs(out errorMessageBuilder);

                if (errorMessageBuilder != null)
                {
                    MessageBox.Show(errorMessageBuilder.ToString(), this.Text);
                    return;
                }


                var ci = this.CurrentCurrency;
                var spot = SpotCheckBox.Checked;
                var generators = new List<LevelsGenerator>();

                foreach (ListViewItem item in GeneratorsListView.CheckedItems)
                {
                    var lg = (LevelsGenerator)item.Tag;
                    generators.Add(lg);
                    lg.CurrencyList = CurrencyComboBox.Items.Cast<CurrencyInfo>().ToArray();
                }

                var templateToUpdate = (string)null;

                if (update && templateOpenFileDialog.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                {
                    templateToUpdate = templateOpenFileDialog.FileName;
                }

                foreach (var lg in generators)
                    lg.ShowUI(ci, spot, loadedDBs.Values);

                using (var bw = new BackgroundWorker() { WorkerSupportsCancellation = true })
                {
                    var waitForm = new DownloadVolumesForm();
                    waitForm.CancelDelegate += p => bw.CancelAsync();
                    
                    bw.DoWork += (sender, e) => { e.Result = GenerateLevels(loadedDBs, generators, bw, ci, spot, templateToUpdate); };
                    bw.RunWorkerCompleted += (sender, e) =>
                    {
                        waitForm.Dispose();
                        if (e.Cancelled)
                            return;

                        var tnf = e.Error as TemplateNotFound;
                        if (tnf != null)
                        {
                            MessageBox.Show("Шаблон 'OptL.tpl' не содержит образец уровня с именем '" + tnf.TemplateName + "'");
                            return;
                        }
                        else if (e.Error != null)
                            throw e.Error;

                        var res = (GenerateLevelsResult)e.Result;

                        if (!string.IsNullOrWhiteSpace(res.errorMessages))
                            MessageBox.Show(res.errorMessages, "Во время обработки произошли ошибки", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        if (saveTemplateDirectory != "")
                            saveFileDialog1.InitialDirectory = saveTemplateDirectory;
                        if (res.Template != null && saveFileDialog1.ShowDialog(this) == System.Windows.Forms.DialogResult.OK)
                        {
                            saveTemplateDirectory = Path.GetDirectoryName(saveFileDialog1.FileName);
                            File.WriteAllText(saveFileDialog1.FileName, res.Template);
                        }

                        foreach (var file in res.additionalFiles)
                        {
                            using (var fs = new System.Windows.Forms.SaveFileDialog())
                            {
                                if (saveTemplateDirectory != "")
                                    fs.InitialDirectory = saveTemplateDirectory;
                                fs.CheckPathExists = true;
                                fs.FileName = file.Name;
                                //fs.Filter = "Excel файл CSV|csv";
                                fs.Title = this.Name;
                                if (fs.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                                {
                                    System.IO.File.WriteAllBytes(fs.FileName, file.Data);
                                }
                            }
                        }
                    };

                    bw.RunWorkerAsync();
                    waitForm.ShowDialog(this);
                }
            }
            else
                MessageBox.Show("Нужно загрузить хотябы один бюллетень и выбрать валюту.");
        }


        class GenerateLevelsResult
        {
            public string Template;
            public IEnumerable<LevelsGeneratorAdditionalFile> additionalFiles;
            public string errorMessages;
        }

        private static GenerateLevelsResult GenerateLevels(
            Dictionary<DateTime, DBParser> loadedDBs,
            IEnumerable<LevelsGenerator> generators,
            BackgroundWorker bw,
            CurrencyInfo ci,
            bool spot, string templateToUpdate)
        {
            var tr = new MT4Tools.TemplateReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OptL.tpl"));
            System.Xml.XmlDocument newDoc = new System.Xml.XmlDocument();
            newDoc.Load(tr);
            var lb = new TemplateBuilder(newDoc, ci, spot);

            //Dictionary<DateTime, List<LevelVolumes>> downloadedVolumes = null;

            var writeTemplate = false;

            var files = new List<LevelsGeneratorAdditionalFile>();

            var errorMessages = new StringBuilder();

            foreach (var lg in generators)
            {
                //if (lg.UsesCashFlowHorizontal)
                //{
                //    if (downloadedVolumes == null)
                //    {
                //        var dvf = new DownloadVolumesForm();
                //        dvf.Contract = CurrentCurrency.ForexName;
                //        dvf.ShowDialog(this);
                //        downloadedVolumes = dvf.DownloadedVolumes;
                //    }
                //    lg.CashFlowHorizontal = downloadedVolumes;
                //}

                if (bw.CancellationPending) return null;

                var lg2 = lg as LevelsGenerator2;

                try
                {
                    if (lg2 != null)
                    {
                        files.AddRange(lg2.GenerateTemplate(loadedDBs.Values, ci, lb));
                    }
                    else
                        lg.GenerateTemplate(lb, ci, loadedDBs.Values);
                }
                catch (LevelGeneratorException e)
                {
                    errorMessages.AppendLine(e.Message);
                }
                writeTemplate |= lg.WritesTemplate;
            }

            if (!writeTemplate)
                return new GenerateLevelsResult() { Template = null, additionalFiles = files }; ;

            if (templateToUpdate != null)
            {
                var oldDoc = new System.Xml.XmlDocument();

                oldDoc.Load(new TemplateReader(templateToUpdate));

                var cutTimeNode = oldDoc.SelectSingleNode("//object[object_name='OptLCut']/time_0[1]"); //time_0=1383746400

                var cutTime = cutTimeNode == null ? DateTime.MinValue : TemplateBuilder.GetNodeTime(cutTimeNode);

                foreach (XmlNode node in oldDoc.SelectNodes("//object[object_name!='OptLCut' and substring(object_name,1,4)='OptL']"))
                {
                    if (bw.CancellationPending) return null;
                    var time_0 = TemplateBuilder.GetNodeTime(node.SelectSingleNode("time_0"));
                    var time_1 = TemplateBuilder.GetNodeTime(node.SelectSingleNode("time_1"));

                    if (cutTime.HasValue)
                    {
                        // не удаляем те, которые хотябы частично находятся слева от линии
                        if ((time_0.HasValue && time_0 < cutTime) || (time_1.HasValue && time_1 < cutTime))
                            continue;
                    }
                    node.ParentNode.RemoveChild(node);
                }


                var srcPrimaryIndicator = newDoc.SelectSingleNode("/chart/window/indicator[name='main']");
                var dstPrimaryIndicator = oldDoc.SelectSingleNode("/chart/window/indicator[name='main']");
                var srcSecondIndicator = newDoc.SelectSingleNode("/chart/window/indicator[name='" + TemplateBuilder.SecondWindowName + "']");
                var dstSecondIndicator = oldDoc.SelectSingleNode("/chart/window/indicator[name='" + TemplateBuilder.SecondWindowName + "']");

                if (srcSecondIndicator != null && dstSecondIndicator == null)
                {
                    var dstSecondWindow = oldDoc.ImportNode(srcSecondIndicator.ParentNode, true);
                    dstSecondIndicator = dstSecondWindow.SelectSingleNode("indicator");
                    foreach (XmlNode o in dstSecondIndicator.SelectNodes("object"))
                        dstSecondIndicator.RemoveChild(o);
                    oldDoc.FirstChild.AppendChild(dstSecondWindow);
                }

                foreach (XmlNode node in newDoc.SelectNodes("//object[substring(object_name,1,4)='OptL']"))
                {
                    if (bw.CancellationPending) return null;

                    var time_0 = TemplateBuilder.GetNodeTime(node.SelectSingleNode("time_0"));
                    var time_1 = TemplateBuilder.GetNodeTime(node.SelectSingleNode("time_1"));

                    if (cutTime.HasValue)
                    {
                        // не копируем те, которые хотябы частично окажутся слева от линии
                        if ((time_0.HasValue && time_0 < cutTime) || (time_1.HasValue && time_1 < cutTime))
                            continue;
                    }

                    var objectNode = oldDoc.ImportNode(node, true);

                    if (node.ParentNode == srcPrimaryIndicator)
                    {
                        dstPrimaryIndicator.AppendChild(objectNode);
                    }
                    else if (node.ParentNode == srcSecondIndicator)
                    {
                        dstSecondIndicator.AppendChild(objectNode);
                    }
                    else
                    {
                        throw new Exception("Обнаружен неподдерживаемый тип окна индикатора");
                    }
                }
                newDoc = oldDoc;
            }

            var sb = new System.Text.StringBuilder();
            System.IO.TextWriter tw = new System.IO.StringWriter(sb);
            var xw = new MT4Tools.TemplateWriter(tw);
            newDoc.Save(xw);
            return new GenerateLevelsResult() { Template = sb.ToString(), additionalFiles = files, errorMessages = errorMessages.ToString() };
        }

        private void DBListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            ((ItemSorter)DBListView.ListViewItemSorter).SortColumn = e.Column;
            DBListView.Sort();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveDBs();
        }

        private void SaveDBs()
        {
            using (var file = File.Create(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cache.bin")))
            {
                //var xws = new XmlWriterSettings();
                //xws.Indent = true;
                var xw = new BinaryXMLWritter(file);
                //var xw = XmlWriter.Create(file, xws);
                //xw.WriteStartDocument();
                xw.WriteStartElement("Cache");
                xw.WriteStartElement("CIS");
                foreach (CurrencyInfo ci in CurrencyComboBox.Items)
                {
                    xw.WriteStartElement("CI");
                    xw.WriteAttributeString("Name", ci.ForexName);
                    xw.WriteEndElement();
                }
                xw.WriteEndElement();

                var dbs = GetLoadedDBsAsList();
                dbs.Sort((db1, db2) => db2.Date.CompareTo(db1.Date));

                foreach (DBParser db in dbs)
                {
                    db.Save(xw);
                }
                xw.WriteEndElement();
                xw.Flush();
            }
        }

        private void GeneratorsListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            UpdateLevels();
        }

        private void GeneratorsListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (GeneratorsListView.SelectedItems.Count > 0)
            {
                var generator = GeneratorsListView.SelectedItems[0].Tag as LevelsGenerator;

                LevelsGeneratorPropertyGrid.SelectedObject = generator;

                //generator.Currency = CurrentCurrency;
                //generator.DbParser = DBListView.SelectedItems.Count == 1 ? (DBParser)DBListView.SelectedItems[0].Tag : null;
            }
        }

        void Settings_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            UpdateLevels();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            SaveSettings();
        }

        private void SaveSettings()
        {
            XmlDocument doc = new XmlDocument();
            var settings = doc.CreateElement("OptLSettings");
            doc.AppendChild(settings);

            var parsers = GetLoadedDBsAsList();

            settings.AppendChild(doc.CreateElement("CurrencyInfoSettings"));
            foreach (CurrencyInfo ci in CurrencyComboBox.Items)
                ci.SaveSettings(doc, parsers);

            settings.AppendChild(doc.CreateElement("GeneratorSettings"));
            foreach (ListViewItem lvi in GeneratorsListView.Items)
            {
                var gen = (LevelsGenerator)lvi.Tag;

                var elementName = gen.GetType().Name;
                var generatorSettings = doc.CreateElement("Generator");
                doc.DocumentElement["GeneratorSettings"].AppendChild(generatorSettings);
                generatorSettings.SetAttribute("Name", elementName);
                gen.SaveSettings(generatorSettings);
            }

            if (saveTemplateDirectory != "")
            {
                var std = doc.CreateElement("SaveTemplateDirectory");
                settings.AppendChild(std);
                std.AppendChild(doc.CreateTextNode(saveTemplateDirectory));
            }

            if (dbDirectory != "")
            {
                var dd = doc.CreateElement("DbDirectory");
                settings.AppendChild(dd);
                dd.AppendChild(doc.CreateTextNode(dbDirectory));
            }

            if (CurrentCurrency != null)
            {
                var lc = doc.CreateElement("LastCurrency");
                settings.AppendChild(lc);
                lc.AppendChild(doc.CreateTextNode(CurrentCurrency.ForexName));
            }

            if (autoRemember.Checked)
            {
                var lc = doc.CreateElement("AutoRemember");
                settings.AppendChild(lc);
                lc.AppendChild(doc.CreateTextNode("true"));
            }

            doc.Save(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OptLSettings.xml"));
        }

        private List<DBParser> GetLoadedDBsAsList()
        {
            var parsers = new List<DBParser>();
            foreach (ListViewItem lvi in DBListView.Items)
                parsers.Add((DBParser)lvi.Tag);
            return parsers;
        }

        private void LoadSettings()
        {
            var settingsFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OptLSettings.xml");

            if (!File.Exists(settingsFile))
                return;

            XmlDocument doc = new XmlDocument();
            doc.Load(settingsFile);

            var settings = (XmlElement)(doc["OptLSettings"]);

            var parsers = new Dictionary<DateTime, List<DBParser>>();
            foreach (ListViewItem lvi in DBListView.Items)
            {
                var parser = (DBParser)lvi.Tag;
                if (!parsers.ContainsKey(parser.Date))
                    parsers.Add(parser.Date, new List<DBParser>());
                parsers[parser.Date].Add(parser);
            }

            var currencyInfoSettings = settings[@"CurrencyInfoSettings"];

            foreach (CurrencyInfo ci in CurrencyComboBox.Items)
            {
                var ciSettings = (XmlElement)(currencyInfoSettings.SelectSingleNode(@"CurrencyInfo[@ForexName='" + ci.ForexName + "']"));
                if (ciSettings != null)
                    ci.LoadSettings(ciSettings, parsers);
            }

            var generatorSettings = settings[@"GeneratorSettings"];
            foreach (ListViewItem lvi in GeneratorsListView.Items)
            {
                var gen = (LevelsGenerator)lvi.Tag;

                var genSettings = (XmlElement)(generatorSettings.SelectSingleNode(@"Generator[@Name='" + gen.GetType().Name + "']"));
                if (genSettings != null)
                    gen.LoadSettings(genSettings);
            }

            var lastCurrencySettings = (XmlElement)settings[@"LastCurrency"];
            if (lastCurrencySettings != null)
            {
                var forexName = lastCurrencySettings.InnerText.Trim();
                foreach (var item in CurrencyComboBox.Items.Cast<CurrencyInfo>())
                {
                    if (item.ForexName == forexName)
                    {
                        CurrencyComboBox.SelectedItem = item;
                        break;
                    }
                }
            }

            var saveTemplateDirectory = (XmlElement)settings[@"SaveTemplateDirectory"];
            if (saveTemplateDirectory != null)
            {
                this.saveTemplateDirectory = saveTemplateDirectory.InnerText.Trim();
            }

            var dbDirectory = (XmlElement)settings[@"DbDirectory"];
            if (dbDirectory != null)
            {
                this.dbDirectory = dbDirectory.InnerText.Trim();
            }

            autoRemember.Checked = 
                settings["AutoRemember"] != null && settings["AutoRemember"].InnerText.Trim().ToLower() == "true";
        }

        public Dictionary<DateTime, DBParser> GetLoadedDbs(out StringBuilder errorMessageBuilder)
        {
            errorMessageBuilder = null;

            var res = new Dictionary<DateTime, DBParser>();
            var dates = new Dictionary<DateTime, object>();

            foreach (ListViewItem li in DBListView.Items)
            {
                var db = (DBParser)li.Tag;
                if (res.ContainsKey(db.Date))
                {
                    if (errorMessageBuilder == null)
                    {
                        errorMessageBuilder = new StringBuilder();
                        errorMessageBuilder.AppendLine("Для указанных ниже дат имеется более одного бюллетеня. Удалите лишние и повторите операцию.");
                    }
                    if (!dates.ContainsKey(db.Date))
                    {
                        errorMessageBuilder.Append(db.Date.ToShortDateString() + " ");
                        dates.Add(db.Date, null);
                    }
                }
                else
                    res.Add(db.Date, db);
            }
            return res;
        }

        private void StrikeExplorer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var se = new StrikeExplorer();

            se.SetDBs(GetLoadedDBsAsList(), (CurrencyInfo)CurrencyComboBox.SelectedItem);
            se.ShowDialog();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="loadedDBs"></param>
        /// <param name="outOfMoney"></param>
        /// <param name="contractCount">0 = все</param>
        /// <returns></returns>
        private string GenerateExport_Dengaz(Dictionary<DateTime, DBParser> loadedDBs, bool outOfMoney, int contractCount)
        {
            var ci = CurrentCurrency;
            //bool IgnoreOiThreshold = false;
            //bool HideCab = false;

            StringBuilder sb = new StringBuilder();

            var cult = System.Globalization.CultureInfo.GetCultureInfo("ru-ru");

            foreach (var db in loadedDBs.Values)
            {
                var callSection = db.GetSection(ci.CallSection);
                var putSection = db.GetSection(ci.PutSection);

                //1,2 контракты

                //CALL с мин. раст до закр. фью, нужны страйки меньше, но не он сам
                //    Просуммировать OI
                //    Просуммировать OICH

                //PUT  с мин. раст до закр. фью, все которые больше, но не он сам
                //    Просуммировать OI
                //    Просуммировать OICH

                //Дата торгов, 1 конт CALL OI, CALL OICH, PUT ...


                var calls = callSection
                    .Contracts.FindAll(c => c.Name == ci.CallContractName);

                var puts = putSection
                    .Contracts.FindAll(c => c.Name == ci.PutContractName);

                

                //var minCall1Index = ValJel_ClosestStrike(futuresClose, calls[0].Strikes);
                //var minCall2Index = ValJel_ClosestStrike(futuresClose, calls[1].Strikes);
                //var minPut1Index = ValJel_ClosestStrike(futuresClose, puts[0].Strikes);
                //var minPut2Index = ValJel_ClosestStrike(futuresClose, puts[1].Strikes);

                var SUMM_OI_C_1 = 0;
                //var SUMM_OI_C_2 = 0;
                var SUMM_OICH_C_1 = 0;
                //var SUMM_OICH_C_2 = 0;
                var SUMM_OI_P_1 = 0;
                //var SUMM_OI_P_2 = 0;
                var SUMM_OICH_P_1 = 0;
                //var SUMM_OICH_P_2 = 0;

                int contractN = 1;
                foreach (var callContract in calls)
                {
                    if (contractCount != 0 && contractN > contractCount)
                        break;

                    var futuresClose = callSection.GetFuturesByOption(callContract.Date).SettPrice;
                    foreach (var call in callContract.Strikes)
                    {
                        var sv = call.StrikeValue * ci.StrikeMultiplier;
                        if ((!outOfMoney && sv <= futuresClose) || (outOfMoney && sv > futuresClose))
                        {
                            SUMM_OI_C_1 += call.OpenInterest;
                            SUMM_OICH_C_1 += call.OpenInterestChange;
                        }
                    }
                    contractN++;
                }

                contractN = 1;
                foreach (var putContract in puts)
                {
                    if (contractCount != 0 && contractN > contractCount)
                        break;

                    var futuresClose = callSection.GetFuturesByOption(putContract.Date).SettPrice;
                    foreach (var put in putContract.Strikes)
                    {
                        var sv = put.StrikeValue * ci.StrikeMultiplier;
                        if ((!outOfMoney && sv >= futuresClose) || (outOfMoney && sv < futuresClose))
                        {
                            SUMM_OI_P_1 += put.OpenInterest;
                            SUMM_OICH_P_1 += put.OpenInterestChange;
                        }
                    }
                    contractN++;
                }

                //futuresClose = callSection.GetFuturesByOption(calls[1]).SettPrice;

                //foreach (var call in calls[1].Strikes)
                //{
                //    var sv = call.StrikeValue * ci.StrikeMultiplier;
                //    if ((!outOfMoney && sv <= futuresClose) || (outOfMoney && sv > futuresClose))
                //    {
                //        SUMM_OI_C_2 += call.OpenInterest;
                //        SUMM_OICH_C_2 += call.OpenInterestChange;
                //    }
                //}

                //foreach (var put in puts[1].Strikes)
                //{
                //    var sv = put.StrikeValue * ci.StrikeMultiplier;
                //    if ((!outOfMoney && sv >= futuresClose) || (outOfMoney && sv < futuresClose))
                //    {
                //        SUMM_OI_P_2 += put.OpenInterest;
                //        SUMM_OICH_P_2 += put.OpenInterestChange;
                //    }
                //}

                sb.AppendLine(string.Join(";", new string[] {
                    db.Date.ToString("dd.MM.yyyy"),
                    SUMM_OI_C_1.ToString(cult),
                    SUMM_OICH_C_1.ToString(cult),
                    SUMM_OI_P_1.ToString(cult),
                    SUMM_OICH_P_1.ToString(cult),
                    //SUMM_OI_C_2.ToString(cult),
                    //SUMM_OICH_C_2.ToString(cult),
                    //SUMM_OI_P_2.ToString(cult),
                    //SUMM_OICH_P_2.ToString(cult)
                }));
            }
            return sb.ToString();
        }

        private int ValJel_ClosestStrike(decimal futuresClose, List<Strike> strikes)
        {
            var ci = this.CurrentCurrency;
            decimal min = decimal.MaxValue;
            var minIndex = -1;

            for (int i = 0; i < strikes.Count; i++)
            {
                var strike = strikes[i];
                var v = Math.Abs(futuresClose - strike.StrikeValue * ci.StrikeMultiplier);
                if (v < min)
                {
                    min = v;
                    minIndex = i;
                }
            }
            return minIndex;
        }
    }


    class ItemSorter : IComparer
    {
        public int SortColumn;
        public int Compare(object x, object y)
        {
            var xx = (DBParser)(((ListViewItem)x).Tag);
            var yy = (DBParser)(((ListViewItem)y).Tag);

            switch (SortColumn)
            {
                case 0: return 0;
                case 1: return xx.FileName.CompareTo(yy.FileName);// xx.ForwardPoints.CompareTo(yy.ForwardPoints);
                case 2: return xx.LevelDate.CompareTo(yy.LevelDate);
                case 3: return xx.Date.CompareTo(yy.Date); 
                case 4: return xx.BulletinType.CompareTo(yy.BulletinType);
                default:
                    throw new Exception();
            }
        }
    }
}

