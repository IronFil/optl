﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Text;
using OptL;
using System.ComponentModel;
using System.Xml;

namespace MedianPrice
{
    public class MedianPriceLevelsGenerator : LevelsGenerator
    {
        public override void GenerateTemplate(ITemplateBuilder templateBuilder, CurrencyInfo ci, IEnumerable<DBParser> dbs)
        {
            foreach (var db in dbs)
            {
                GenerateDayLevels(templateBuilder, ci, db);
            }
        }

        public void GenerateDayLevels(ITemplateBuilder templateBuilder, CurrencyInfo ci, DBParser l)
        {
            var bc = new BalanceComparer();

            bc.calls = new List<OptLevel>();
            bc.puts = new List<OptLevel>();

            var callSection = l.GetSection(ci.CallSection);
            var putSection = l.GetSection(ci.PutSection);

            if (callSection.Contracts.Count == 0)
                return;

            decimal min = decimal.MaxValue;
            decimal max = decimal.MinValue;

            foreach (var s in callSection.Contracts.Find(c => c.Name.StartsWith(ci.CallContractName)).Strikes.FindAll(s=>s.OpenInterest > 0))
            {
                var ol = new OptLevel(s, ci, true);
                bc.calls.Add(ol);
                min = Math.Min(ol.Level, min);
                max = Math.Max(ol.Level, max);
            }

            foreach (var s in putSection.Contracts.Find(c => c.Name.StartsWith(ci.PutContractName)).Strikes.FindAll(s =>s.OpenInterest > 0))
            {
                var ol = new OptLevel(s, ci, false);
                bc.puts.Add(ol);
                min = Math.Min(ol.Level, min);
                max = Math.Max(ol.Level, max);
            }

            DateTime levelDate;

            switch (l.Date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    levelDate = l.Date.AddDays(1); break;
                case DayOfWeek.Friday:
                    levelDate = l.Date.AddDays(3);
                    break;
                default:
                    throw new Exception();
            }

            List<decimal> balances = new List<decimal>();

            decimal step = (max - min) / 10000;
            for (decimal i = min; i < max; i += step)
                balances.Add(i);
          
            decimal res = 0;

            //bc.BestMatchDelta = decimal.MaxValue; bc.BestMatch = 0; bc.IgnoreCalls = false; bc.IgnorePuts = false;
            //res = balances.BinarySearch(0.001m, bc);
            //var balanceW = bc.BestMatch;

            bc.Method = this.WeightingMethod;
            bc.BestMatchDelta = decimal.MaxValue; bc.BestMatch = 0; bc.IgnoreCalls = true; bc.IgnorePuts = false;
            res = balances.BinarySearch(0.001m, bc);
            var putWeightLevel = bc.BestMatch;

            //var sb = new StringBuilder();
            //foreach (var level in bc.puts)
            //{
            //    sb.Append(level.Strike.StrikeValue);
            //    sb.Append("\t");
            //    sb.Append(level.Strike.OpenInterestChange);
            //    sb.Append("\t");
            //    sb.Append(level.Strike.VolumeTradesCleared);
            //    sb.Append("\t");
            //    sb.Append(level.Strike.SettPrice);
            //    sb.Append("\t");
            //    sb.Append(level.Level);
            //    sb.Append("\t");
            //    var w = (level.Strike.OpenInterestChange > 0 ? level.Strike.VolumeTradesCleared * level.Strike.SettPrice : 0) * level.Strike.SettPrice;
            //    sb.AppendLine(w.ToString());
            //}
            //sb.AppendLine(bc.BestMatch.ToString());

            bc.Method = this.WeightingMethod;
            bc.BestMatchDelta = decimal.MaxValue; bc.BestMatch = 0; bc.IgnoreCalls = false; bc.IgnorePuts = true;
            res = balances.BinarySearch(0.001m, bc);
            var callWeightLevel = bc.BestMatch;

            var balanceY = (putWeightLevel + callWeightLevel) / 2;

            templateBuilder.AddLevel(putWeightLevel, putWeightLevel, levelDate, l, 1f, "", "", "K2_TOP2");
            templateBuilder.AddLevel(callWeightLevel, callWeightLevel, levelDate, l, 1f, "", "", "K2_BOTTOM2");
            //templateBuilder.AddLevel(balanceW, balanceW, levelDate, l, 1f, "", "", "K2_BALANCEW");
            templateBuilder.AddLevel(balanceY, balanceY, levelDate, l, 1f, "", "", "K2_BALANCEW");
        }

        //LevelsGeneratorSettings settings = new LevelsGeneratorSettings();

        //public override LevelsGeneratorSettings Settings
        //{
        //    get { return settings; }
        //}

        class BalanceComparer : IComparer<decimal>
        {
            public List<OptLevel> calls;
            public List<OptLevel> puts;

            public decimal BestMatchDelta = decimal.MaxValue;
            public decimal BestMatch;

            public bool IgnoreCalls = false;
            public bool IgnorePuts = false;

            public WeightingMethod Method { get; set; }

            void CalculateWeights(decimal balancePoint, out decimal left, out decimal right, List<OptLevel> calls, List<OptLevel> puts)
            {
                left = 0;
                right = 0;

                if (!IgnoreCalls)
                    foreach (var level in calls)
                    {
                        decimal w;
                        if (Method == WeightingMethod.Classic)
                        {
                            w = (level.Strike.OpenInterestChange > 0 ?
                                level.Strike.VolumeTradesCleared * level.Strike.SettPrice : 0) * level.Strike.SettPrice;
                        }
                        else
                        {
                            w = (level.Strike.OpenInterest + level.Strike.VolumeTradesCleared) * level.Strike.SettPrice * level.Strike.SettPrice;
                        }

                        if (level.Level < balancePoint) left += w;
                        else if (level.Level > balancePoint) right += w;
                    }

                if (!IgnorePuts)
                    foreach (var level in puts)
                    {
                        decimal w;
                        if (Method == WeightingMethod.Classic)
                        {
                            w =
                                (level.Strike.OpenInterestChange > 0 ?
                                level.Strike.VolumeTradesCleared * level.Strike.SettPrice : 0) * level.Strike.SettPrice;
                        }
                        else
                        {
                            w = (level.Strike.OpenInterest + level.Strike.VolumeTradesCleared) * level.Strike.SettPrice * level.Strike.SettPrice;
                        }

                        if (level.Level < balancePoint) left += w;
                        else if (level.Level > balancePoint) right += w;
                    }

            }

            public int Compare(decimal x, decimal y)
            {
                decimal left1;
                decimal right1;
                CalculateWeights(x, out left1, out right1, calls, puts);

                if (Math.Abs(left1 - right1) < BestMatchDelta)
                {
                    BestMatchDelta = Math.Abs(left1 - right1);
                    BestMatch = x;
                }

                return left1 < right1 ? -1 : 1;
            }
        }

        public override void GenerateText(StringBuilder sb, CurrencyInfo ci, bool spot, DBParser selectedDb)
        {
            base.GenerateText(sb, ci, spot, selectedDb);
        }

        public override string ToString()
        {
            return "Средняя цена";
        }

        private WeightingMethod weightingMethod = WeightingMethod.Classic;
        [DisplayName("Метод расчёта")]
        [Description("")]
        [DefaultValue(WeightingMethod.Classic)]
        public WeightingMethod WeightingMethod
        {
            get
            {
                return weightingMethod;
            }
            set
            {
                if (weightingMethod != value)
                {
                    weightingMethod = value;
                    OnPropertyChanged("WeightingMethod");
                }
            }
        }

        public override void SaveSettings(XmlElement settings)
        {
            settings.SetAttribute("WeightingMethod", this.WeightingMethod.ToString());
        }

        public override void LoadSettings(XmlElement genSettings)
        {
            if (genSettings.HasAttribute("WeightingMethod"))
                WeightingMethod = (WeightingMethod)Enum.Parse(typeof(WeightingMethod), genSettings.GetAttribute("WeightingMethod"));
        }
    }

    public enum WeightingMethod
    {
        [FieldDisplayName("Классический")]
        Classic,
        [FieldDisplayName("(Volume+OI)*SettPrice^2")]
        Roma,
    }
}
