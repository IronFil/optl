﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.ComponentModel;

namespace OptL
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
        //    Volumes();
        //    return;
            //currencyInfo.Add(new CurrencyInfo("S&P", "EMINI S&P CALL", "EMINI S&P PUT", 47, 48));

            //(Страйк * 0,001)   +/- (премия * 0,001) евро
            //(Страйк * 0,001)   +/- (премия *  0,01) фунт
            //(Страйк * 0,00001) +/- (премия * 0,001) ена

            TypeDescriptor.AddProvider(new EnumTypeDescriptionProvider(), typeof(Enum));

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }

    class EnumTypeDescriptionProvider : TypeDescriptionProvider
    {
        Dictionary<Type, EnumTypeDescriptor> typeConverters =
            new Dictionary<Type, EnumTypeDescriptor>();

        public override ICustomTypeDescriptor GetTypeDescriptor(Type objectType, object instance)
        {
            if (!typeConverters.ContainsKey(objectType))
                typeConverters.Add(objectType, new EnumTypeDescriptor(new MyTypeConverter(objectType)));
            return typeConverters[objectType];
        }

        class EnumTypeDescriptor : CustomTypeDescriptor
        {
            private MyTypeConverter myTypeConverter;

            public EnumTypeDescriptor(MyTypeConverter myTypeConverter)
            {
                this.myTypeConverter = myTypeConverter;
            }

            public override TypeConverter GetConverter()
            {
                return myTypeConverter;
            }
        }

        class MyTypeConverter : TypeConverter
        {
            Dictionary<string, object> descr2value = new Dictionary<string, object>();
            Dictionary<object, string> value2descr = new Dictionary<object, string>();

            private Type objectType;

            public MyTypeConverter(Type objectType)
            {
                this.objectType = objectType;

                foreach (var field in objectType.GetFields())
                {
                    if (field.FieldType == objectType)
                    {
                        object value = Enum.Parse(objectType, field.Name);

                        var attrs = field.GetCustomAttributes(typeof(FieldDisplayNameAttribute), false);
                        if (attrs.Length == 0)
                        {
                            value2descr.Add(value, field.Name);
                            descr2value.Add(field.Name, value);
                        }
                        else
                        {
                            value2descr.Add(value, ((FieldDisplayNameAttribute)attrs[0]).DisplayName);
                            descr2value.Add(((FieldDisplayNameAttribute)attrs[0]).DisplayName, value);
                        }
                    }
                }
            }

            public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
            {
                return true;
            }

            public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
            {
                return true;
            }

            public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
            {
                return descr2value[(string)value];
            }

            public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
            {
                if (value is string)
                    return descr2value[(string)value];
                else
                    return value2descr[value];
            }

            public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
            {
                return true;
            }

            public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
            {
                var stdVals = new List<object>();

                foreach (var v in Enum.GetValues(objectType))
                    stdVals.Add(v);


                //var stdVals = new List<ClassicOptionLevelStyle>() {
                //        ClassicOptionLevelStyle.Normal, ClassicOptionLevelStyle.Volumes,
                //    ClassicOptionLevelStyle.OpenInterest, ClassicOptionLevelStyle.OpenInterestSetPrice,
                //    ClassicOptionLevelStyle.VolumesSetPrice};
                return new StandardValuesCollection(stdVals);
            }

            internal static MyTypeConverter Create(Enum type)
            {
                throw new NotImplementedException();
            }
        }
    }

    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = false)]
    public sealed class FieldDisplayNameAttribute : System.ComponentModel.DisplayNameAttribute
    {
        public FieldDisplayNameAttribute(string displayName)
            : base(displayName)
        {
        }
    }
}
