﻿// This file is a part of OptL software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to create MetaTrader 4 templates from CME Group Inc. Daily bulletin reports
// This version of OptL and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты OptL, созданной Тимуром Вишняковым (tumypv@yandex.ru)
// для создания шаблонов MetaTrader 4 по данным из отчётов Чикагской биржи
// Эта версия OptL и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace OptL
{
    public partial class ProcessingForm : Form
    {
        public List<DBParser> Result = null;

        public string[] fileNames { get; set; }
        IEnumerable<CurrencyInfo> cis;

        public ProcessingForm()
        {
            InitializeComponent();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            var res = new List<DBParser>();

            float n = 0;
            foreach (var fileName in fileNames)
            {
                n++;
                res.Add(LoadDB(fileName, cis));
                if (backgroundWorker1.CancellationPending)
                {
                    res = null;
                    e.Cancel = true;
                    break;
                }

                backgroundWorker1.ReportProgress((int)((n / fileNames.Length) * 100));
            }
            e.Result = res;
        }

        private DBParser LoadDB(string fileName, IEnumerable<CurrencyInfo> cis)
        {
            var l = new DBParser();
            l.Path = Path.GetDirectoryName(fileName);
            l.FileName = Path.GetFileNameWithoutExtension(fileName);

            CMEDB.CMEDBParser parser = new CMEDB.CMEDBParser();

            const string prefixSection = "Section";

            using (var fs = File.OpenRead(fileName))
            using (var zs = new ICSharpCode.SharpZipLib.Zip.ZipInputStream(fs))
            {
                ICSharpCode.SharpZipLib.Zip.ZipEntry entry = null;
                for (; ; )
                {
                    entry = zs.GetNextEntry();
                    if (entry == null)
                        break;
                    string entryName = Path.GetFileName(entry.Name);
                    if (string.IsNullOrEmpty(entryName) || !entryName.StartsWith(prefixSection))
                        continue;

                    int sectionCode = int.Parse(entryName.Substring(prefixSection.Length, 2));

                    int year = int.Parse(Path.GetFileName(fileName).Substring(18, 4));

                    var parse = false;
                    foreach (var ci in cis)
                        if (ci.CallSection == sectionCode || ci.PutSection == sectionCode
                            || (year >= 2012 && ci.FutSection == sectionCode))
                        {
                            parse = true;
                            break;
                        }

                    if (parse)
                        parser.Parse(zs, l);
                }
            }

            return l;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar1.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                Result = (List<DBParser>)e.Result;
            }
        }

        internal void Start(string[] fileNames, IEnumerable<CurrencyInfo> cis)
        {
            this.cis = cis;
            this.fileNames = fileNames;
            backgroundWorker1.RunWorkerAsync();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            backgroundWorker1.CancelAsync();
        }
    }
}
