﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл содержит вспомогательный код, которому не нашлось места в других файлах

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CMEDB
{
    public static class Utils
    {
        public static string FillMask(string fileOut, int sectionCode, DateTime bulletinDate, int bulletinNumber)
        {
            var currency = CurrencyInfo.AvailableCurrencyInfoList.Find(ci => ci.CallSection == sectionCode || ci.PutSection == sectionCode);
            // #CP на "C" если в бюллетене только CALL, на "P" если только PUT, на пустую строку если бюллетень смешенный
            // #FX на EUR для Евро, GBP для Фунта и т.д.

            return fileOut
                .Replace("#DD", bulletinDate.ToString("dd"))
                .Replace("#MM", bulletinDate.ToString("MM"))
                .Replace("#YYYY", bulletinDate.ToString("yyyy"))
                .Replace("#YY", bulletinDate.ToString("yy"))
                .Replace("#SC", sectionCode.ToString("00"))
                .Replace("#BN", bulletinNumber.ToString())
                .Replace("#CP", currency.PutSection == currency.CallSection ? "" : (currency.CallSection == sectionCode ? "C" : "P"))
                .Replace("#FX", currency.ForexName);

        }

        public static string[] DBMonth = new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

        public static DateTime ParseContractDate(string MMM, string YY)
        {
            DateTime res;
            if (TryParseContractDate(MMM, YY, out res))
                return res;
            else
                throw new FormatException();
        }

        public static bool TryParseContractDate(string MMM, string YY, out DateTime contractDate)
        {
            return DateTime.TryParse(MMM + " 1, " + YY, out contractDate);
        }

    }
}
