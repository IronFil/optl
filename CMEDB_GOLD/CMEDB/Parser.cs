﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл содержит основной алгоритм конвертера

using System;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Drawing;
using System.IO;
using CMEDB;
using System.Xml;

namespace CMEDB
{
    public class CMEDBParser
    {
        ICMEDBParserListener parserListener;

        public static List<DBPDFFormatInfo> supportedFormats = new List<DBPDFFormatInfo>();
        DBPDFFormatInfo DBPDFFormatInfo;

        PdfPageHeadingInfo headerInfo;

        //Указывает, что ма нашли место, откуда начинаются данные для импорта страйков опционного контракта
        bool dataMarkerFoundOpt;
        bool dataMarkerFoundFut;

        public int LineParseErrorCount { get; private set; }

        System.Text.RegularExpressions.Regex containsTotal = new System.Text.RegularExpressions.Regex(@"T\s*O\s*T\s*A\s*L",
            System.Text.RegularExpressions.RegexOptions.Compiled | System.Text.RegularExpressions.RegexOptions.IgnoreCase);

        static CMEDBParser()
        {
            var types = System.Reflection.Assembly.GetExecutingAssembly().GetTypes();
            foreach (var type in types)
            {
                if (type.IsSubclassOf(typeof(DBPDFFormatInfo)))
                {
                    supportedFormats.Add((DBPDFFormatInfo)type.GetConstructor(Type.EmptyTypes).Invoke(null));
                }
            }
            // Более новые версии должны идти раньше, чтобы при поиске иметь возможность сработать первыми
            supportedFormats.Sort((f1,f2) => - f1.Version.CompareTo(f2.Version));
        }

        public void Parse(Stream source, ICMEDBParserListener parserListener)
        {
            PdfReader reader = new PdfReader(source);
            ParseInternal(reader, "", parserListener);
        }

        public void Parse(string srcPath, ICMEDBParserListener parserListener)
        {
            PdfReader reader = new PdfReader(srcPath);
            ParseInternal(reader, srcPath, parserListener);
        }

        private void ParseInternal(PdfReader reader, string sourceName, ICMEDBParserListener parserListener)
        {
            LineParseErrorCount = 0;

            this.parserListener = parserListener;

            List<string> lines = new List<string>();

            dataMarkerFoundOpt = false;
            dataMarkerFoundFut = false;

            this.DBPDFFormatInfo = null;

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                var s = new CMEDB_ExtractionStrategy();
                IRenderListener listener = new FilteredTextRenderListener(s, new RenderFilter[] { });
                PdfContentStreamProcessor processor = new PdfContentStreamProcessor(listener);
                PdfDictionary pageDic = reader.GetPageN(i);
                PdfDictionary resourcesDic = pageDic.GetAsDict(PdfName.RESOURCES);

                processor.ProcessContent(ContentByteUtils.GetContentBytesForPage(reader, i), resourcesDic);

                if (this.DBPDFFormatInfo == null)
                {
                    if (i != 1)
                        throw new Exception();
                    var text = s.GetResultantText();
                    this.DBPDFFormatInfo = DetectSectionType(text);
                }
                var formattedText = s.GetFormattedText(this.DBPDFFormatInfo);
                if (!Refine(formattedText, lines, i))
                    break;
            }

            if (parserListener != null)
                parserListener.ProcessSectionInfo(headerInfo.bulletinSectionCode, headerInfo.bulletinName, headerInfo.bulletinNumber, headerInfo.bulletinDate, headerInfo.IsFinal);

            Parse(lines);
        }

        DBPDFFormatInfo DetectSectionType(string src)
        {
            string line;
            var sr = new StringReader(src);
            bool found = false;

            string lastLine = "";
            // Обработка верхнего колонтитула страницы
            while (!found && (line = sr.ReadLine()) != null)
            {
                var tr = line.Replace(" ", "");

                if (!(tr.Contains("PG") && tr.Contains("BULLETIN")))
                {
                    lastLine = tr;
                    continue;
                }

                    foreach (var f in supportedFormats)
                    {
                        if (f.TryParsePageHeader(tr, lastLine, out headerInfo))
                        {
                            DBPDFFormatInfo = f;
                            return f;
                        }
                    }
            }
            throw new Exception("Section is not supported");
        }

        /// <summary>
        /// Возвращает True для линий, однозначно сигнализирующих о конце нужных данных в DB
        /// </summary>
        /// <returns></returns>
        bool IsStopLine(string tr)
        {
            return ((DBPDFFormatInfo is GOLD_DBPDFFormatInfo || DBPDFFormatInfo is ENERGY_DBPDFFormatInfo)
                && tr.Replace(" ", "").StartsWith("OPTIONSEOO'SANDBLOCKS"));  // у металлов и энергоносителей после этого начинается левый хлам
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="res"></param>
        /// <param name="pageN"></param>
        /// <returns>true, если есть смысл просматривать оставшиеся страницы</returns>
        bool Refine(string src, List<string> res, int pageN)
        {
            string line;
            var sr = new StringReader(src);
            bool found = false;
            string lastLine=null;
            // Обработка верхнего колонтитула страницы
            while (!found && (line = sr.ReadLine()) != null)
            {
                var tr = line.Replace(" ","");
                if (IsStopLine(tr)) return false;

                if (!(tr.Contains("PG") && tr.Contains("BULLETIN")))
                {
                    lastLine = tr;
                    continue;
                }

                PdfPageHeadingInfo currentPageHeaderInfo;
                if (DBPDFFormatInfo.TryParsePageHeader(tr, lastLine, out currentPageHeaderInfo))
                {
                    if (!this.headerInfo.Equals(currentPageHeaderInfo))
                        throw new Exception();
                    else
                        found = true;
                }
            }
            if (!found)
            {
                if (pageN < 3)
                    return true; // Фьючерсы могут занимать более 1 страницы, например у Сои (57)
                else
                    throw new Exception();
            }

            if (DBPDFFormatInfo is ENERGY_DBPDFFormatInfo) // У энергоносителей перед данными идёт колонтитул с предупреждениями, а занчит нужно искать маркер данных на каждой странице
                dataMarkerFoundOpt = false;

            found = false;
            // На первой странице маркер данных, на остальных не нужно - там данные должны идти сразу за колонтитулом
            if (!dataMarkerFoundOpt && !dataMarkerFoundFut)
            {
                while ((line = sr.ReadLine()) != null)
                {
                    var tr = line.Replace(" ","");
                    if (IsStopLine(tr)) return false;

                    if (DBPDFFormatInfo.IsStartOptMarker(line))
                    {
                        dataMarkerFoundOpt = true;
                        break;
                    }
                    else if (DBPDFFormatInfo.IsStartFutMarker(line))
                    {
                        dataMarkerFoundFut = true;
                        break;
                    }
                }

                if (!dataMarkerFoundOpt && !dataMarkerFoundFut)
                    return true; // На странице нет маркера
                else
                    res.Add(line);
            }

            found = false;
            while ((line = sr.ReadLine()) != null)
            {
                var tr = line.Replace(" ","");
                if (IsStopLine(tr)) return false;

                if (DBPDFFormatInfo is GOLD_DBPDFFormatInfo && (tr.StartsWith("GLOBEX") || tr.StartsWith("OPEN") || tr.StartsWith("CISES") || tr.StartsWith("VOLUME")))
                {
                    // Заголовочная строка, игнорим
                    continue;
                }

                if (DBPDFFormatInfo is GOLDFUT_DBPDFFormatInfo && (tr.StartsWith("SETT.PRICE") || tr.StartsWith("OPEN") || tr.StartsWith("&PT.CHGE") || tr.StartsWith("GLOBEXOUTCRY")))
                {
                    // Заголовочная строка, игнорим
                    continue;
                }

                if (DBPDFFormatInfo is Soybean_DBPDFFormatInfo && (tr.StartsWith("SOYBEANCRUSH,WHEATANDCSOWHEATOPTIONS")
                    || tr.StartsWith("SOYBEANPUTS")
                    || tr == "VOLUME"
                    || tr.StartsWith("SETT.PRICE")
                    || tr.StartsWith("STRIKEOPENRANGE")
                    || tr.StartsWith("PT.CHGE.")
                ))
                {
                    // Заголовочная строка, игнорим
                    continue;
                }

                if (tr.StartsWith("THEINFORMATIONCONTAINED") || tr.StartsWith("VOLUMEANDOPENINTEREST\"RECORDS\"SHOWNFORCBOTPRODUCTS"))
                    return true;

                res.Add(line);
            }
            throw new Exception();
        }

        private bool Parse(List<string> lines)
        {
            DateTime? contractDate = null;
            var contractDateStr = "";
            string comod = "";
            decimal futuresSettPrice = 0;

            bool readingStrikes = false;
            bool readingFut = false;

            bool startDataMarkerInStrikes = false;
            bool lookingForDateMarker = false;
            

            int lineN = 0;

            foreach (var line in lines)
            {
                lineN++;

                var tr = line.Trim();

                if (readingStrikes)
                {
                    if (DBPDFFormatInfo.IsStrike(line))
                    {
                        Strike s = null;

                        Exception ex = null;

                        try
                        {
                            s = Strike.Parse(comod, contractDate.Value, line, DBPDFFormatInfo);
                        }
                        catch (Exception _ex)
                        {
                            LineParseErrorCount++;
                            ex = _ex;
                        }

                        if (ex == null)
                        {
                            if (parserListener != null)
                                parserListener.ProcessStrike(s);
                        }
                        else
                        {
                            if (parserListener != null)
                                parserListener.Error(ex, line);
                        }
                    }
                    else if (containsTotal.IsMatch(tr))
                    {
                        int pdf_total_vol;
                        int pdf_total_oi;
                        int pdf_total_oi_ch;
                        ParsePdfTotal(tr, out pdf_total_vol, out pdf_total_oi, out pdf_total_oi_ch);

                        if (parserListener != null)
                        {
                            parserListener.ProcessTotal(pdf_total_vol, pdf_total_oi, pdf_total_oi_ch, comod, contractDate.Value, futuresSettPrice);
                        }
                        readingStrikes = false;
                        lookingForDateMarker = true;
                    }
                    else if (DBPDFFormatInfo.IsStartOptMarker(line))
                    {
                        var newComod = DBPDFFormatInfo.FormatOptionComodName(line.Trim());
                        if (comod != newComod)
                            throw new Exception();
                        startDataMarkerInStrikes = true;
                    }
                    else if (tr == contractDateStr) // В СОЕ может встретиться дата контракта два раза подряд
                    {

                    }
                    else
                    {
                        if (startDataMarkerInStrikes)
                        {
                            if (!tr.Contains("FUTURES") || !tr.Contains("SETT."))
                            {
                                string mmm = tr.Substring(0, 3);
                                string yy = tr.Substring(3, 2);
                                var cd = Utils.ParseContractDate(mmm, yy);
                                if (contractDate != cd)
                                    throw new Exception();
                            }
                            startDataMarkerInStrikes = false;
                        }
                        else
                            throw new Exception();
                    }
                }
                else if (readingFut)
                {
                    if (containsTotal.IsMatch(tr))
                    {
                        ParseFutTotal(line, comod);
                        readingFut = false;
                        lookingForDateMarker = false;
                    }
                    else if (DBPDFFormatInfo.isFuturesLine(tr))
                    {
                        FuturesInfo fut;
                        try
                        {
                            fut = FuturesInfo.Parse(comod, line, DBPDFFormatInfo);
                        }
                        catch (FormatException)
                        {

                            if (DBPDFFormatInfo.PreviousVersion != null)
                            {
                                DBPDFFormatInfo = DBPDFFormatInfo.PreviousVersion;
                                fut = FuturesInfo.Parse(comod, line, DBPDFFormatInfo);
                            }
                            else
                                throw;
                        }
                        if (parserListener != null)
                            parserListener.ProcessFut(fut);
                    }
                    else
                    {
                        // Либо заголовки, либо какой-то мусор, либо мы уже проехали фьючерс и читаем что-то между ним и опционами
                    }
                }
                else if (DBPDFFormatInfo.IsStartOptMarker(line))
                {
                    comod = DBPDFFormatInfo.FormatOptionComodName(line.Trim());
                    lookingForDateMarker = true;
                }
                else if (DBPDFFormatInfo.IsStartFutMarker(line))
                {
                    comod = DBPDFFormatInfo.FuturesNameOverride ?? line.Trim();
                    readingFut = true;
                }
                else if (lookingForDateMarker)
                {
                    lookingForDateMarker = false;
                    string mmm = tr.Substring(0, 3);
                    string yy = tr.Substring(3, 2);

                    if (Array.IndexOf(Utils.DBMonth, mmm) != -1 && char.IsDigit(yy[0]) && char.IsDigit(yy[1]))
                    {
                        contractDate = Utils.ParseContractDate(mmm, yy);
                        contractDateStr = mmm + yy;
                        var settIdx = tr.IndexOf("SETT.");
                        if (DBPDFFormatInfo.HasFuturesSettPrice && settIdx != -1)
                        {
                            futuresSettPrice =
                                decimal.Parse(tr.Substring(settIdx)
                                    .Split(new string[] { " ", "+", "-", ")" }, System.StringSplitOptions.RemoveEmptyEntries)[1],
                                System.Globalization.CultureInfo.InvariantCulture);
                        }
                        else
                            futuresSettPrice = 0;

                        readingStrikes = true;
                    }
                    else
                    {
                        // Это не признак конца DB, т.к. может быть лишь концом страницы,
                        // а после некоторого кол-ва хлама (колонтитулов) будет продолжение
                        if (parserListener != null)
                            parserListener.ProcessStop(line);
                    }
                }
                else
                {
                    ;
                }
            }

            return true;
        }

        private void ParsePdfTotal(string line, out int pdf_total_vol, out int pdf_total_oi, out int pdf_total_oi_ch)
        {
            var tfields = line.Trim().Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            if (DBPDFFormatInfo is GOLD_DBPDFFormatInfo)
            {
                // TODO: размер шрифта у TOTAL и страйков отличается, а у металлов кол-во цифр в строке постоянно меняется, так что не извлечь корректно никак
                pdf_total_vol = 0; // int.Parse("0" + line.Substring(127, 8).Trim());
                pdf_total_oi = 0;// int.Parse("0" + line.Substring(143, 8).Trim());
                pdf_total_oi_ch = 0;// int.Parse(line.Substring(151).Replace(" ", ""));
            }
            else if (DBPDFFormatInfo is ENERGY_DBPDFFormatInfo)
            {
                // TODO: размер шрифта у TOTAL и страйков отличается, а у энергий кол-во цифр в строке постоянно меняется, так что не извлечь корректно никак
                pdf_total_vol = 0; // int.Parse("0" + line.Substring(127, 8).Trim());
                pdf_total_oi = 0; // int.Parse(tfields[1]);
                pdf_total_oi_ch = 0;// int.Parse(line.Substring(151).Replace(" ", ""));
            }
            else if (DBPDFFormatInfo is EuroDollarCall_DBPDFFormatInfo || DBPDFFormatInfo is EuroDollarPut_DBPDFFormatInfo)
            {
                // TODO: размер шрифта у TOTAL и страйков отличается, а у энергий кол-во цифр в строке постоянно меняется, так что не извлечь корректно никак
                pdf_total_vol = 0; // int.Parse("0" + line.Substring(127, 8).Trim());
                pdf_total_oi = 0; // int.Parse(tfields[1]);
                pdf_total_oi_ch = 0;// int.Parse(line.Substring(151).Replace(" ", ""));
            }
            else if (tfields.Length == 5)
            {
                pdf_total_vol = int.Parse(tfields[1]);
                pdf_total_oi = int.Parse(tfields[2]);
                pdf_total_oi_ch = int.Parse(tfields[3] + tfields[4]);
            }
            else if (tfields.Length == 3)
            {
                pdf_total_vol = int.Parse(tfields[1]);
                pdf_total_oi = int.Parse(tfields[2]);
                pdf_total_oi_ch = 0;
            }
            else if (tfields.Length == 4)
            {//CHF
                pdf_total_vol = int.Parse(tfields[1]);
                pdf_total_oi = int.Parse(tfields[2].TrimEnd('+','-'));
                pdf_total_oi_ch = int.Parse((tfields[2].EndsWith("-") ? "-" : "") + tfields[3]);
            }
            else
            {
                throw new Exception();
            }
        }

        private void ParseFutTotal(string line, string comod)
        {
            var tfields = DBPDFFormatInfo.SplitFut(line, comod);

            if (parserListener != null)
                if (DBPDFFormatInfo is GOLDFUT_DBPDFFormatInfo)
                {
                    parserListener.ProcessFutTotal
                    (
                        0,
                        int.Parse(tfields[9]),
                        int.Parse(tfields[11]),
                        tfields.Count <= 12 ? 0 : int.Parse(tfields[12].Replace(" ", ""))
                    );
                }
                else
                {
                    parserListener.ProcessFutTotal
                    (
                        int.Parse(tfields[8]),
                        int.Parse(tfields[9]),
                        int.Parse(tfields[10]),
                        tfields.Count <= 11 ? 0 : int.Parse(tfields[11].Replace(" ", ""))
                    );
                }
        }
    }


    public interface ICMEDBParserListener
    {
        void ProcessStrike(Strike strike);

        void ProcessSectionInfo(int bulletinSectionCode, string bulletinName, int bulletinNumber, DateTime bulletinDate, bool isFinal);

        void Error(Exception ex, string line);

        void ProcessTotal(int total_vol, int total_oi, int total_oi_ch, string contractName, DateTime contractDate, decimal futuresSettPrice);

        void ProcessStop(string line);

        void ProcessFut(FuturesInfo fut);

        void ProcessFutTotal(int rth_vol, int globex_vol, int oi, int oi_ch);

        string FillMask(string fileOut);
    }

    public class StrikeParser
    {
        static System.Globalization.CultureInfo ic = System.Globalization.CultureInfo.InvariantCulture;

        public string NameText { get; set; }
        public string StrikeValueText { get; set; }
        public string OpenRangeText { get; set; }
        public string HighText { get; set; }
        public string LowText { get; set; }
        public string ClosingRangeText { get; set; }
        public string SettPriceText { get; set; }
        public string PtChgeText { get; set; }
        public string DeltaText { get; set; }
        public string ExercisesText { get; set; }
        public string VolumeTradesClearedText { get; set; }
        public string PntOrRthVolumeText { get; set; }
        public string OpenInterestText { get; set; }
        public string OpenInterestChangeText { get; set; }
        public string ContractHightText { get; set; }
        public string ContractLowText { get; set; }

        static System.Text.RegularExpressions.Regex highOrLowIsZero = new System.Text.RegularExpressions
            .Regex(@"^(-{4,4}|CAB|.*\..*\..*)$", System.Text.RegularExpressions.RegexOptions.Compiled); // ---- или CAB или содержит более одной точки (встречается в mini S&P за 2014)

        private Strike Parse()
        {
            Strike res = new Strike();

            res.StrikeValue = int.Parse(this.StrikeValueText);

            if (this.OpenRangeText == "----")
            {
                res.OpenRangeStart = 0;
                res.OpenRangeEnd = 0;
            }
            else
            {
                var or = this.OpenRangeText.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                res.OpenRangeStart = decimal.Parse(or[0].TrimEnd('$'), ic);
                res.OpenRangeEnd = or.Length > 1 ? decimal.Parse(or[1].TrimEnd('A', 'B', '$'), ic) : 0;
            }

            res.High = highOrLowIsZero.IsMatch(this.HighText) ? 0 : decimal.Parse(this.HighText.TrimStart('#').TrimEnd('A', 'B'), ic);

            res.Low = highOrLowIsZero.IsMatch(this.LowText) ? 0 : decimal.Parse(this.LowText.TrimStart('*').TrimEnd('A', 'B'), ic);

            if (this.ClosingRangeText == "----")
            {
                res.ClosingRangeStart = 0.01M; // TODO: ClosingRangeStart = 0.01M По просьбе Стаса, потом вынести это в экспортёр, а здесь сделать 0
                res.ClosingRangeEnd = 0;
            }
            else if (this.ClosingRangeText == "CAB")
            {
                res.ClosingRangeStart = 0;
                res.ClosingRangeEnd = 0;
                res.SettPriceCab = true;
            }
            else
            {
                var cr = this.ClosingRangeText.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                res.ClosingRangeStart = decimal.Parse(cr[0].TrimEnd('N', 'A', 'B'), ic);
                res.ClosingRangeEnd = cr.Length > 1 ? decimal.Parse(cr[0].TrimEnd('N', 'A', 'B'), ic) : 0;
            }

            if (this.SettPriceText == "CAB")
            {
                res.SettPrice = 0.01m;
                res.SettPriceCab = true;
            }
            else if (this.SettPriceText.StartsWith("----")) // ----- или ----+ это ---- и ещё - или + на конце от следующей колонки
            {
                res.SettPrice = 0;
            }
            else
            {
                res.SettPrice = decimal.Parse(this.SettPriceText.TrimEnd('P', '+', '-'), ic);
            }

            if (this.PtChgeText == "----")
                res.PtChge = 0;
            else if (this.PtChgeText.Contains("NEW"))
                res.PtChge = 0;
            else if (this.PtChgeText == "UNCH")
                res.PtChge = 0;
            else //если в предыдущем поле будет ----, то тут будет косяк!!!!
            {
                bool neg = this.SettPriceText == "-----" || (!this.SettPriceText.StartsWith("----") && this.SettPriceText.EndsWith("-"));
                res.PtChge = decimal.Parse((neg ? "-" : "") + this.PtChgeText.Trim(), ic);
            }

            res.Delta = this.DeltaText == "----" ? 0.001M : decimal.Parse(this.DeltaText, ic);

            res.Exercises = this.ExercisesText == "----" ? 0 : int.Parse(this.ExercisesText, ic);

            res.VolumeTradesCleared = this.VolumeTradesClearedText == "----" ? 0 : int.Parse(this.VolumeTradesClearedText, ic);
            res.PntVolume = (this.PntOrRthVolumeText == null || this.PntOrRthVolumeText == "----") ? 0 : int.Parse(this.PntOrRthVolumeText, ic);

            res.OpenInterest = this.OpenInterestText == "----" ? 0 : int.Parse(this.OpenInterestText.TrimEnd('+', '-'));

            if (this.OpenInterestChangeText == "UNCH")
                res.OpenInterestChange = 0;
            else if (this.OpenInterestText.EndsWith("-")) // Когда появляется "----", то ОИ тоже уменьшается, если не был и так на нуле, а отдельного минуса перед change не стоит
                res.OpenInterestChange = int.Parse("-" + this.OpenInterestChangeText);
            else
                res.OpenInterestChange = int.Parse(this.OpenInterestChangeText);

            res.ContractHight = this.ContractHightText == "----" ? 0M : (decimal.Parse(this.ContractHightText.TrimEnd('B', '$'), ic));

            res.ContractLow = this.ContractLowText == "----" ? 0M : (decimal.Parse(this.ContractLowText.TrimEnd('A', 'B', '$'), ic));

            return res;
        }

        public static Strike ParseCurrency(string comod, DateTime contractDate, string line, DBPDFFormatInfo formatInfo)
        {
            var res = new StrikeParser();
            var fields = formatInfo.SplitStrike(line);

            for (int i = 0; i < fields.Count; i++)
            {
                var ss = fields[i];
                switch (i)
                {
                    case 0: res.StrikeValueText = ss; break;
                    case 1: res.OpenRangeText = ss; break;
                    case 2: res.HighText = ss; break;
                    case 3: res.LowText = ss; break;
                    case 4: res.ClosingRangeText = ss; break;
                    case 5: res.SettPriceText = ss; break;
                    case 6: res.PtChgeText = ss; break;
                    case 7: res.DeltaText = ss; break;
                    case 8: res.ExercisesText = ss; break;
                    case 9: res.VolumeTradesClearedText = ss; break;
                    case 10: res.OpenInterestText = ss; break;
                    case 11: res.OpenInterestChangeText = ss; break;
                    case 12: res.ContractHightText = ss; break;
                    case 13: res.ContractLowText = ss; break;
                    default:
                        throw new Exception();
                }
            }

            var s = res.Parse();
            s.Name = comod;
            s.ContractDate = contractDate;
            return s;
        }

        public static Strike ParseEuroDollarCall(string comod, DateTime contractDate, string line, DBPDFFormatInfo formatInfo)
        {
            var res = new StrikeParser();
            var fields = formatInfo.SplitStrike(line);

            for (int i = 0; i < fields.Count; i++)
            {
                var ss = fields[i];
                switch (i)
                {
                    case 0: res.StrikeValueText = ss; break;
                    case 1: res.OpenRangeText = ss; break;
                    case 2: res.HighText = ss; break;
                    case 3: res.LowText = ss; break;
                    case 4: res.ClosingRangeText = ss; break;
                    case 5: res.SettPriceText = ss; break;
                    case 6: res.PtChgeText = ss; break;
                    case 7: res.DeltaText = ss; break;
                    case 8: res.ExercisesText = ss; break;
                    case 9: res.PntOrRthVolumeText = ss; break;
                    case 10: res.VolumeTradesClearedText = ss; break;
                    case 11: res.OpenInterestText = ss; break;
                    case 12: res.OpenInterestChangeText = ss; break;
                    case 13: res.ContractHightText = ss; break;
                    case 14: res.ContractLowText = ss; break;
                    default:
                        throw new Exception();
                }
            }

            var s = res.Parse();
            s.Name = comod;
            s.ContractDate = contractDate;
            return s;
        }

        public static Strike ParseCommodity(string comod, DateTime contractDate, string line, DBPDFFormatInfo formatInfo)
        {
            var res = new StrikeParser();
            var fields = formatInfo.SplitStrike(line);

            res.ContractHightText = "----";
            res.ContractLowText = "----";

            for (int i = 0; i < fields.Count; i++)
            {
                var ss = fields[i];
                switch (i)
                {
                    case 0: res.StrikeValueText = ss; break;
                    case 1: break;
                    case 2: res.OpenRangeText = ss; break;
                    case 3: break;
                    case 4:
                        int markIndex = ss.IndexOf('/');
                        if (markIndex >= 0)
                        {
                            res.HighText = ss.Substring(0, markIndex);
                            res.LowText = ss.Substring(markIndex + 1);
                        }
                        else
                        {
                            res.HighText = "----";
                            res.LowText = "----";
                        }
                        break;
                    case 5: break;
                    case 6: res.ClosingRangeText = ss; break;
                    case 7: res.SettPriceText = ss; break;
                    case 8: res.PtChgeText = ss; break;
                    case 9: res.DeltaText = ss; break;
                    case 10: res.ExercisesText = ss; break;
                    case 11: break;
                    case 12: res.VolumeTradesClearedText = ss; break;
                    case 13: res.PntOrRthVolumeText = ss; break; 
                    case 14: res.OpenInterestText = ss; break;
                    case 15: res.OpenInterestChangeText = ss; break;
                    default:
                        throw new Exception();
                }
            }

            var s = res.Parse();
            s.Name = comod;
            s.ContractDate = contractDate;
            return s;
        }
    }

    public class Strike
    {
        public string Name {get; set;}
        public DateTime ContractDate {get; set;}
        public int StrikeValue {get; set;}
        public decimal OpenRangeStart {get; set;}
        public decimal OpenRangeEnd { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal ClosingRangeStart { get; set; }
        public decimal ClosingRangeEnd { get; set; }
        public decimal SettPrice {get; set;}
        public bool SettPriceCab { get; set; }
        public decimal PtChge {get; set;}
        public decimal Delta { get; set; }
        public int Exercises {get;set;}
        
        public int VolumeTradesCleared {get;set;}
        /// <summary>
        /// Pnt (Energy, Metals) or Rth (for Euro Dollar) volume
        /// </summary>
        public int PntVolume { get; set; }
        public int OpenInterest {get;set;}
        public int OpenInterestChange {get;set;}
        public decimal ContractHight { get; set; }
        public decimal ContractLow { get; set; }

        const string csvString = "\"{0}\"";
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append(string.Format(csvString + ";", Name));
            sb.Append(string.Format(csvString + ";", ContractDate.Month));
            sb.Append(string.Format(csvString + ";", ContractDate.Year));
            sb.Append(StrikeValue+";");
            sb.Append(OpenRangeStart + ";");
            sb.Append(OpenRangeEnd + ";");
            sb.Append(High + ";");
            sb.Append(Low + ";");
            sb.Append(ClosingRangeStart + ";");
            sb.Append(ClosingRangeEnd + ";");
            if (SettPriceCab)
                sb.Append("CAB;");
            else
                sb.Append(SettPrice + ";");
            sb.Append(PtChge+";");
            sb.Append(Delta + ";");
            sb.Append(Exercises + ";");
            sb.Append(VolumeTradesCleared + ";");
            sb.Append(OpenInterest + ";");
            sb.Append(OpenInterestChange + ";");
            sb.Append(ContractHight + ";");
            sb.Append(ContractLow + ";");
            return sb.ToString();
        }

        public void WriteXML(XmlWriter xw)
        {
            xw.WriteStartElement("Strike");

            xw.WriteAttributeString("ContractHight", XmlConvert.ToString(ContractHight));
            xw.WriteAttributeString("ContractLow", XmlConvert.ToString(ContractLow));

            xw.WriteAttributeString("StrikeValue", XmlConvert.ToString(StrikeValue));
            xw.WriteAttributeString("SettPrice", XmlConvert.ToString(SettPrice));
            xw.WriteAttributeString("Delta", XmlConvert.ToString(Delta));
            xw.WriteAttributeString("VolumeTradesCleared", XmlConvert.ToString(VolumeTradesCleared));
            xw.WriteAttributeString("PntVolume", XmlConvert.ToString(PntVolume));
            xw.WriteAttributeString("OpenInterest", XmlConvert.ToString(OpenInterest));
            xw.WriteAttributeString("OpenInterestChange", XmlConvert.ToString(OpenInterestChange));
            xw.WriteEndElement();
        }

        public static Strike ReadXML(XmlReader xr, string name, DateTime contractDate)
        {
            Strike s = new Strike();
            s.Name = name;
            s.ContractDate = contractDate;

            s.ContractHight = XmlConvert.ToDecimal(xr.GetAttribute("ContractHight"));
            s.ContractLow = XmlConvert.ToDecimal(xr.GetAttribute("ContractLow"));

            s.StrikeValue = XmlConvert.ToInt32(xr.GetAttribute("StrikeValue"));
            s.SettPrice = XmlConvert.ToDecimal(xr.GetAttribute("SettPrice"));
            s.Delta = XmlConvert.ToDecimal(xr.GetAttribute("Delta"));
            s.VolumeTradesCleared = XmlConvert.ToInt32(xr.GetAttribute("VolumeTradesCleared"));
            s.PntVolume = XmlConvert.ToInt32(xr.GetAttribute("PntVolume"));
            s.OpenInterest = XmlConvert.ToInt32(xr.GetAttribute("OpenInterest"));
            s.OpenInterestChange = XmlConvert.ToInt32(xr.GetAttribute("OpenInterestChange"));

            return s;
        }

        internal static Strike Parse(string comod, DateTime contractDate, string line, DBPDFFormatInfo formatInfo)
        {
            if (formatInfo is GOLD_DBPDFFormatInfo || formatInfo is ENERGY_DBPDFFormatInfo)
                return StrikeParser.ParseCommodity(comod, contractDate, line, formatInfo);
            else if (formatInfo is EuroDollarCall_DBPDFFormatInfo)
                return StrikeParser.ParseEuroDollarCall(comod, contractDate, line, formatInfo);
            else
                return StrikeParser.ParseCurrency(comod, contractDate, line, formatInfo);
        }
    }

    public class FuturesInfo
    {
        static System.Globalization.CultureInfo ic = System.Globalization.CultureInfo.InvariantCulture;

        public string Name { get; set; }
        public DateTime Date;
        public decimal OpenRangeStart { get; set; }
        public decimal OpenRangeEnd { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal ClosingRangeStart { get; set; }
        public decimal ClosingRangeEnd { get; set; }
        public decimal SettPrice { get; set; }
        public bool SettPriceCab { get; set; }
        public decimal Reciprocal { get; set; }
        public decimal PtChge { get; set; }
        public int RthVolume { get; set; }
        public int GlobexVolume { get; set; }
        public int OpenInterest { get; set; }
        public int OpenInterestChange { get; set; }
        public decimal ContractHight { get; set; }
        public decimal ContractLow { get; set; }

        public string MonthText { get; set; }
        public string YearText { get; set; }
        public string OpenRangeText { get; set; }
        public string HighText { get; set; }
        public string LowText { get; set; }
        public string ClosingRangeText { get; set; }
        public string SettPriceText { get; set; }
        public string ReciprocalText { get; set; }
        public string PtChgeText { get; set; }
        public string RthVolumeText { get; set; }
        public string GlobexVolumeText { get; set; }
        public string OpenInterestText { get; set; }
        public string OpenInterestChangeText { get; set; }
        public string ContractHightText { get; set; }
        public string ContractLowText { get; set; }

        const string csvString = "\"{0}\"";
        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.Append(string.Format(csvString + ";", Name));
            sb.Append(string.Format(csvString + ";", Date.Month));
            sb.Append(string.Format(csvString + ";", Date.Year));
            sb.Append(string.Format(csvString + ";", ""));
            sb.Append(string.Format(csvString + ";", OpenRangeStart));
            sb.Append(string.Format(csvString + ";", OpenRangeEnd));
            sb.Append(string.Format(csvString + ";", High));
            sb.Append(string.Format(csvString + ";", Low));
            sb.Append(string.Format(csvString + ";", ClosingRangeStart));
            sb.Append(string.Format(csvString + ";", ClosingRangeEnd));
            
            if (SettPriceCab)
                sb.Append("CAB");
            else
                sb.Append(string.Format(csvString + ";", SettPrice));
            sb.Append(string.Format(csvString + ";", PtChge));
            sb.Append(string.Format(csvString + ";", Reciprocal));
            
            sb.Append(string.Format(csvString + ";", RthVolume));
            sb.Append(string.Format(csvString + ";", GlobexVolume));

            sb.Append(string.Format(csvString + ";", OpenInterest));
            sb.Append(string.Format(csvString + ";", OpenInterestChange));
            sb.Append(string.Format(csvString + ";", ContractHight));
            sb.Append(string.Format(csvString + ";", ContractLow));

            return sb.ToString();
        }

        //internal static FuturesInfo Parse(string comod, string line, DBPDFFormatInfo formatInfo)
        //{
        //    FuturesInfo res;
        //    if (formatInfo.PreviousVersion != null)
        //    {
        //        try
        //        {
        //            res = ParseInternal(comod, line, formatInfo);
        //        }
        //        catch (FormatException)
        //        {
        //            res = ParseInternal(comod, line, formatInfo.PreviousVersion);
        //        }
        //    }
        //    else
        //    {
        //        res = ParseInternal(comod, line, formatInfo);
        //    }

        //    return res;
        //}

        public static FuturesInfo Parse(string comod, string line, DBPDFFormatInfo formatInfo)
        {
            var res = new FuturesInfo();
            var fields = formatInfo.SplitFut(line, comod);

            for (int i = 0; i < fields.Count; i++)
            {
                var ss = fields[i];

                if (formatInfo is GOLDFUT_DBPDFFormatInfo)
                {
                    switch (i)
                    {
                        case 0: res.MonthText = ss.Substring(0, 3); res.YearText = ss.Substring(3, 2); break;
                        case 1: res.OpenRangeText = "----"; break;
                        case 2: res.HighText = "----"; break;
                        case 3: res.LowText = "----"; break;
                        case 4: res.ClosingRangeText = "----"; break;
                        case 5: res.RthVolumeText = "----"; res.ReciprocalText = "(0)"; res.ContractHightText = "----"; res.ContractLowText = "----"; break;
                        case 6: res.SettPriceText = ss; break;
                        case 7: res.PtChgeText = ss; break;
                        case 8: break;
                        case 9: res.GlobexVolumeText = ss; break;
                        case 10: break;
                        case 11: res.OpenInterestText = ss; break;
                        case 12: res.OpenInterestChangeText = ss; break;
                        default:
                            throw new Exception();
                    }
                }
                else
                {
                    switch (i)
                    {
                        case 0: res.MonthText = ss.Substring(0, 3); res.YearText = ss.Substring(3, 2); break;
                        case 1: res.OpenRangeText = ss; break;
                        case 2: res.HighText = ss; break;
                        case 3: res.LowText = ss; break;
                        case 4: res.ClosingRangeText = ss; break;
                        case 5: res.SettPriceText = ss; break;
                        case 6: res.ReciprocalText = ss; break;
                        case 7: res.PtChgeText = ss; break;
                        case 8: res.RthVolumeText = ss; break;
                        case 9: res.GlobexVolumeText = ss; break;
                        case 10: res.OpenInterestText = ss; break;
                        case 11: res.OpenInterestChangeText = ss; break;
                        case 12: res.ContractHightText = ss; break;
                        case 13: res.ContractLowText = ss; break;
                        default:
                            throw new Exception();
                    }
                }
            }

            // Название у фьючерса на металлы написано более крупным шрифтом из-за этого лишние пробелы
            res.Name = formatInfo is GOLDFUT_DBPDFFormatInfo ?
                comod.Replace("GC  FUT             COM EX GOLD  FUTUR ES", "COMEX GOLD FUTURES")
                .Replace("SI  FUT             COM EX SILV ER FUT URES", "COMEX SILVER FUTURES") : comod;

            res.ParseTextFields();
            return res;
        }

        public void WriteXML(XmlWriter xw)
        {
            xw.WriteStartElement("Futures");
            xw.WriteAttributeString("Name", Name);
            xw.WriteAttributeString("MonthText", MonthText);
            xw.WriteAttributeString("YearText", YearText);
            xw.WriteAttributeString("OpenRangeText", OpenRangeText);
            xw.WriteAttributeString("HighText", HighText);
            xw.WriteAttributeString("LowText", LowText);
            xw.WriteAttributeString("ClosingRangeText", ClosingRangeText);
            xw.WriteAttributeString("SettPriceText", SettPriceText);
            xw.WriteAttributeString("ReciprocalText", ReciprocalText);
            xw.WriteAttributeString("PtChgeText", PtChgeText);
            xw.WriteAttributeString("RthVolumeText", RthVolumeText);
            xw.WriteAttributeString("GlobexVolumeText", GlobexVolumeText);
            xw.WriteAttributeString("OpenInterestText", OpenInterestText);
            xw.WriteAttributeString("OpenInterestChangeText", OpenInterestChangeText);
            xw.WriteAttributeString("ContractHightText", ContractHightText);
            xw.WriteAttributeString("ContractLowText", ContractLowText);
            xw.WriteEndElement();
        }

        public static FuturesInfo ReadXML(XmlReader xr)
        {
            FuturesInfo s = new FuturesInfo();
            s.Name = xr.GetAttribute("Name");
            s.MonthText = xr.GetAttribute("MonthText");
            s.YearText = xr.GetAttribute("YearText");
            s.OpenRangeText = xr.GetAttribute("OpenRangeText");
            s.HighText = xr.GetAttribute("HighText");
            s.LowText = xr.GetAttribute("LowText");
            s.ClosingRangeText = xr.GetAttribute("ClosingRangeText");
            s.SettPriceText = xr.GetAttribute("SettPriceText");
            s.ReciprocalText = xr.GetAttribute("ReciprocalText");
            s.PtChgeText = xr.GetAttribute("PtChgeText");
            s.RthVolumeText = xr.GetAttribute("RthVolumeText");
            s.GlobexVolumeText = xr.GetAttribute("GlobexVolumeText");
            s.OpenInterestText = xr.GetAttribute("OpenInterestText");
            s.OpenInterestChangeText = xr.GetAttribute("OpenInterestChangeText");
            s.ContractHightText = xr.GetAttribute("ContractHightText");
            s.ContractLowText = xr.GetAttribute("ContractLowText");
            s.ParseTextFields();
            return s;
        }

        private void ParseTextFields()
        {
            this.Date = Utils.ParseContractDate(this.MonthText, this.YearText);

            if (this.OpenRangeText == "----")
            {
                this.OpenRangeStart = 0;
                this.OpenRangeEnd = 0;
            }
            else
            {
                var or = this.OpenRangeText.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                this.OpenRangeStart = decimal.Parse(or[0], ic);
                this.OpenRangeEnd = or.Length > 1 ? decimal.Parse(or[1], ic) : 0;
            }

            this.High = this.HighText == "----" ? 0 : decimal.Parse(this.HighText.TrimStart('#').TrimEnd('A', 'B'), ic);

            this.Low = this.LowText == "----" ? 0 : decimal.Parse(this.LowText.TrimStart('*').TrimEnd('A', 'B'), ic);

            if (this.ClosingRangeText == "----" || this.ClosingRangeText == "")
            {
                this.ClosingRangeStart = 0M;
                this.ClosingRangeEnd = 0M;
            }
            else
            {
                var cr = this.ClosingRangeText.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                this.ClosingRangeStart = decimal.Parse(cr[0].TrimEnd('N', 'A', 'B'), ic);
                this.ClosingRangeEnd = cr.Length > 1 ? decimal.Parse(cr[0].TrimEnd('N', 'A', 'B'), ic) : 0;
            }

            this.SettPrice = this.SettPriceText == "----" ? 0 : decimal.Parse(this.SettPriceText.TrimEnd('P'), ic);


            this.Reciprocal = this.ReciprocalText == "" ? 0m : decimal.Parse(this.ReciprocalText.TrimStart('(').TrimEnd(')'), ic);

            this.PtChge = (this.PtChgeText.EndsWith("NEW") || this.PtChgeText == "UNCH") ? 0 : decimal.Parse(this.PtChgeText.Replace(" ", ""), ic);

            this.RthVolume = this.RthVolumeText == "----" ? 0 : int.Parse(this.RthVolumeText);

            this.GlobexVolume = this.GlobexVolumeText == "----" ? 0 : int.Parse(this.GlobexVolumeText);

            this.OpenInterest = string.IsNullOrEmpty(this.OpenInterestText) ? 0 : int.Parse(this.OpenInterestText);

            this.OpenInterestChange = this.OpenInterestChangeText == "UNCH" ? 0 : int.Parse(this.OpenInterestChangeText.Replace(" ", ""), ic);

            this.ContractHight = this.ContractHightText == "----" ? 0 : decimal.Parse(this.ContractHightText.TrimEnd('B'), ic);

            this.ContractLow = this.ContractLowText == "----" ? 0 : decimal.Parse(this.ContractLowText.TrimEnd('A', 'B'), ic);
        }
    }
}
