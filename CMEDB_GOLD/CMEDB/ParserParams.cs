﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл содержит настройки конвертера, специфичные для различных секций и версий отчёта
// Если вы пытаетесь настроить конвертер для поддержки новой секции или секции формат которой изменился, начать следует здесь
// Обратите внимание на необходимость сохранения совместимости с предыдущими форматами секции, если вы меняете что-то для отчёта за 2115 год,
// убедитесь что при этом конвертер не перестанет понимать отчёты за 2008 год.

using System;
using System.Collections.Generic;
using System.Text;

namespace CMEDB
{
    public abstract class DBPDFFormatInfo
    {
        public abstract int SectionCode { get; }
        public abstract bool IsStartOptMarker(string line);
        public virtual bool IsStartFutMarker(string line) { return false; }

        protected abstract bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo);
        public abstract float CharSpacing { get; }

        public virtual int Version { get { return 1; } }

        public bool TryParsePageHeader(string line, string lastLine, out PdfPageHeadingInfo headerInfo)
        {
            PdfPageHeadingInfo hi;
            var res = this.TryParsePageHeader(line, out hi);

            if (!res)
            {
                headerInfo = null;
                return false;
            }
            if (lastLine.Contains("PRELIMINARY"))
                hi.IsFinal = false;
            else if (lastLine.Contains("FINAL"))
                hi.IsFinal = true;
            else
                throw new Exception();
            headerInfo = hi;
            return res;
        }

        int[] splitPositionsOpt;
        int[] splitPositionsFut;
        int[] splitPositionsFut2; // EC/BP CROSS RT

        protected abstract string PositionOptMarkers { get; }

        protected virtual string PositionFutMarkers { get { return null; } }

        public int[] SplitPositionsOpt
        {
            get
            {
                if (splitPositionsOpt == null)
                    this.splitPositionsOpt = CalcPositions(PositionOptMarkers);
                return splitPositionsOpt;
            }
        }

        int[] SplitPositionsFut
        {
            get
            {
                if (splitPositionsFut == null)
                    this.splitPositionsFut = CalcPositions(PositionFutMarkers);
                return splitPositionsFut;
            }
        }

        int[] SplitPositionsFut2
        {
            get
            {
                if (splitPositionsFut2 == null && this is BRITPNDCALL_DBPDFFormatInfo)
                    this.splitPositionsFut2 = CalcPositions((this as BRITPNDCALL_DBPDFFormatInfo).PositionFutMarkers2);
                return splitPositionsFut2;
            }
        }

        private int[] CalcPositions(string markers)
        {
            var pos = new List<int>();
            for (int i = 0; i < markers.Length; i++)
            {
                if (markers[i] == '|')
                    pos.Add(i);
            }
            return pos.ToArray();
        }

        public abstract string Name { get; }

        public override string ToString()
        {
            return Name;
        }

        public List<string> SplitFut(string line, string comod)
        {
            var fields = SplitLine(line, comod != "EC/BP CROSS RT" ?  this.SplitPositionsFut : this.SplitPositionsFut2);
            return fields;
        }

        public bool isFuturesLine(string tr)
        {
            if (tr.Length < 5)
                return false;
            var month = tr.Substring(0, 3);
            var year = tr.Substring(3, 2);
            DateTime cd;
            // MAR15 ---- ----  ----  1.5685N (.6397) - 3 2767 +  53 15 1.7130B 1.5576
            // если начинается с даты и содержит скобки, наверное это фьючерс
            var startsWithDate = Utils.TryParseContractDate(month, year, out cd);
            if (startsWithDate && this is EMINISnPCALL_DBPDFFormatInfo)
                return true;
            else
                return Utils.TryParseContractDate(month, year, out cd) && tr.Contains("(") && tr.Contains(")");
        }

        public List<string> SplitStrike(string line)
        {
            var fields = SplitLine(line, this.SplitPositionsOpt);
            return fields;
        }

        private static List<string> SplitLine(string line, int[] splitPositions)
        {
            var fields = new List<string>();
            for (int i = 0; i < splitPositions.Length; i++)
            {
                var fromPos = splitPositions[i];
                var length = (i == splitPositions.Length - 1 ? line.Length : splitPositions[i + 1]) - fromPos;
                if (fromPos + length > line.Length)
                {
                    if (fromPos < line.Length)
                        fields.Add(line.Substring(fromPos));
                    break;
                }
                var ss = line.Substring(fromPos, length).Trim();
                fields.Add(ss);
            }
            return fields;
        }

        public bool IsStrike(string line)
        {
            var strike = line.Substring(SplitPositionsOpt[0], 4);
            int val;
            return int.TryParse(strike, out val) && val > 0;
        }

        public virtual bool HasFuturesSettPrice { get { return true; } }

        public virtual string FormatOptionComodName(string comod)
        {
            return System.Text.RegularExpressions.Regex.Replace(comod, " {2,}", " ");
        }

        protected bool formatChange2015_07_07(PdfPageHeadingInfo headerInfo)
        {
            return headerInfo.bulletinDate >= new DateTime(2015, 07, 07);
        }

        protected bool formatChange2014_11_07(PdfPageHeadingInfo headerInfo)
        {
            return (headerInfo.bulletinDate >= new DateTime(2014, 11, 07)
                    || headerInfo.bulletinDate == new DateTime(2014, 11, 4) /* похоже здесь они эксперимент провели */);
        }

        public virtual string FuturesNameOverride {
            get
            {
                return null;
            }
        }

        public virtual DBPDFFormatInfo PreviousVersion
        {
            get
            {
                return null;
            }
        }
    }

    public class PdfPageHeadingInfo
    {
        public int bulletinSectionCode;
        public int bulletinNumber;
        public string bulletinName;
        public DateTime bulletinDate;
        public bool IsFinal;

        //PG27BULLETIN#47@BRITISHPOUNDCALLOPTIONSFri,Mar09,2012PG27
        public PdfPageHeadingInfo (int bulletinSectionCode, string bulletinNumber, string bulletinName, DateTime bulletinDate)
	    {
            this.bulletinSectionCode = bulletinSectionCode;
            this.bulletinNumber = bulletinNumber == "" ? 0 : int.Parse(bulletinNumber);
            this.bulletinName = bulletinName;
            this.bulletinDate = bulletinDate;
        }

        public override bool Equals(object obj)
        {
            var other = (PdfPageHeadingInfo)obj;
            return
                this.bulletinSectionCode == other.bulletinSectionCode &&
                this.bulletinNumber == other.bulletinNumber &&
                this.bulletinName == other.bulletinName &&
                this.bulletinDate == other.bulletinDate;
        }

        public override int GetHashCode()
        {
            return bulletinDate.GetHashCode();
        }
    }

    class BRITPNDPUT_DBPDFFormatInfo_v2 : BRITPNDPUT_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class BRITPNDCALL_DBPDFFormatInfo_v3 : BRITPNDCALL_DBPDFFormatInfo_v2
    {
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 8.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    SEP15               1.5598                1.5601         1.5405          1.5436   (    .6478) -    160       187   111551   168237 -   3431  1.7059B  1.4554A  
                    |    SEP15               0.7093                #.7150B       .7070             .7117   (   1.4051) +   265       ----    1843     21958 -      94 .7476B   .6999A  // EC/BP CROSS RT  */
                    "    |                  |                     |           |                 ||         |           |          |       |       |        |         |        |";
            }
        }

        public override string PositionFutMarkers2
        {
            get
            {
                return PositionFutMarkers;
            }
        }

        BRITPNDCALL_DBPDFFormatInfo_v2 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new BRITPNDCALL_DBPDFFormatInfo_v2();
                return previousVersion;
            }
        }
    }

    class BRITPNDCALL_DBPDFFormatInfo_v2 : BRITPNDCALL_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    1.5852                1.5852     *1.5852       1.5852               1.5860  (    .6305) +     27    127   105076   156380 +   7592  1.7165   1.4838A */
                    "    |        |                    |           |          |                    |          |           |       |       |       |        |        |         |";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class BRITPNDCALL_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 27; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "BRITPNDCALL";
            //WKBP-3B-C
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKBP-[0-9][A-Z]-C$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("CALLOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return  3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1540      ----                    ----        ----        5.780N                 5.650-     13      .999    ----    ----         3     UNCH    11.100B  6.150B
                    |     1580      ----                    ----        ----        2.010N                 1.730-     28      .953    ----    ----         6     UNCH    8.900B   1.470B
                    |     1590      ----                    ----        ----        1.240N                  .870-     37      .780    ----    ----        63     UNCH    8.100B   .750B
                    |     1600      ----                    ----        ----        0.640N                  .280-     36      .324    ----     448       594 +    281    6.650B   .210B
                    |     1600      1.240                   1.240       *1.240      1.240                  1.380+     75     1.000    1204      41        ----   1232    4.730B   .180B
                    |     1580      ----                    ----        ----        0.080N             CAB          ----      ----    ----       2        ----    331    6.850    .010
                    |     1400      1.150                   #1.150      *1.150      1.150                  1.110+    NEW      .132    ----       2         2 +      2    1.460B   1.150B */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override string Name
        {
            get { return "Section27_British_Pound_Call_Options.pdf"; }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "BRITPNDFUT" || tr == "EC/BPCROSSRT";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    JUN12    ----                    ----        ----       1.5529N              1.5532   (   .6438) +      3    3515  121250    75254 -  26600   1.6526B 1.5222A
                    |    SEP08    1.9700 @1.9701        1.9702B     1.9665A      1.9679               1.9672   (   .5083) -     91    3078   76489    99658 -    647   2.0906B 1.9106A */
                    "    |        |                    |           |          |                    |           |          |       |       |       |        |        |         |";
            }
        }

        //"EC/BP CROSS RT"
        public virtual string PositionFutMarkers2
        {
            get
            {
                return
                    /*
                    |    JUN13    0.8421                 .8452B      .8411       ----                  .8427  (   1.1866) +    160    ----   2655     14241 +      30  .8804B  .8404A
                    |    MAR12    0.8342                 .8378B      .8331A      ----                  .8368  (   1.1950) +    395     606      600    9514 +    402   .8648B  .8230 */
                    "    |        |                    |           |          |                    |          |           |       |       |        |       |         |       |";
            }
        }
    }

    class BRITPNDPUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 28; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "BRITPNDPUT";
            //WKBP-3B-C
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKBP-[0-9][A-Z]-P$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("PUTOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1540      ----                    ----        ----        5.780N                 5.650-     13      .999    ----    ----         3     UNCH    11.100B  6.150B
                    |     1580      ----                    ----        ----        2.010N                 1.730-     28      .953    ----    ----         6     UNCH    8.900B   1.470B
                    |     1590      ----                    ----        ----        1.240N                  .870-     37      .780    ----    ----        63     UNCH    8.100B   .750B
                    |     1600      ----                    ----        ----        0.640N                  .280-     36      .324    ----     448       594 +    281    6.650B   .210B
                    |     1600      1.240                   1.240       *1.240      1.240                  1.380+     75     1.000    1204      41        ----   1232    4.730B   .180B
                    |     1580      ----                    ----        ----        0.080N             CAB          ----      ----    ----       2        ----    331    6.850    .010
                    |     1400      1.150                   #1.150      *1.150      1.150                  1.110+    NEW      .132    ----       2         2 +      2    1.460B   1.150B */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override string Name
        {
            get { return "Section28_British_Pound_Put_Options.pdf"; }
        }
    }

    class EMINISnPCALL_DBPDFFormatInfo_v2 : EMINISnPCALL_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }
        public override bool HasFuturesSettPrice { get { return false; } }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "EMINIS&PFUT";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    SEP15   2044.75               2074.75     2044.75      ----                   2069.00   +   2780         945    1572913    2622223 -  448282126.25    1795.75A
                    |    MAR16   2110.00               #2111.25B   2094.50      ----                   2101.40   -    750        ----         30       1062 +      22111.25B   1948.25A
                    |    JUN16   2050.75               2058.00B    *2017.25A    ----                   2055.60   +    880        ----         47        223 +     452105.50B   2017.25A
                    |    DEC15   2082.25               2091.25     2072.75      ----                   2081.80   +   UNCH        ----        659      18296 +    1052118.75B   1785.00A*/
                    "    |     |                     |          |           |                        |          ||        |          |          |          |        |        |";
            } // +   UNCH это что-то новенькое, раньше такого не встречал, поправить
            // TODO: а ещё OIL глюякает
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1810     259.50                   #259.50     259.50      ----                   258.50-     10     .982   ----      20        221    UNCH    259.50B  64.75A
                    |      800     ----                     ----        ----        ----                  1267.60+     10    1.000   ----    ----         75    UNCH    1073.25B 913.50A
                    |     1850     221.00                   #221.00     214.75A     ----                   218.80-     20     .976   ----      54      16036 -    47    221.00B  48.75A */
                    "     |        |                        |           |          |                   |          |      |        |      |       |            |     |           |";
            }
        }
    }

    class EMINISnPPUT_DBPDFFormatInfo_v2 : EMINISnPPUT_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }
        public override bool HasFuturesSettPrice { get { return false; } }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1810     259.50                   #259.50     259.50      ----                   258.50-     10     .982   ----      20        221    UNCH    259.50B  64.75A
                    |      800     ----                     ----        ----        ----                  1267.60+     10    1.000   ----    ----         75    UNCH    1073.25B 913.50A
                    |     1850     221.00                   #221.00     214.75A     ----                   218.80-     20     .976   ----      54      16036 -    47    221.00B  48.75A */
                    "     |        |                        |           |          |                   |          |      |        |      |       |            |     |           |";
            }
        }
    }

    class EMINISnPCALL_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 47; } }
        
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "EMINIS&PCALL";
        }

        public override bool IsStartFutMarker(string line)
        {
            return base.IsStartFutMarker(line);
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("E-MINIS&P500CALLOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1345      26.25                   44.25B      26.25       ----                    26.60-   1690     .726    ----      25      2029     UNCH   74.50B   17.50A
                    |     1350      26.00                   39.75B      22.50       ----                    22.80-   1620     .679    ----     249      4823 -    200   69.75B   11.25A
                    |     1000      ----                    ----        367.75A     ----                   365.00-   2090     .999    ----    ----        11     UNCH   414.50B  187.00A
                    |      250      992.50                  1.00003.7   992.50      ----                  1004.10+    NEW     .999    ----       1         1 +      1   1017.50B 969.50A*/
                    "     |         |                       |           |           |                  |          |      |        |       |       |           |      |           |";
            }
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        public override string Name
        {
            get { return "Section47_E_Mini_S_And_P_500_Call_Options.pdf"; }
        }
    }

    class EMINISnPPUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 48; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "EMINIS&PPUT";
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("E-MINIS&P500PUTOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1345      26.25                   44.25B      26.25       ----                    26.60-   1690     .726    ----      25      2029     UNCH   74.50B   17.50A
                    |     1350      26.00                   39.75B      22.50       ----                    22.80-   1620     .679    ----     249      4823 -    200   69.75B   11.25A
                    |     1000      ----                    ----        367.75A     ----                   365.00-   2090     .999    ----    ----        11     UNCH   414.50B  187.00A
                    |      250      992.50                  1.00003.7   992.50      ----                  1004.10+    NEW     .999    ----       1         1 +      1   1017.50B 969.50A
                    |     1305      ----                    #134.50B    *119.25A    ----                 ----+       ----     .642    ----       1         1 +      1   134.50B  119.25A*/
                    "     |         |                       |           |           |                  |          |      |        |       |       |           |      |           |";
            }
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        public override string Name
        {
            get { return "Section48_E_Mini_S_And_P_500_Put_Options.pdf"; }
        }
    }

    class EUROFX_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 39; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "EUROFXCALL" || tr == "EUROFXPUT";
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKEC-[0-9][A-Z]-[CP]$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("EUROFX&CME$INDEXOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1350      17.10                   #17.10      *17.10      17.10                  19.40+     NEW     .320    ----       1         1 +      1    19.50B   8.70A
                    |     1280      11.20 @11.30            11.50       11.20       11.50                  11.50+       4     .269    ----     801      5688 -    404    50.90B   9.50A
                    |     1190      ----                    ----        ----        ----                -----        ----     ----    ----    ----        ----   UNCH    230.10B  160.70A
                    |     1320      9.90 @9.80              10.10       9.50        9.50                    9.40-       7     .209    ----    1367      3345 +    652    83.30    4.60
                    |     1540      3.10                    3.20        3.10        3.20 @3.10              3.30-      39     .438    ----     455       909 +    261    56.20B   2.60
                    |     1220      ----                    ----        ----        88.40N                 93.90+      55     .996    ----    ----        55     UNCH    129.50B  85.50A
                    |     1335      128.50 @130.00          #131.00     128.50      131.00                100.40-      42     .855    ----     334       278 -    195    131.00   8.50"*/
                    "     |         |                       |           |           |                  |          |      |        |       |       |           |      |           |";
            }
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        public override string Name
        {
            get { return "Section39_Euro_FX_And_Cme$Index_Options.pdf"; }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var s = tr == "EUROFXFUT";
            //if (s) { ;}
            return s;
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    SEP12    1.21280              1.21500B    *1.21280      1.21500B            1.21350  (    .8241) -     33    3081  281651   323231 -   9100 1.45940B  1.20760 
                    |    DEC09    1.47830              1.48400B     1.47450A     1.47950 @1.47980B   1.47970  (    .6758) +      7    2121  227281   165128 +   5032 1.53170B  1.26450A*/
                    "    |        |                   |           |          |                    |           |           |       |       |       |        |        |         |";
            }
        }
    }

    class EUROFX_DBPDFFormatInfo_v4 : EUROFX_DBPDFFormatInfo_v3
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 4; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v4, для DB с 8.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    1.23960              1.24060    *1.23800     1.24060             1.24440  (   .8036) +     54     509    326872   463244 +    2827  1.39840B  1.23610
                    |    SEP15              1.10660              1.10680         1.09270        1.09860  (   .9102) -    72         402    274988   352107 +    7026  1.39660B  1.04930A */
                    "    |                 |                    |            |               ||          |          |      |           |         |        |           |         |";
            }
        }

        EUROFX_DBPDFFormatInfo_v3 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new EUROFX_DBPDFFormatInfo_v3();
                return previousVersion;
            }
        }
    }

    class EUROFX_DBPDFFormatInfo_v3 : EUROFX_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    1.23960              1.24060    *1.23800     1.24060             1.24440  (   .8036) +     54     509    326872   463244 +    2827  1.39840B  1.23610*/
                    "    |        |                   |           |          |                    |         |          |       |       |         |        |           |         |";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1190     ----                     ----        ----        49.10N                 54.40+     53     1.000      1    ----         ----     1    109.70B  47.40A    */
                    "     |        |                        |           |           |                  |          |     |         |      |       |            |     |          |";
            }
        }

        public override bool IsStartFutMarker(string line)
        {
            return line.Replace(" ", "").StartsWith("EUROFX&CME$INDEXFUTURES");
        }

        public override string FuturesNameOverride
        {
            get
            {
                return "EURO FX FUT";
            }
        }

    }

    class EUROFX_DBPDFFormatInfo_v2 : EUROFX_DBPDFFormatInfo
    {
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res = 
                base.TryParsePageHeader(line, out headerInfo)
                && headerInfo.bulletinDate >= new DateTime(2012, 12, 07);
            return res;
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC12    ----                    ----         ----    1.29390N            1.30040   (  .7690) +     65    6130    231198   180690 -   22702   1.44050B  1.20680A*/
                    "    |        |                   |           |          |                    |          |         |       |       |         |        |           |         |";
            }
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.12.2012";
            }
        }
    }

    class AUSTDLR_DBPDFFormatInfo_v3 : AUSTDLR_DBPDFFormatInfo_v2
    {
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 9.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14   ----                 ----        ----     0.8615N               .8593 (  1.1637) -    22     42   74554  123564 -   6181   .9411B    .8520
                    |    SEP15             0.7473               .7473      *.7370            .7411  (  1.3493) -   57       3198  100451  135475 +   3834   .9185B    .7370   */
                    "    |               |                     |          |               ||        |          |     |           |       |      |        |         |";
            }
        }

        AUSTDLR_DBPDFFormatInfo_v2 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new AUSTDLR_DBPDFFormatInfo_v2();
                return previousVersion;
            }
        }
    }

    class AUSTDLR_DBPDFFormatInfo_v2 : AUSTDLR_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.92f; } }

        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14   ----                 ----        ----     0.8615N               .8593 (  1.1637) -    22     42   74554  123564 -   6181   .9411B    .8520 */
                    "    |      |                  |           |          |                    |       |          |      |       |       |      |        |         |";
            }
        }

        protected override string PositionOptMarkers
        {
	        get 
	        {
                return
                    /*
                    |    1020     1.64                   #1.64      1.64       1.64                  1.63-     13     .536   ----      7       248 +     2    6.32B   1.50
                    |    1040     0.85                   #.85       *.85       0.85                   .84+     43     .452   ----     60       411 +     2    4.20B   .33
                    |    1025     ----                   ----       ----       0.90N                 1.16+     26     .753   ----   ----       173    UNCH    5.27B   .47
                    |     980     ----                   ----       ----       4.63N                 5.20+     57     .883   ----   ----         4    UNCH    6.91B   4.03A
                    |     895     ----                   ----       ----       0.15N                   .15   UNCH    .103   ----      8       228 +     8    .83B    .12A
                    |     865     ----                   ----       ----       0.21N                  .27+      6    .363   ----     226      248 +    79    2.37B   .15A*/
                    "    |        |                      |          |          |              |            |     |       |      |       |          |     |          |";
	        }
        }
    }

    class AUSTDLR_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 38; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "AUSTDLRCALL" || tr == "AUSTDLRPUT";
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKAD-[0-9][A-Z]-[CP]$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("AUSTRALIANDOLLARANDNEWZEALANDDOLLAROPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.902893f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |    1020     1.64                   #1.64      1.64       1.64                  1.63-     13     .536   ----      7       248 +     2    6.32B   1.50
                    |    1040     0.85                   #.85       *.85       0.85                   .84+     43     .452   ----     60       411 +     2    4.20B   .33
                    |    1025     ----                   ----       ----       0.90N                 1.16+     26     .753   ----   ----       173    UNCH    5.27B   .47
                    |     980     ----                   ----       ----       4.63N                 5.20+     57     .883   ----   ----         4    UNCH    6.91B   4.03A*/
                    "    |        |                      |          |          |              |            |     |        |      |      |           |     |          |";
            }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var s = tr == "AUSTDLRFUT";
            if (s) { ;}
            return s;
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    SEP12    1.21280              1.21500B    *1.21280      1.21500B            1.21350  (    .8241) -     33    3081  281651   323231 -   9100 1.45940B  1.20760 
                    |    DEC09    1.47830              1.48400B     1.47450A     1.47950 @1.47980B   1.47970  (    .6758) +      7    2121  227281   165128 +   5032 1.53170B  1.26450A
                    "    |        |                   |           |          |                    |           |           |       |       |       |        |        |         |";
                    |    MAR13   ----                 ----        ----     1.0310N              1.0325  (   .9685) +   15    3399   40591  169985 -  3243    1.0520   .9514A
                    |    DEC12   1.0530            #1.0530      1.0530     1.0530               1.0561  (   .9469) +   47     963   34009   58956 - 17820    1.0582   .9549 
                    |    MAR11   ----                 ----        ----     0.7676N               .7752  ( 1.2900) +   760    599    2129    26488 +   175    .7891B   .7158A  <- NEW ZEALND*/
                    "    |      |                  |           |          |                    |        |         |      |       |       |       |       |          |";
            }
        }

        public override string Name
        {
            get { return "Section38_Australian_Dollar_New_Zealand_Dollar_Options.pdf"; }
        }
    }

    class CANADADLRCALL_DBPDFFormatInfo_v3 : CANADADLRCALL_DBPDFFormatInfo_v2
    {
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 9.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    1.5852                1.5852     *1.5852       1.5852               1.5860  (    .6305) +     27    127   105076   156380 +   7592  1.7165   1.4838A
                    |    SEP15              0.7897                  .7899         *.7817         .7851   (   1.2737) -     43         760   76165    115334 +   4724  .9285B  .7777A */
                    "    |                 |                      |             |          ||            |           |       |            |       |        |        |         |";
            }
        }

        CANADADLRCALL_DBPDFFormatInfo_v2 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new CANADADLRCALL_DBPDFFormatInfo_v2();
                return previousVersion;
            }
        }
    }

    class CANADADLRCALL_DBPDFFormatInfo_v2 : CANADADLRCALL_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    1.5852                1.5852     *1.5852       1.5852               1.5860  (    .6305) +     27    127   105076   156380 +   7592  1.7165   1.4838A */
                    "    |        |                    |           |          |                    |          |           |       |       |       |        |        |         |";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class CANADADLRCALL_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 29; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr=="CANADADLRCALL";
            //WKCD-2C-C
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKCD-[0-9][A-Z]-C$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("CANADIANDOLLARCALLOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1000      ----                    ----         ----       3.60N                   3.20-     40      .551    ----    ----         5     UNCH    3.25     3.00 */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "CANADADLRFUT";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    MAR13    ----                    ----        ----       1.0020N              1.0037   (   .9963) +     17   2168    37659   134662 -   2412   1.0330B.9469A
                    |    SEP08    1.9700 @1.9701        1.9702B     1.9665A      1.9679               1.9672   (   .5083) -     91    3078   76489    99658 -    647   2.0906B 1.9106A */
                    "    |        |                    |           |          |                    |           |          |       |       |       |        |        |         |";
            }
        }

        public override string Name
        {
            get { return "Section29_Canadian_Dollar_Call_Options.pdf"; }
        }
    }

    class CANADADLRPUT_DBPDFFormatInfo_v2 : CANADADLRPUT_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class CANADADLRPUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 30; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "CANADADLRPUT";
            //WKCD-2C-P
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKCD-[0-9][A-Z]-P$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("CANADIANDOLLARPUTOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1000      ----                    ----         ----       3.60N                   3.20-     40      .551    ----    ----         5     UNCH    3.25     3.00 */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override string Name
        {
            get { return "Section30_Canadian_Dollar_Put_Options.pdf"; }
        }
    }
    class JAPANESEYENCALL_DBPDFFormatInfo_v3 : JAPANESEYENCALL_DBPDFFormatInfo_v2
    {
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 9.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC15    0.0082070           .0082220B  *.0081385A     0.0081440N            .0081880( 122.1299) +     44   ----      122     1958 +     32  .009978B  .007977
                    |    SEP15               0.0081670           .0082030       .0081450        .0081745 (   122.33) -      0         818   120535   248561 +    263  .009950B  .007956A*/
                    "    |                  |                  |              |             ||           |           |       |            |       |        |        |         |";
            }
        }

        JAPANESEYENCALL_DBPDFFormatInfo_v2 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new JAPANESEYENCALL_DBPDFFormatInfo_v2();
                return previousVersion;
            }
        }
    }

    class JAPANESEYENCALL_DBPDFFormatInfo_v2 : JAPANESEYENCALL_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    0.008674             .008674    *.008674      0.008674             .008733  (   114.51) +     32    101   278532   210096 -   1570  .010456B  .008652
                    |    JUN15    0.008614            #.008614    *.008614      0.008614             .008625  (   115.94) -     42      1        6       84 +      3  .009937B  .008583A  
                    |    DEC15    0.0082070           .0082220B  *.0081385A     0.0081440N            .0081880( 122.1299) +     44   ----      122     1958 +     32  .009978B  .007977*/
                    "    |        |                  |          |             |                    |          |           |       |       |       |        |        |         |";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class JAPANESEYENPUT_DBPDFFormatInfo_v2 : JAPANESEYENPUT_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class JAPANESEYENCALL_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 33; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "JAPANYENCALL";
            //WKJY-3J-C
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKJY-[0-9][A-Z]-C$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("JAPANESEYENCALLOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1540      ----                    ----        ----        5.780N                 5.650-     13      .999    ----    ----         3     UNCH    11.100B  6.150B
                    |     1580      ----                    ----        ----        2.010N                 1.730-     28      .953    ----    ----         6     UNCH    8.900B   1.470B
                    |     1590      ----                    ----        ----        1.240N                  .870-     37      .780    ----    ----        63     UNCH    8.100B   .750B
                    |     1600      ----                    ----        ----        0.640N                  .280-     36      .324    ----     448       594 +    281    6.650B   .210B
                    |     1600      1.240                   1.240       *1.240      1.240                  1.380+     75     1.000    1204      41        ----   1232    4.730B   .180B
                    |     1580      ----                    ----        ----        0.080N             CAB          ----      ----    ----       2        ----    331    6.850    .010
                    |     1400      1.150                   #1.150      *1.150      1.150                  1.110+    NEW      .132    ----       2         2 +      2    1.460B   1.150B */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "JAPANYENFUT";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    MAR13    ----                    ----        ----       1.0020N              1.0037   (   .9963) +     17   2168    37659   134662 -   2412   1.0330B.9469A
                    |    SEP08    1.9700 @1.9701        1.9702B     1.9665A      1.9679               1.9672   (   .5083) -     91    3078   76489    99658 -    647   2.0906B 1.9106A
                    |    DEC11    ----                    ----        ----       0.013034N           .013023   (   76.79) -     11    3399   99386   148334 -   2331  .013188B  .011729A
                    |    MAR12    0.012972            #.012972     .012972       0.012972            .013010   (   76.86) +    112   18776   70687   158058 +  19029  .013279   .011783A
                    |    TOTAL  JAPAN YEN FUT                                                                                        18776   70733    158363+   9022
                    |    MAR12    0.012816             .012816    *.012816       0.012816            .012815   (   78.03) -     16    3993   34769   129353 -   1710  .013279   .011783A */
                    "    |        |                   |           |           |                    |           |          |       |       |       |         |        |         |";
            }
        }

        public override string Name
        {
            get { return "Section33_Japanese_Yen_Call_Options.pdf"; }
        }
    }

    class JAPANESEYENPUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 34; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "JAPANYENPUT";
            if (res) return res;
            res = System.Text.RegularExpressions.Regex.IsMatch(tr, "^WKJY-[0-9][A-Z]-P$");
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("JAPANESEYENPUTOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.602997f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1540      ----                    ----        ----        5.780N                 5.650-     13      .999    ----    ----         3     UNCH    11.100B  6.150B
                    |     1580      ----                    ----        ----        2.010N                 1.730-     28      .953    ----    ----         6     UNCH    8.900B   1.470B
                    |     1590      ----                    ----        ----        1.240N                  .870-     37      .780    ----    ----        63     UNCH    8.100B   .750B
                    |     1600      ----                    ----        ----        0.640N                  .280-     36      .324    ----     448       594 +    281    6.650B   .210B
                    |     1600      1.240                   1.240       *1.240      1.240                  1.380+     75     1.000    1204      41        ----   1232    4.730B   .180B
                    |     1580      ----                    ----        ----        0.080N             CAB          ----      ----    ----       2        ----    331    6.850    .010
                    |     1400      1.150                   #1.150      *1.150      1.150                  1.110+    NEW      .132    ----       2         2 +      2    1.460B   1.150B */
                    "     |         |                       |           |           |                  |         |      |         |       |       |           |      |            |";
            }
        }

        public override string Name
        {
            get { return "Section34_Japanese_Yen_Put_Options.pdf"; }
        }
    }

    class GOLD_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 64; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX",""); // почему-то иногда пишут COMEX...OPTIONS, а на следующей странице уже просто ...OPTIONS
            return
                tr == "OGPUTGOLDOPTIONS"
                || tr == "OGCALLGOLDOPTIONS"
                || tr == "SOPUTSILVEROPTIONS"
                || tr == "SOCALLSILVEROPTIONS"
                || tr == "POPUTPLATINUMOPTIONS"
                || tr == "POCALLPLATINUMOPTIONS"
                || tr == "HROPUTU.S.STEELAPO"
                || tr == "HROCALLU.S.STEELAPO"
                || tr == "PAOPUTNYMEXPALLADIUMOPTIONS"
                || tr == "PAOCALLNYMEXPALLADIUMOPTIONS"
                || tr == "ICTPUTIRONORE62%FECFRCHINAAPO"
                || tr == "ICTCALLIRONORE62%FECFRCHINAAPO"
                || tr == "HXPUTCOPPEROPTIONS"
                || tr == "HXCALLCOPPEROPTIONS";
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("METALSOPTIONPRODUCTS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONPRODUCTS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.594069f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1000   0.10     ----             0.10/0.10        ----                0.10N                0.10    UNCH   .0014   ----    ----      207    ----    1230 +    174
                    |     1175   ----     ----             ----             ----                0.90N                0.70 -  0.20   .0138   ----    ----     ----    ----      87     UNCH
                    |     2100   ----     ----             ----/*1.615A      ----                1.874N             1.810 -   0.064 .8523   ----    ----     ----    ----       3     UNCH
                    |     1700   ----     ----               ----                ----              ----           ----    -  4.80    ----     ----    ----    ----     ----     0 -    200*/
                    "     |    |     |         |         |                 |                   |                  |        |       |     |        |       |        |       |       |";
            }
        }

        public override string FormatOptionComodName(string comod)
        {
            // В DB от Fri, Apr 25, 2014 первый раз пишут:
            // OG PUT GOLD OPTIONS
            // а потом:
            // OG PUT COMEX GOLD OPTIONS
            // то же и с call
            return base.FormatOptionComodName(comod.Replace("COMEX", ""));
        }

        public override string Name
        {
            get { return "Section64_Metals_Option_Products.pdf"; }
        }

    }

    // TODO: эта секция пока не работает
    class GOLDFUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 62; } }
        public override bool IsStartOptMarker(string line)
        {
            return false;
        }

        protected override string PositionOptMarkers
        {
            get { return ""; }
        }


        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("METALFUTURESPRODUCTS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "FUTURESPRODUCTS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.902893f; }
        }

        public override string Name
        {
            get { return "Section62_Metals_Futures_Products.pdf"; }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "GCFUTCOMEXGOLDFUTURES" || tr == "SIFUTCOMEXSILVERFUTURES";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    FEB13 1655.90  1664.20       1681.00 /1655.90   1664.20 /1664.20  1664.20              1675.80 +  19.90      126     82346   2746   255539  -    760
                    |    SEP12 27.105   ----          27.895 /26.990     27.801 /27.801    26.995N               27.801P+  0.806      190     35861   1394    56599  -    727 */
                    "    |     |       |             |                  |                 |                    |        |       |        |         |      |          |";
            }
        }
    }

    class ENERGY_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 63; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX", ""); // почему-то иногда пишут COMEX...OPTIONS, а на следующей странице уже просто ...OPTIONS
            return
                false
                || tr == "LOCALLNYMEXCRUDEOILOPTIONS(PHY)"
                || tr == "LOCALLNYMEXCRUDEOILOPTIONS(PHY)"
                || tr == "LOPUTNYMEXCRUDEOILOPTIONS(PHY)"
                || tr == "OHCALLNYMEXHEATINGOIL(PHY)OPTIONS"
                || tr == "OHPUTNYMEXHEATINGOIL(PHY)OPTIONS"
                || tr == "ONCALLNYMEXNATURALGASOPTIONS(PHY)"
                || tr == "ONPUTNYMEXNATURALGASOPTIONS(PHY)";
        }


        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("ENERGYOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(
                int.Parse(fields[1]),
                fields[2],
                fields[3],
                DateTime.Parse(fields[4])
            );

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.594069f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     8225   ----     ----           ----               ----            ----                25.83  -   0.21   1.0000   ----    ----    ----    ----        50     UNCH*/
                    "     |    |     |         |        |                 |               |                  |          |       |       |        |       |        |       |        |";
            }
        }

        public override string Name
        {
            get { return "Section63_Energy_Options_Products.pdf"; }
        }

    }


    class ENERGY_2011_DBPDFFormatInfo : ENERGY_DBPDFFormatInfo
    {
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && headerInfo.bulletinDate < new DateTime(2011, 08, 26);
            return res;
        }


        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     4000   ----     ----               ----                ----              49.38N              50.30 +   0.92  .9992  ----      ----    ----   ----   12     UNCH
                    |     8200   8.23     ----               8.90B/6.48A         ----              7.53N                8.36 +   0.83  .9746  ----      ----       2   ---- 2908 +      2
                    |     10000  0.03     ----               0.04/*0.02          ----              0.03N                0.02 -   0.01  .0101  ----       106     318   ----31216 +     58
                    |     4000   ----     ----               ----                ----              49.38N              50.30 +   0.92  .9992  ----      ----    ----   ----   12     UNCH
                    |     8200   8.23     ----               8.90B/6.48A         ----              7.53N                8.36 +   0.83  .9746  ----      ----       2   ---- 2908 +      2 */
                    "     |    |     |         |            |                   |                |                  |         |      |      |       |        |        |    |      |";                
            }
        }

        public override string Name
        {
            get { return base.Name + " для DB до 26.08.2011"; }
        }
    }

    class SWISSFRNCCALL_DBPDFFormatInfo_v3 : SWISSFRNCCALL_DBPDFFormatInfo_v2
    {
        public override int Version { get { return 3; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2015_07_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v3, для DB с 9.07.2015";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    ----                  ----         ----       1.0363N              1.0344   (    .9667) -     19     43    42376    60683 +    120   1.1478B   1.0268 
                    |    SEP15              1.0638               1.0642        *1.0536            1.0594 (      .9439) -    39        141    17664    23370 +   1374   1.2001B   .9848A*/
                    "    |                |                    |              |               ||         |             |      |           |       |        |        |         |";
            }
        }

        SWISSFRNCCALL_DBPDFFormatInfo_v2 previousVersion;
        public override DBPDFFormatInfo PreviousVersion
        {
            get
            {
                if (previousVersion == null)
                    previousVersion = new SWISSFRNCCALL_DBPDFFormatInfo_v2();
                return previousVersion;
            }
        }
    }

    class SWISSFRNCCALL_DBPDFFormatInfo_v2 : SWISSFRNCCALL_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    DEC14    ----                  ----         ----       1.0363N              1.0344   (    .9667) -     19     43    42376    60683 +    120   1.1478B   1.0268 
                    |    JUN15    1.0465             #1.0465      *1.0465       1.0465               1.0464   (    .9557) -     54     28     1007     4108 +    190   1.2012B   .9803A*/
                    "    |        |                  |            |           |                    |          |           |       |       |       |        |        |         |";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1575     ----                     ----        ----       1.000N                 1.100+     NEW     1.000      2       3         ----  UNCH    5.960B   .520B */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class SWISSFRNCPUT_DBPDFFormatInfo_v2 : SWISSFRNCPUT_DBPDFFormatInfo
    {
        public override float CharSpacing { get { return 3.618f; } }
        public override int Version { get { return 2; } }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var res =
                base.TryParsePageHeader(line, out headerInfo)
                && formatChange2014_11_07(headerInfo);
            return res;
        }

        public override string Name
        {
            get
            {
                return base.Name + " v2, для DB с 7.11.2014";
            }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    | */
                    "     |        |                        |           |          |                   |         |      |         |       |      |            |      |           |";
            }
        }
    }

    class SWISSFRNCCALL_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 35; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "SWISSFRNCCALL";
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("CALLOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.61f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     1055      ----                    ----         ----      2.02N                     .95-    107      .783    ----   ----         15    UNCH     2.17B   1.02A */
                    "     |         |                       |            |         |                  |          |      |         |       |      |            |      |           |";
            }
        }

        public override string Name
        {
            get { return "Section35_Swiss_Franc_Call_Options.pdf"; }
        }

        public override bool IsStartFutMarker(string line)
        {
            var tr = line.Replace(" ", "");
            return tr == "SWISSFRNCFUT";
        }

        protected override string PositionFutMarkers
        {
            get
            {
                return
                    /*
                    |    MAR12    ----                  ----         ----       1.0607N              1.0494    (    .9529)-    113     356   32621    41456 +   1838    1.4171B  1.0331A
                    |    MAR12    1.0703             #1.0740B     *1.0703       1.0740B              1.0745    (    .9307)+     75     837   14644    37976 +    556    1.4171B  1.0331A
                    |    TOTAL  CNH FUT                                                                                                 0        2      297 -      2*/
                    "    |     |                     |            |           |                    |           |          |       |       |       |        |        |          |";
            }
        }
    }

    class SWISSFRNCPUT_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 36; } }
        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "");
            var res = tr == "SWISSFRNCPUT";
            return res;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("PUTOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override float CharSpacing
        {
            get { return 3.61f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /**/
                    "     |         |                       |            |         |                  |          |      |         |       |      |            |      |           |";
            }
        }

        public override string Name
        {
            get { return "Section36_Swiss_Franc_Put_Options.pdf"; }
        }
    }


    class CornOatRoughRice_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 56; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX", "");
            return
                false
                || tr == "CORNCALL"
                || tr == "CORNPUT"
                || tr == "OATSCALL"
                || tr == "OATSPUT"
                || tr == "ROUGHRICECALL"
                || tr == "ROUGHRICEPUT"
            ;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!tr.StartsWith("PG") || !tr.Contains("CORN,OATANDROUGHRICEOPTIONS"))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(
                int.Parse(fields[1]),
                fields[2],
                fields[3],
                DateTime.Parse(fields[4])
            );

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.5986f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     350     ----                      ----        ----       1267N                     1281 +      12   1.000    141       1         ----   140   1612B     1117A
                    |     485     ----                      80          80         82N                         83P+       1    .414   ----      58       4245 +     6   452B      71A
                    |     520     ----                      ----        ----       CAB                    ----         UNCH    .009   ----       6       4334    UNCH   830       2 */
                    "    |        |                         |           |          |                    |          |       |       |      |       |            |     |           |";
            }
        }

        public override string Name
        {
            get { return "Section56_Corn_Oat_RoughRice_Options.pdf"; }
        }

    }

    class Soybean_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 57; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX", "");
            return
                false
                || tr == "SOYBEANCALL"
                || tr == "SOYBEANPUT"
                || tr == "SOYMEALCALL"
                || tr == "SOYMEALPUT"
                || tr == "SOYBEANOILC"
                || tr == "SOYBEANOILP"
                || tr == "WHEATCALL"
                || tr == "WHEATPUT"
            ;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!(tr.Contains("SOYBEAN") && tr.Contains("PG")))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "BULLETIN#", "@", "OPTIONS", ",CSOSOYBEANOIL,", "PG" }, StringSplitOptions.None);
            if (fields.Length != 5)
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(
                int.Parse(fields[4]),
                fields[1],
                fields[2],
                DateTime.Parse(fields[3])
            );

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.5986f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     350     ----                      ----        ----       1267N                     1281 +      12   1.000    141       1         ----   140   1612B     1117A
                    |     485     ----                      80          80         82N                         83P+       1    .414   ----      58       4245 +     6   452B      71A
                    |     1180    ----                      ----        ----       ----                   ---- +        NEW    .716   ----       2          2 +     2   ----      ----   */
                    "    |        |                         |           |          |                    |          |       |       |      |       |            |     |           |";
            }
        }

        public override string Name
        {
            get { return "Section57_Soybean_Soymeal_Soyoil_SoybeanCrush_wheat_Options.pdf"; }
        }

    }

    class EuroDollarCall_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 51; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX", "");
            return
                tr == "EURODLRCALL"
            ;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!(tr.Contains("EURODOLLARCALLOPTIONS") && tr.Contains("PG")))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.5986f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     9950      ----                    .075B       ----        0.075B             .072+     0.5     .361    ----    7200   2500    295123 +    810 .190B    .030*/
                    "    |          |                       |           |           |             |         |       |        |       |       |      |           |      |      |";
            }
        }

        public override string Name
        {
            get { return "Section51_Euro_Dollar_Call_Options.pdf"; }
        }

    }

    class EuroDollarPut_DBPDFFormatInfo : DBPDFFormatInfo
    {
        public override int SectionCode { get { return 52; } }

        public override bool IsStartOptMarker(string line)
        {
            var tr = line.Replace(" ", "").Replace("COMEX", "");
            return
                tr == "EURODLRPUT"
            ;
        }

        protected override bool TryParsePageHeader(string line, out PdfPageHeadingInfo headerInfo)
        {
            var tr = line.Replace(" ", "");
            if (!(tr.Contains("EURODOLLARPUTOPTIONS") && tr.Contains("PG")))
            {
                headerInfo = null;
                return false;
            }

            var fields = line.Split(new string[] { "PG", "BULLETIN#", "@", "OPTIONS" }, StringSplitOptions.None);
            if (fields.Length != 6 || fields[1] != fields[5])
                throw new Exception("Unknown section format.");

            headerInfo = new PdfPageHeadingInfo(int.Parse(fields[1]), fields[2], fields[3], DateTime.Parse(fields[4]));

            return headerInfo.bulletinSectionCode == SectionCode;
        }

        public override bool HasFuturesSettPrice
        {
            get
            {
                return false;
            }
        }

        public override float CharSpacing
        {
            get { return 3.5986f; }
        }

        protected override string PositionOptMarkers
        {
            get
            {
                return
                    /*
                    |     9925      ----                    ----        ----        ----                 CAB          ----    .000    ----    ----       250       UNCH ----     .010A
                    |     9962      0.005                   .005        *.005       0.005                     .007    UNCH    .174    ----    1000     16949 -      700 .070B    .010
                    |     9900      ----                    ----        ----        ----                 CAB          ----    .000    ----    6000    495353  +    1000 .610B    .000
                    |     9887      ----                    ----        ----        0.020N                   .017-     0.2    .093    ----   14800    174560  -    4050 .990B    .020*/
                    "    |          |                       |           |           |         |                      |       |        |    |         |         |       |        |";
            }
        }

        public override string Name
        {
            get { return "Section52_Euro_Dollar_Put_Options.pdf"; }
        }
    }
}
