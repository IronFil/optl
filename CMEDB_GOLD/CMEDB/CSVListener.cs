﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл предназначен для записи результатов конвертации в файл csv


using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace CMEDB
{
    public class CSVListener : ICMEDBParserListener
    {
        protected TextWriter output;
        
        const string csvString = "\"{0}\"";

        protected int fut_oi_ch;
        protected int fut_oi;
        protected int fut_globex_vol;
        protected int fut_rth_vol;

        protected int opt_total_vol;
        protected int opt_total_oi;
        protected int opt_total_oi_ch;

        protected bool isFirstStrike = true;
        bool isFirstFut = true;

        int bulletinNumber;
        DateTime bulletinDate;
        string sectionCode;

        public bool NoTotal { get; set; }

        public bool PrefixLinesWithDateAndNumber { get; set; }

        public bool ZeroCab { get; set; }

        public CSVListener(TextWriter output, string sourcePath)
        {
            this.output = output;
            output.WriteLine("[" + sourcePath + "]");
            this.PrefixLinesWithDateAndNumber = false;
            this.NoTotal = false;
        }

        protected void WriteLinePrefix()
        {
            if (PrefixLinesWithDateAndNumber)
            {
                output.Write(bulletinDate.ToShortDateString());
                output.Write(";");
                output.Write(bulletinNumber);
                output.Write(";");
            }
        }

        public virtual void ProcessFutTotal(int rth_vol, int globex_vol, int oi, int oi_ch)
        {
            WriteLinePrefix();
            output.WriteLine("FUTURES TOTAL;;;;;;;;;;;;;"+rth_vol+";"+globex_vol+";"+oi+";"+oi_ch);
            if (!NoTotal)
            {
                if (this.fut_rth_vol != rth_vol || this.fut_globex_vol != globex_vol || this.fut_oi != oi || this.fut_oi_ch != oi_ch)
                {
                    WriteLinePrefix();
                    output.WriteLine("COMPUTED FUTURES TOTAL;;;;;;;;;;;;;" + this.fut_rth_vol + ";" + this.fut_globex_vol + ";" + this.fut_oi + ";" + this.fut_oi_ch);
                }
            }
            this.fut_rth_vol = 0;
            this.fut_globex_vol = 0;
            this.fut_oi = 0;
            this.fut_oi_ch = 0;
        }


        public virtual void ProcessFut(FuturesInfo fut)
        {
            if (isFirstFut)
            {
                WriteLinePrefix();
                WriteFutHeader();
                isFirstFut = false;
            }

            WriteLinePrefix();
            output.WriteLine(fut.ToString());
            this.fut_rth_vol += fut.RthVolume;
            this.fut_globex_vol += fut.GlobexVolume;
            this.fut_oi += fut.OpenInterest;
            this.fut_oi_ch += fut.OpenInterestChange;
        }

        protected void WriteFutHeader()
        {
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", ""));

            output.Write(string.Format(csvString + ";", "OPEN RANGE (start)"));
            output.Write(string.Format(csvString + ";", "OPEN RANGE (end)"));
            output.Write(string.Format(csvString + ";", "HIGH"));
            output.Write(string.Format(csvString + ";", "LOW"));
            output.Write(string.Format(csvString + ";", "CLOSING RANGE (start)"));
            output.Write(string.Format(csvString + ";", "CLOSING RANGE (end)"));
            
            output.Write(string.Format(csvString + ";", "SETT.PRICE"));
            output.Write(string.Format(csvString + ";", "PT. CHGE"));
            output.Write(string.Format(csvString + ";", "RECIPROCAL"));
            

            output.Write(string.Format(csvString + ";", "RTH VOLUME"));
            output.Write(string.Format(csvString + ";", "GLOBEX VOLUME"));

            output.Write(string.Format(csvString + ";", "OPEN INTEREST"));
            output.Write(string.Format(csvString + ";", "OPEN INTEREST CHANGE"));
            output.Write(string.Format(csvString + ";", "CONTRACT HIGH"));
            output.Write(string.Format(csvString + ";", "CONTRACT LOW"));
            output.WriteLine();
        }

        public virtual void ProcessStrike(Strike strike)
        {
            if (isFirstStrike)
            {
                WriteLinePrefix();
                WriteStrikeHeader();
                isFirstStrike = false;
            }
            WriteLinePrefix();
            if (ZeroCab)
                output.WriteLine(strike.ToString().Replace("CAB", "0"));
            else
                output.WriteLine(strike.ToString());
            this.opt_total_oi += strike.OpenInterest;
            this.opt_total_oi_ch += strike.OpenInterestChange;
            this.opt_total_vol += strike.VolumeTradesCleared;
        }

        protected void WriteStrikeHeader()
        {
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", ""));
            output.Write(string.Format(csvString + ";", "STRIKE"));
            output.Write(string.Format(csvString + ";", "OPEN RANGE (start)"));
            output.Write(string.Format(csvString + ";", "OPEN RANGE (end)"));
            output.Write(string.Format(csvString + ";", "HIGH"));
            output.Write(string.Format(csvString + ";", "LOW"));
            output.Write(string.Format(csvString + ";", "CLOSING RANGE (start)"));
            output.Write(string.Format(csvString + ";", "CLOSING RANGE (end)"));
            output.Write(string.Format(csvString + ";", "SETT.PRICE"));
            output.Write(string.Format(csvString + ";", "PT.CHGE."));
            output.Write(string.Format(csvString + ";", "DELTA"));
            output.Write(string.Format(csvString + ";", "EXERCISES"));
            output.Write(string.Format(csvString + ";", "VOLUME TRADES CLEARED"));
            output.Write(string.Format(csvString + ";", "OPEN INTEREST"));
            output.Write(string.Format(csvString + ";", "OPEN INTEREST CHANGE"));
            output.Write(string.Format(csvString + ";", "CONTRACT HIGH"));
            output.Write(string.Format(csvString + ";", "CONTRACT LOW"));
            output.WriteLine();
        }

        public void ProcessSectionInfo(int bulletinSectionCode, string bulletinName, int bulletinNumber, DateTime bulletinDate, bool isFinal)
        {
            this.bulletinDate = bulletinDate;
            this.bulletinNumber = bulletinNumber;
            this.sectionCode = bulletinSectionCode.ToString("00");

            output.WriteLine("Section;" + bulletinSectionCode + ";Name;" + bulletinName + ";N;" + bulletinNumber + ";Date;" + bulletinDate.ToShortDateString());
        }

        public void Error(Exception ex, string line)
        {
            output.WriteLine("PARSE ERROR;" + ex.Message + ";" + line);
        }

        public void ProcessTotal(int total_vol, int total_oi, int total_oi_ch, string contractName, DateTime contractDate, decimal futuresSettPrice)
        {
            WriteLinePrefix();
            output.WriteLine("FuturesSettPrice;" + futuresSettPrice);
            WriteLinePrefix();
            output.WriteLine("DB TOTAL;;;;;;;;;;;;;;" + total_vol + ";" + total_oi + ";" + total_oi_ch + ";;;");
            if (!NoTotal)
            {
                if (this.opt_total_vol != total_vol || this.opt_total_oi != total_oi || this.opt_total_oi_ch != total_oi_ch)
                {
                    WriteLinePrefix();
                    output.WriteLine("COMPUTED TOTAL;;;;;;;;;;;;;;" + total_vol + ";" + total_oi + ";" + total_oi_ch + ";;;");
                }
            }
            this.opt_total_oi = 0;
            this.opt_total_oi_ch = 0;
            this.opt_total_vol = 0;
        }

        public void ProcessStop(string line)
        {
            output.WriteLine("STOPPED ON " + line.Trim());
            output.Flush();
        }

        public string FillMask(string fileOut)
        {
            return CMEDB.Utils.FillMask(fileOut, int.Parse(sectionCode), bulletinDate, bulletinNumber);
        }
    }
}
