﻿// This file is a part of CMEDB software utility created by Timur Vishnyakov (tumypv@yandex.ru)
// to convert CME Group Inc. Daily bulletin reports from pdf to csv for further processing in spreadsheet applications.
// This version of CMEDB and it's source code is Public domain software
// You can modify it and use in any way you want as long as this notice remains unchanged

// Этот файл является частью утилиты CMEDB созданной Тимуром Вишняковым (tumypv@yandex.ru)
// Для преобразования ежедневных отчётов Чикагской биржи из pdf в csv для дальнейшей обработки в электронных таблицах
// Эта версия CMEDB и её исходный код являются общественным достоянием, это значит что
// вы можете модифицировать её исходный код и использвать для любых своих целей при условии что это уведомление остаётся неизменным

// Этот файл содержит код графического интерфейса пользователя

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using CMEDB;

namespace CMEDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
#if test
            //foreach (var item in Directory.GetFiles(@"C:\000\000\", "*.pdf", SearchOption.AllDirectories))
            //{
                var p = new CMEDBParser();
                var item = @"E:\bad_eur\21_Section39_Euro_FX_And_Cme$Index_Options.pdf";

                var sb = new StringBuilder();
                using (var tw = new StringWriter(sb))
                {
                    p.Parse(item,
                        new CSVListener(tw, item));
                }
                Console.WriteLine(item);
            //}
#else
            InitializeComponent();
            ShowInfo();
#endif
        }

        private void ShowInfo()
        {
            var sb = new StringBuilder();

            sb.AppendLine("Эта версия является последней. CMEDB v3.17");
            sb.AppendLine("Поддерживаются следующие секции:");
            foreach (var format in CMEDBParser.supportedFormats)
            {
                sb.AppendLine(format.ToString());
            }

            textBox3.Text = sb.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox1.Text = openFileDialog1.FileName;
            saveFileDialog1.FileName = Path.GetFileNameWithoutExtension(openFileDialog1.FileName)+".CSV";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            saveFileDialog1.ShowDialog(this);
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            textBox2.Text = saveFileDialog1.FileName;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var z = File.Create(saveFileDialog1.FileName))
            using (var sw = new StreamWriter(z))
            {
                var parser = new CMEDBParser();
                try
                {
                    var csv = new CSVListener(sw, openFileDialog1.FileName);
                    parser.Parse(openFileDialog1.FileName, csv);
                    MessageBox.Show("Конвертирование завершено успешно");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сбой обработки\n" + ex.Message);
                }

                if (parser.LineParseErrorCount > 0)
                {
                    MessageBox.Show(
                        "Документ был обработан с ошибками, проблемные строки помечены маркером PARSE ERROR");
                }

                sw.Flush();
                sw.Close();
            }
        }
    }
}
